---
title: "Setup"
subtitle: "Where everything starts."
tags: 
---

Enough chit-chat, let us delve right into it.

# Installing Git {#git}

There are different ways of installing Git based on the operating system you are using. Once this is done, please ***do not skip [the last subsection]({{'/01-setup#git-config' | relative_url }})***: it is a required configuration step (if you skip it, Git will ask you before you can change the contents of a project).

## Linux-based distributions

For starters, you can just type `git` in a terminal to see if you have it pre-installed or not. If a pretty long prompt describing "basic" usage of the command is displayed, congratulations: you may skip this part. Otherwise, you will have to install it... which is not difficult at all, because all package managers know Git. Here are a few examples:

* On Ubuntu/Debian:

```shell
apt install git
```

(Of course, you might need to add `sudo` in front of this command and every `apt` command below.)

On Ubuntu, you can add the PPA (Personal Package Archive) that will provide you with the latest stable version:

```shell
add-apt-repository ppa:git-core/ppa
apt update
apt install git
```

* On Fedora:

```shell
dnf install git
```

Replace `dnf` with `yum` if you are using Fedora 21 or below.

* Other Linux distributions: the [official documentation](https://git-scm.com/download/linux) has you covered.

## MacOS

Start by just typing `git` in a terminal: if a pretty long prompt describing "basic" usage is displayed, Git is already installed on your system. Otherwise, you should get a prompt to install Xcode Command Line Tools. Just so you know: while Xcode itself is a monster (over 40GB!), the Command Line Tools can be installed as a standalone and "only" require between 1 and 2GB. An alternative is this command:

```shell
xcode-select --install
```

In both cases, you will end up with the version of Git that is packaged with the Xcode Command Line Tools, which may not be the most up-to-date. If you want the latest version, well... As always (*sigh*), you will need either [Homebrew](https://brew.sh/) or [Macports](https://www.macports.org/). Then, with Homebrew:

```shell
brew install git
```

With Macports:

```shell
sudo port install git
```

## Windows

If you really love your standalone installers, both 32-bit and 64-bit installers are provided [here](https://git-scm.com/download/win).

There is another option that I would warmly recommend, as it will likely save your life a couple more times in the future: [Scoop](https://scoop.sh/#/) is a package manager that you can very easily install and run from the PowerShell, and it installs packages without requiring specific permissions (i.e., no permission popup windows and no inputting passwords every minute). The website provides you with simple instructions for installation; basically just input these two commands in a PowerShell terminal:

```shell
Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser
Invoke-RestMethod -Uri https://get.scoop.sh | Invoke-Expression
```

You can then install Git with a simple

```shell
scoop install git
```

## Local configuration {#git-config}

Once Git is installed, you may configure it as you want. There are [a lot of options](https://git-scm.com/book/en/v2/Customizing-Git-Git-Configuration), most of which will not make sense before we delve further into Git. The only thing that is required is telling Git your name and e-mail address; this information will not be spread on the Internet and/or sold to companies (this is not [Honey](https://www.joinhoney.com/fr/)!), it will just be written in the metadata of your *commits* (we will be talking about these [later on]({{'/03-linear-git-project#repo' | relative_url }})) so that your collaborators know who contributed which piece of code.

Only two commands are needed from the terminal (or the PowerShell terminal for Windows users):

```bash
git config --global user.name "Your Name"
git config --global user.email "youremail@yourdomain.com"
```

{: .box-note}
Once again, [there are a lot more options to uncover](https://git-scm.com/book/en/v2/Customizing-Git-Git-Configuration), so that you can customize your Git experience. Most of the options are purely local, including, but not limited to, changing the interface colors, telling Git which tool you want to use for merging different versions of the same file ([more on that later]({{'/03-linear-git-project#conflict' | relative_url }})), autoconverting newfile characters (useful for collaborations between Mac/Linux and Windows users)... You can either access those by using `git config` commands, or by directly editing a specific file in a given Git project.

Great, you have Git on your computer and it's good to go!

# Connecting to GitLab {#gitlab}

{: .box-note}
From now on, every use of the word "GitLab" refers to the `gitlab.inria.fr` instance. Things work in a very similar fashion on `gitlab-int.inria.fr`, except that one cannot [invite external collaborators](https://external-account.inria.fr/) there. Other instances, including `gitlab.com`, are not covered here, but they are basically the same tool: you might just have less options on `gitlab.com` as a simple, non-paying user... and your data would be stored on some server somewhere, potentially accessible by Gosh-knows-whom.

## Accessing the website

This one is easy:

* Go to `gitlab.inria.fr` with your favorite Web browser (even if it is Safari, Chrome or Microsoft Edge, we will let it slide).
* Connect with iLDAP, using your usual CAS username-password pair.
* Well, that's it. Welcome to GitLab. You should be welcomed with the list of projects hosted on GitLab that you have access to, and you can filter out the ones you did not create yourself by clicking on "Personal". We shall explore the interface pretty soon.

But now, there is a technical hurdle that we should be dealing with. You see, you do not need to log on to the website every time you want to work on a project hosted on GitLab (if this were the case, no one would bother using it). Instead, you "pull" a local copy, on your computer, that you can edit as you wish, and when you are satisfied with your changes, you can "push" the results back. Not everyone can pull a project to their computer, and even less people can push their changes. So, when your computer asks GitLab to pull or push some code, how does GitLab check that you are allowed to do this?

There are two possibilities: using HTTPS (the same protocol used for logging you on to the website), and SSH. If you want to use HTTPS, then you will have to type your CAS username and password every time you have to interact with the server, and this becomes really annoying, really quick. Let us configure SSH authentication instead.

## Configuring SSH authentication

SSH authentication uses *asymmetrical cryptography*. Basically, you need a pair of keys, a private key and a public key, that are related in some mathematical way. The public key can be used to encrypt messages, but only the private key can be used to decrypt them. This is basically how it is used for client-server authentication:

* GitLab (the server) uses your public key to encrypt a message and sends you the cipher.
* Your computer (the client) uses the private key to decrypt the message and send it back to the server.
* The server checks that the message you sent it is, indeed, the one it encrypted in the first place. If so, he knows that you have the private key that corresponds to the public key he has in store: you are authenticated, it's business time.

Hence, you need to have your pair of keys, and you need to send your public key to GitLab. Everything else is done under the hood. Let's do this!

### Generating your SSH keys

If you already have an SSH key that you want to use, you can (of course) skip this step. Otherwise:

* If you're on Windows: Go to `Settings > App > Optional features` and check that the OpenSSH Client is installed. If it is not, you may click the `Add a feature` / `+` button, scroll down to OpenSSH Client, and click `Install`. Using Scoop (see above) is a very convenient alternative: just run `scoop install open-ssh` and you're ready to go.

    * If you're on Linux or MacOS, you already have OpenSSH installed.

* From the Terminal (Linux/MacOS) or the PowerShell (Windows), run either `ssh-keygen -t ed25519` or `ssh-keygen -t rsa -b 4096` (Ed25519 and RSA are two different cryptosystems: Ed25519 is considered safer, but the additional parameter in the second command tells the tool to create a 4096-bit RSA key, which is basically [unbreakable in the foreseeable future](https://security.stackexchange.com/questions/90077/ssh-key-ed25519-vs-rsa)).

    * When asked for the full path of the file in which the key should be saved, just press Enter to keep the default path.
    * When prompted, enter (twice) a passphrase for your key. You technically can just press Enter so as not to set any passphrase, but this additional layer of security is strongly recommended. (By default, you will have to type this passphrase every time a command such as `git fetch`, `git pull`, `git push`... is run. However, there is a way to alleviate this, as we shall see in a few seconds.)
    * Done! You are told where your public and private keys are, plus a long string of seemingly random characters and a weird abstract ASCII art. You may totally ignore these last two things.
    * If you are afraid about this "having-to-type-your-huge-passphrase-100-times-a-day" thing, good news are coming your way. What if you were only asked your passphrase once per session? Pretty simple: just type `ssh-add ~/.ssh/<name-of-the-private-key>` (where the name of the private key should be either `id_rsa` or `id_ed25519`). This will definitely make your life easier. You may now say "good night" to your private key: this should be the last time you explicitly use it.
        * If you're on Windows, you might face an error, reading something like "Error connecting to agent: no such file or directory". This is because the SSH agent does not start automatically. [Here is how to fix this.](https://stackoverflow.com/a/74879997)

### Giving your public key to GitLab

* Check the contents of your public key (the filename ends with `.pub`, and the whole path to this file is in the output of `ssh-keygen`): you may browse the folder in which it was saved and open the file with any text editor, or display it in your terminal/shell using a command like `more`, or `less`, or `cat`. The key should begin with `ssh-rsa` or `ssh-ed25519` and end with something like `<your_username>@<the_name_of_your_computer>`. Copy the whole think. (The example shown below is an SSH key that I never used and has been erased, for safety reasons.)

![](../assets/img/01-setup/ssh-keygen.jpg){: .mx-auto.d-block :}
> Folder `/Users/mmalanda` is my home folder, also known as `~` by the terminal. My public key is the whole line that starts with `ssh-ed25519` and ends with `mmalanda@sparemac-az`.

* Log on to GitLab (that's still `gitlab.inria.fr`), click on the round picture on the upper-left corner, then "Edit profile".

![](../assets/img/01-setup/ssh-key-1.jpg){: .mx-auto.d-block :}

* In the menu that appeared on your left, click on "SSH keys".

![](../assets/img/01-setup/ssh-key-2.jpg){: .mx-auto.d-block :}

* Paste your public key in the large "Key" text box. Title can be anything you want (if you need to work on several computers, you should give names that will clearly tell *Future You* which key comes with which computer). Keep "Authentication & Signing" in the "Usage type" dropdown. As for the expiration date, it is tempting to just click on the `(x)` button to remove it altogether, but keep in mind that a good SSH key, just like a good password, should be changed regularly.

![](../assets/img/01-setup/ssh-key-3.jpg){: .mx-auto.d-block :}

* Click "Add key", *et voilà*.

* Later on, you will just have to "tell" GitLab that SSH authentication is what you want, instead of HTTPS authentication: as we shall see pretty soon, this is done when you [initialize a fresh Git project]({{'/02-creating-projects' | relative_url }}) or when you [clone an existing project]({{'/03-linear-git-project#clone' | relative_url }}). We will come back to it.

For the moment, everything is set up. Breathe in. Take a few seconds to appreciate how far you have gone and reconnect with your senses.

Breathe out.

# Optional: Configuring VSCode {#vscode}

{: .box-success}
**VSCode** is a very good code editor with thousands of extensions, including parsers and linters for pretty much every programming language you can think of. It offers a lot of features with a pretty simple and readable interface.

VSCode is by far the most used developer environment tool, with nearly three quarters of respondents to the [2024 Stack Overflow Developer Survey](https://survey.stackoverflow.co/2024/technology#1-integrated-development-environment) declaring that they use it. Later on, this tutorial will show you [how to use Git from VSCode]({{'/07-vscode' | relative_url }}).

For the moment, let us configure VSCode for the use of Git... or maybe not: **VSCode knows Git**.

Is Git installed on your computer? Great! Let us open VSCode and have a look at the menu on the left:

![](../assets/img/01-setup/vc-logo.jpg){: .mx-auto.d-block :}

See that third icon, the weird branching stuff? This is the button to access the version control menu. Bam, done. We will get back to it later on.

----

Everything is ready! [**Time to create our first project.**]({{'/02-creating-projects' | relative_url }})

You may also go back to the [detailed table of contents]({{'/index#toc' | relative_url }}).
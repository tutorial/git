---
title: "Branches and issues"
subtitle: "Where paths split and meet again."
tags: 
---

Are you ready to drift even further from the perfect world we were living in, like, a few thousand words ago? As a refresher:

* We started by assuming that no two people would ever be working on the project at the same time. In this ideal world, it was enough to just pull, make every change we wanted, commit whenever we wanted, and just push our commits all at once before leaving office.
* We then went a bit closer to the real world, with people working at the same time on the same project. We learned what a pull conflict is, and how we can resolve such conflicts.

However, in both cases, there is a fundamental feature of Git that we have ignored: **branching**. Coupled with issues, a pretty simple feature of GitLab, branching is your number one ally for efficient development. You can learn about each of these features separately; however, I think the simplest way to wrap your head around each is to see how they can efficiently interact in a standard setting.

Wait, what I wrote above was an understatement of the truth. Let us frame it the other way around:

{: .box-error}
Using neither Git's **branches**, nor GitLab's **issues**, is how you get a project off the rails.

I went down this path once, and the only reason the project could recover was that it was still pretty small, very few people were working on it, and I had kept pages and pages of notes (and even then, there were missing pieces that we had to painstakingly find and put back into place).

Let us make this less abstract.

# What you should not do {#not-do}

Imagine that you have been working with other people on a project, and your team managed to create a first working version. However, you really feel like this experimental feature you have been thinking about for months would be incredibly valuable for the software as a whole.

Knowing nothing about branches and issues, or maybe just deciding that you do not really care, you start developing your new feature on your working copy. You know that, once it finally works, you will just have to go through the usual "commit-and-push" ritual.

Well, here are some drawbacks of your method:

* You are working on a collaborative project, yet ***no one will be able to work with you on this new feature***, because your work is purely local. This is likely an absolute waste of resources.
* Once you start making changes to your working copy, you have to *only* work on your new feature, either until it is ready to commit and push, or (this unfortunately happens) until you have to scrap the idea and dismiss everything you have done. ***What if you have to go back to the `main` branch***, in its current "shared" state, for helping fix an urgent issue, or collaborating on another specific development, or just because there are other features on which you would like to work once in a while? The workarounds would involve a lot of copy-pasting files and creating temporary folders, not to mention that it would be incredibly risk-prone. (For instance, delete the wrong folder and all your local work is lost.)
* Let's assume that you managed to design the entire feature by yourself, and are ready to push it. When it is finally pushed, ***not a single collaborator got an opportunity to tell you whether your work is well-done, but everyone now has to deal with it***, as it becomes **the** current version of the codebase. For all you know, maybe it does not even work properly on their computers, or maybe there are some glaring issues that you did not notice, or no one actually wants/needs this new feature.

Let us solve all these problems, starting with the one that you should have considered from the very beginning: is your feature actually wanted/needed, and if so, can some of your collaborators help you with it?

# Using issues {#issues}

{: .box-success}
On GitLab, an **issue** is how you propose a feature, report a bug, and discuss new ideas. Issues can be loosely compared to forum threads, or topics in subreddits, but loaded with a lot of tools designed by and for developers.

{: .box-note}
The word "issue" is a misnomer: creating an issue is not something you only do when you encounter a problem. One could argue that the very name of this feature may hinder its proper use by inexperienced users. Once again, **issues are to be used for actual problems with the code, but also for feature proposals or even discussing fresh ideas**.

If you work with GitLab, **even if you are working alone on a project**, you **have** to use issues, if only because it is the best and safest way to keep track of changes that should be made, features that should be added, feedback from users, ideas that you may want to explore later. I am not one to give orders, but again, this one is so important that I really have to burn it in your retinas:

{: .box-warning}
**Using GitLab issues is a must-do.**<br/>It is not merely about convenience: it must become a working habit.<br/>Basically, you should open an issue every time there is something that, in your opinion, should be handled, thought about at some point, and/or discussed with your collaborators.

To make this clearer, we will create a few issues on our dumb bread project.

## Bug report

Mike, one of the members of the project, has tried making his bread using our incredible recipe, and it flat out (pun not intended) did not work, as the dough just would not raise. Of course, he wants to warn everyone that there is a problem, but he will not just send e-mails to everyone: first, he does not have the e-mail addresses of everyone involved, and second, he knows this:

{: .box-success}
**Pretty much everything related to a GitLab project should be discussed on GitLab.**

This makes sense: what if you need to retrieve some information about a bug that occurred a few months ago, and cannot remember whether this specific thing was discussed on GitLab, or by e-mail, or on Discord? GitLab provides you with everything you need to discuss these matters in an efficient way, and everything is neatly archived there.

So, time for an issue!

Mike goes to the homepage of the project (in our case, it is `https://gitlab.inria.fr/bakery/bread/multigrain-baguette` -- remember, this project is in a subgroup in a group, hence the pretty long URL), and sure enough, in the left menu, there is an "Issues" button:

![](../assets/img/04-branches-issues/A-bug-report/bug-report-1.jpg){: .mx-auto.d-block :}

Oh, wait, there are two "Issues" buttons. Time for something a bit surprising (at first) in the new GitLab interface:

<div class="box-warning" markdown="1">
* The "Issues" button in the "Pinned" category is for all issues, from all projects, in which you are involved.
* The "Issues" button in the "Plan" category are those for the current project. **For opening an issue in a specific project, you should use this one.** 
</div>

Clicking on the "Issues" button in the "Plan" category brings Mike to the list of issues in the project.

![](../assets/img/04-branches-issues/A-bug-report/bug-report-3.jpg){: .mx-auto.d-block :}

Perfectly empty. GitLab begs him to open the very first issue of the project. He is happy to oblige.

![](../assets/img/04-branches-issues/A-bug-report/bug-report-4.jpg){: .mx-auto.d-block :}

Pretty clear. The "Type" dropdown only gives two options: "Issue" and "Incident". Not sure, he would rather keep it an issue.

{: .box-note}
**Incidents** are a specific type of issues designed to handle service disruptions or outages. Once an incident is open, actions can be tracked and timed, and subtasks can be created. This is likely not a feature you will need to use. Just remember that **a bug is not an incident**. Incidents are for very urgent matters, to which researchware is often immune by nature.

The description of the issue can use Markdown, which can be a very nice touch: this will make the issue more readable. Mike writes something like this:

```
## How it occurred

I followed to the letter the recipe provided by file `recipe-v1.txt` in the root folder. However, [this step](https://gitlab.inria.fr/bakery/bread/multigrain-baguette/-/blob/main/recipe-v1.txt#L7) did not succeed: after more than an hour, the dough still had its original size.

## Possible fixes

* No quantities are mentioned in the recipe. I may not have used enough yeast (1 tbsp for 1 oz flour).
* I live in a very cold place and the heater in my kitchen broke. Maybe the dough was below "room temperature"?
```

He then scrolls down:

![](../assets/img/04-branches-issues/A-bug-report/bug-report-5.jpg){: .mx-auto.d-block :}

Not sure about what stands there, he decides not to touch anything else and posts his issue.

![](../assets/img/04-branches-issues/A-bug-report/bug-report-6.jpg){: .mx-auto.d-block :}
> Please do not hesitate to contact me for any Photoshop-related requests.

## Interacting with issues

A bit after that, not knowing what happened to the project while I was looking away, I open the page of the project and notice something:

![](../assets/img/04-branches-issues/B-interact-with-issues/issue-handling-1.jpg){: .mx-auto.d-block :}

This "1" on the left... It used to be a "0", right? There is a new issue. Is it on this project? Time to open the "Plan" category:

![](../assets/img/04-branches-issues/B-interact-with-issues/issue-handling-1bis.jpg){: .mx-auto.d-block :}

Yep, it is indeed. I check this issue, and decide that I should reply as soon as possible (after all, this is an important issue):

![](../assets/img/04-branches-issues/B-interact-with-issues/issue-handling-2.jpg){: .mx-auto.d-block :}

However, as project manager, I should probably replace this issue with a better one, stating that the improvement that is actually required is adding quantities to the recipe, and choosing who will be in charge of it.

Of course, going back to the issue list...

![](../assets/img/04-branches-issues/B-interact-with-issues/issue-handling-3.jpg){: .mx-auto.d-block :}

...it is not empty anymore. I can click the "Open", "Closed" and "All" buttons to filter out the list of issues; when I open the issue list, only open issues are displayed. I will now create a new issue, mentioning the one opened by Mike as related (this pointer might be convenient later).

{: .box-success}
**Mentioning issues** is made using the `#` character. This can be done in any issue or *merge request* ([which we will see later]({{'/04-branches-issues#mr' | relative_url }})), be it in the description or discussion.

As a matter of fact, as soon as I type a `#`, a small dropdown appears:

![](../assets/img/04-branches-issues/B-interact-with-issues/issue-handling-4.jpg){: .mx-auto.d-block :}

By default, a list of the 5 latest open issues is displayed (unless there are less, of course). Just clicking on the one I want to reference is enough, and I can then go on writing the issue description. (If the issue you are searching for is not in the dropdown list, just type its number.) I can also mention Mike here:

{: .box-success}
**Mentioning projects or project members** is done using the `@` character.

If I type one or several letters after the `@` character, the items in the dropdown will be refined:

![](../assets/img/04-branches-issues/B-interact-with-issues/issue-handling-5.jpg){: .mx-auto.d-block :}

Mentioning other project members can be done in the description of any issue or merge request, as well as in every comment or thread below. Mentioned members will get notified, which is extremely convenient when you want to get specific input from collaborators, or even general opinions. As is also the case on tools such as Discord or Mattermost, an `@all` tag exists, to be used only in case of stringent need.

As a project maintainer, I am also interested in the options below the description box:

![](../assets/img/04-branches-issues/B-interact-with-issues/issue-handling-6.jpg){: .mx-auto.d-block :}

* By checking the small box, I can make my issue **confidential**, so that guests (i.e., simple users -- [click here]({{'/02-creating-projects#roles' | relative_url }}) if you need a refresher) will not be able to see it. Issues directly related to code internals (algorithmic details, refactoring tasks...) may very well fall into this category, while no feature proposal should ever be made confidential.
* The **assignee** will be the collaborator in charge of handling the issue. Said issue will appear in his to-do list, one of three buttons on the upper left part of the GitLab window:

![](../assets/img/04-branches-issues/B-interact-with-issues/gitlab-notif-buttons.jpg){: .mx-auto.d-block :}

> From left to right: open issues that are assigned to the current user, [merge requests]({{'/04-branches-issues#mr' | relative_url }}) assigned to the current user and/or to be reviewed by them (more on that later), to-do list (automated alerts, notifications, etc. that are not part of the first two categories).

* **Milestones** are a convenient feature for more mature projects. Issues and merge requests can be grouped under a milestone, so that the advancement of the set of all tasks required to reach a given objective (a new feature being fully implemented, a release-ready version...) can be tracked. This will not be discussed here, but it is a very convenient and pretty easy feature to use.
* **Labels** are maintainer-defined strings that can be used in order to sort out the issues. The use of labels makes it easier for managers to distinguish between urgent and low-priority issues, and for developers to directly reach issues on which they might be most helpful. Frequently used labels include `doc`, `bug`, `feature_request`... but the "best" set of labels is indeed project-dependent. (Labels are also important if your team wants to use an **issue board** for easier project management: think of it as [an automated Kanban or Scrum board](https://docs.gitlab.com/ee/user/project/issue_board.html).)
* **Due date** is exactly what it says on the (metaphorical) label. Reminders will be sent automatically to people involved with the issue when its due date is closing in.

For illustration purposes, this in an example of what an issue list can look like on an active project:

![](../assets/img/04-branches-issues/B-interact-with-issues/issue-handling-7.jpg){: .mx-auto.d-block :}

> Symbol 🕓 indicates a milestone. Custom colors can be chosen for tags, so as to make issue browsing even easier for project collaborators (typically, with bright, warm colors for urgent or important matters and less hostile colors for non-urgent tasks).

{: .box-note}
You do not have to use all of these features from the get-go, but as a project gets bigger and involves more people, you might find that using some of them actually makes the whole project workflow better.

In general, assigning issues tends to be a good practice, but assigning an issue to someone who is not even aware of it leans on dictatorial management, which you want to avoid at all costs.

{: .box-success}
More often than not, someone will assign an issue to themself as a way of telling everyone **"I am taking charge of this task and starting working on it"**. Issues that are currently not being addressed should not be assigned, and an issue should not be assigned to someone without their approval.

This is, in a nutshell, why assigning a task to yourself just requires clicking a button: in most cases, you are the person to which you will be assigning tasks. Only when the matter has been discussed before, and one of your collaborators is explicitly willing to be assigned to an issue, should you open the "Assignee" dropdown menu.

As for our "quantities" task, well... I am ready to take charge of it. A simple click and it's done. Here is our fresh task, simply called `#2`:

![](../assets/img/04-branches-issues/B-interact-with-issues/issue-handling-8.jpg){: .mx-auto.d-block :}

This `#1` in the issue description automatically turned into a link to the page of the corresponding issue: this will be the case for a lot of things. Linking between related issues and/or merge requests is fully automated, which proves incredibly convenient when a project gets larger.

Further down the page are two more "little things" that are highly enjoyable as a user:

![](../assets/img/04-branches-issues/B-interact-with-issues/issue-handling-9.jpg){: .mx-auto.d-block :}

The "Activity" part will list everything that happens to this issue, including: being assigned to someone, being mentioned in another issue, being mentioned in a merge request, being closed or reopened, and so on.

As for the "Linked items", we are here in a perfect use-case: I want to state that this issue is closely linked to issue `#1` (actually, in this case, issue `#1` should not be handled before my new issue has been handled, because (i) solving issue `#2` might actually solve `#1` as well, and (ii) if someone else tries solving issue `#1`, they may try and solve the exact problem that issue `#2` is addressing).

A few clicks and keystrokes later:

![](../assets/img/04-branches-issues/B-interact-with-issues/issue-handling-10.jpg){: .mx-auto.d-block :}

And if I go back to the page for issue `#1`:

![](../assets/img/04-branches-issues/B-interact-with-issues/issue-handling-11.jpg){: .mx-auto.d-block :}

How beautiful.

Before jumping through the next hoop, let me state once again the importance of issues (last time, pinky promise):

{: .box-warning}
If you are not using issues, you definitely will lose track of what needs to be fixed or improved, you will get several people working at the same time on the same thing, problems in your codebase will be left unchecked for months (or until the app crashes and no one has any idea why)... Basically, issues **are** how you manage your GitLab project. If you do not use them, you will lose efficiency and accumulate technical debt, even if you are working solo.

Okay, nice. I now have an issue assigned to myself. Time to work on it!

# Working on a feature branch {#feature}

Let us start with the basics:

{: .box-success}
A **branch** is a line of development that can evolve independently.

This is a pretty basic and abstract definition, for a pretty good reason: a more explicit (and valid) definition of branches would require delving into [the internals of Git]({{'/08-internals' | relative_url }}). Telling you that a branch is a *lightweight movable pointer to a commit* will not help you understand how you are supposed to use them, right?

But still, this high-level "independent line of development" non-definition probably does not help you either. What does it mean in practice? Why is branching ***the*** feature of Git? Here is another non-definition that will hopefully make it clearer:

{: .box-success}
By creating a **branch**, you can start working on brand new (sometimes experimental) features with the guarantee that you will not mess with the work on the main line(s) of development. If the features developed on this branch end up working, they can be merged into the main codebase. Otherwise, the branch may just be discarded without posing any threat to the codebase.

*(Branching exists in a lot of modern version control software. However, and this is where Git truly shines, creating a new branch with Git is an incredibly fast operation: no duplicating files, no weird shenanigans, your branch can be created in the blink of an eye... and merging branches is pretty fast and comfortable too, [as we shall see]({{'/04-branches-issues#mr' | relative_url }}).)*

"Okay", you say, "so... It wasn't difficult enough yet, so we have to add a new layer of complexity, is it what you're saying?"

Indeed, this will make things more complex. However, I cannot overstate how convenient and powerful branches are with Git, and the use of GitLab actually alleviates part of the difficulty.

{: .box-note}
One can use Git while ignoring what branches are, but this is a terrible strategy in most cases.<br/>Small project, small number of collaborators who meet on a weekly basis, clear objectives, linear schedule, low-risk project? If you check every single one of these boxes, then branches are not stringently required. Otherwise, you should *really* consider a project workflow that uses branching in one way or another.

In our example, we are not 100% sure that the whole project will be put on hold by every collaborator until our changes can be safely added, tested and validated. This is where branches shine:

{: .box-success}
Although there are multiple uses of branches, one of them should become a habit as quickly as possible: **Every issue should be handled on a dedicated branch.** 

This simple sentence solves pretty much every potential problem that was addressed above:

* When you work on an issue on a dedicated branch, your WIP has no impact whatsoever on the versions of the codebase that live on other branches, including the main branch.
* The branch is created locally, but you can push it to the repo at any moment so that other people can collaborate on the task.
* Switching between branches is very easy, and Git has a lot of mechanisms in place to ensure that you will not lose work in the process.
* When your feature is fully implemented on a branch, you can initiate the process of merging your contents with the contents of the main branch, and require that your changes be reviewed by one or several collaborators. (It is even possible to run automated tests on the merged code before pushing the changes to the target branch, but this is a story for [another time]({{'/05-good-practices#cicd' | relative_url }}).)

Time for action.

<div class="box-success" markdown="1">
**Switching between branches** is performed via the `git switch` command.
* `git switch branch_name` switches your working copy to the branch named `branch_name`, provided that it exists. The full state of your working copy is changed accordingly.
* `git switch -c branch_name` ***c***reates a new branch named `branch_name` (provided that no other branch with the same name already exists) and switches your working copy to this branch.
</div>

{: .box-warning}
A few years ago, command `git checkout` was used for everything, including opening a fresh branch and switching between existing branches. You will definitely find it in a lot of tutorials and Stack Overflow threads. The problem was that `checkout` became a very ambiguous `git` command: `switch` was created to (partially) alleviate this problem, and in my opinion, you should absolutely use it for branch creation and switching. For reference, here are a few `checkout` commands: `git checkout branch_name` is the equivalent of `git switch branch_name`, while `git checkout -b branch_name` is the equivalent of `git switch -c branch_name`.

Here, I will create a branch named `add-quantities` (a pretty fitting and self-explanatory name, should I say). First, let us assume that I forgot about this `-c` thing:

![](../assets/img/04-branches-issues/C-feature-branch/feature-branch-1.jpg){: .mx-auto.d-block :}

Well, that makes sense: `add-quantities` does not exist yet. But hey, there's another thing: I'd rather make sure that my branch is created from an up-to-date version of the project! Given that I already encountered conflicts [while working on a single branch]({{'/03-linear-git-project#conflict' | relative_url }}), who knows what could happen with several branches?

![](../assets/img/04-branches-issues/C-feature-branch/feature-branch-2.jpg){: .mx-auto.d-block :}

Some changes were indeed pushed on the main branch since the last time I pulled, great. And now I have my new branch. Just to be sure, a simple `git status` never harmed anyone:

![](../assets/img/04-branches-issues/C-feature-branch/feature-branch-3.jpg){: .mx-auto.d-block :}

Note that this branch is purely local for now. I am actually willing to solve the issue by myself. If this was not the case, I could just push my branch so that everyone can see it... could I?

![](../assets/img/04-branches-issues/C-feature-branch/feature-branch-4.jpg){: .mx-auto.d-block :}

Alright, this "fatal" error is nothing too bad: basically, `git push`, without any argument, will only work if the branch already exists on the origin. Otherwise, I have to specify that I want to create the corresponding branch on the repo, and for that, I just have to copy-paste what Git just told me (because that's how nice Git is):

![](../assets/img/04-branches-issues/C-feature-branch/feature-branch-5.jpg){: .mx-auto.d-block :}

A lot of things happened here, but most of the contents of this log can be skipped. There are basically two important pieces of information here, all contained in the lower half of the message:

1. Branch `add-quantities` was indeed created on the repo, and is tracked by my local `add-quantities` branch (i.e., other people can now push their own changes on this branch, and I should make proper use of `git pull` and `git push` exactly as I would on the main branch -- or any other branch, for that matter).
2. I am even given a link that I can copy-paste in my browser to request that this branch be merged. No need to keep it somewhere, because (i) Git will provide me with this link every time I push something, and (ii) I can also do it directly from the main page of the project in a few clicks, this is nothing but a handful shortcut.

# Switching branches {#switch}

I have been working on my feature branch for some time now, but I am not quite finished yet. However, a colleague asks me to go back to the `main` branch for a quick fix: the `README` of the project now includes a draft copyright notice that does not look quite right.

![](../assets/img/04-branches-issues/D-switch-branch/switch-1.jpg){: .mx-auto.d-block :}

What I have to do is switch back to `main`, fix this part of the `README` file, commit and push, then go back to my feature branch. Fine, let us use `git switch`.

But before scrolling down, can you guess what will happen? Remember, I told you that, by default, Git will not let you lose changes. As I have uncommitted changes on `add-quantities`, what will happen here?

Well, here you go:

![](../assets/img/04-branches-issues/D-switch-branch/switch-2.jpg){: .mx-auto.d-block :}

Git lets you switch to `main`, but tells you that you have changes (that's the `M` symbol) in `recipe-v1.txt`. In our case, no changes were made in this file on the `main` branch that Git knows of, so that it will just take your uncommitted changes and carry them to the other branch. See? Your work has not been erased!

However, you are still taking risks. There are still a few words hinting at this in the paragraph above: *"that Git knows of"*. Maybe there were changes made on the `origin/main` branch that you have not pulled yet, and if you try and pull them, you may have to solve a conflict.

Plus, you probably do not want your changes to be carried over to `main`: they were to remain on the feature branch for the moment, and you do not want to push them on the main branch by mistake!

There are two simple ways to avoid carrying uncommitted changes to another branch:

* `git stash` and `git stash pop` can be respectively used to store all uncommitted changes "somewhere" and reapply them to your working copy. The workflow in our situation would then be: `git stash` (save all uncommitted changes in a fresh stash entry), `git switch main`, do whatever you want on the main branch, then `git switch add-quantities` to go back to the feature branch, and `git stash pop` to reapply your changes.
  * Just be wary of the fact that the stash is a [stack](https://en.wikipedia.org/wiki/Stack_(abstract_data_type)): Git will not prevent you from calling `git stash` several times in a row, and if you lose track of what you are doing, you may reapply changes at the wrong place, or call `git stash` 3 times, then `git stash pop` only twice, and wonder where some of your work has gone.
  * You might want to use `git stash save "This is what I have done here"` so as to get the memo later on and know which changes you should apply where. [More on "advanced" stashing here.](https://www.atlassian.com/git/tutorials/saving-changes/git-stash) Stashing is still pretty convenient in a lot of situations, for example if you want every commit to contain code that actually compiles.
* If you are afraid of stashing, well... just commit your changes. Duh, right? Well, see above: there are changes you do not want to commit yet. The choice is yours, and it will depend on your tastes and on the situation at hand.

We switched from `add-quantities` to `main` with ease; let's just go back to `add-quantities` (still carrying our uncommitted changes with us), commit our changes, and only then, go back to `main`:

![](../assets/img/04-branches-issues/D-switch-branch/switch-4.jpg){: .mx-auto.d-block :}

`Nothing to commit, working tree clean` is basically your greenlight for switching to another branch without taking any risk. As for the fact that your commit on the feature branch is not pushed, remember that it's perfectly okay! This only means that, for now, this commit only exists on your computer. This can even be a desirable situation (for advanced use).

Time to safely switch back to `main` and (as usual) start by pulling the most recent changes:

![](../assets/img/04-branches-issues/D-switch-branch/switch-5.jpg){: .mx-auto.d-block :}

I can now, as usual, make my changes with whichever editor I enjoy using, commit them, push the commit, yadda yadda. Now is as good a time as any to learn what `git diff` does, because it is a nice one to know about:

![](../assets/img/04-branches-issues/D-switch-branch/git-diff.jpg){: .mx-auto.d-block :}

There are uncommitted changes in `README.md`, so `git diff README.md` shows me what I changed in this file since the last commit. In red, what disappeared. In green, what appeared. (This is a simple line-by-line diff; there are tools that make a better job, but this one is quick and simple.)

Basically, `git status` and `git diff <path_to_file>` are two great commands when you want to know where you're at, and make sure that you commit exactly what you want to.

Now that I know everything is fine, I can commit and push my changes to `main`, before switching back to my feature branch and get back to work:

![](../assets/img/04-branches-issues/D-switch-branch/switch-6.jpg){: .mx-auto.d-block :}

This is where a green box is probably needed:

{: .box-success}
As a general rule, **you do not want to switch to another branch when there is uncommitted work in the current branch**. There are two ways of avoiding this situation: committing your changes before switching, or stashing your uncommitted changes.

In particular, when using the former strategy, you will always be working on a branch "as if it were the only branch", like what we did when only the `main` branch existed.

...expect, of course, when you **decide** that it is time to merge branches.

# Merge requests {#mr}

I just finished the changes I wanted to make, and committed them. This is now what `git status` tells me:

![](../assets/img/04-branches-issues/E-merge-requests/merge-1.jpg){: .mx-auto.d-block :}

My new feature is now fully implemented, and should be integrated into the main branch. However, I will not be merging locally, but instead rely on GitLab's functionalities, for several reasons:

* I get a nice clean web interface that makes the process way simpler than having to type the right commands.
* I can submit my code to collaborators who can review my changes and suggest fixes if needed (which is a very important feature for actual code: maybe I could have used more adapted data structures or algorithms, or the way I wrote my code does not fully comply to the coding standards of the project).
* Finally, a lot of automatic checks can be performed so as to ensure that I did not break the software in some way: this is called a CI pipeline, and is an advanced feature that will be [introduced later on]({{'/05-good-practices#cicd' | relative_url }}).

{: .box-success}
A **merge request** (or MR) is the mechanism by which a member of a GitLab project proposes that the changes made in a branch be incorporated in another branch. An assignee and a reviewer can (and should!) be picked for a MR; the former will act as a "manager" of sorts for this MR, while the latter will be in charge of checking the actual changes and suggest fixes if necessary.

{: .box-note}
On GitHub, this is called a Pull Request, or PR for short. Do not let it confuse you: these are essentially the same thing.

## Opening a MR

As the whole merging process will be carried out on the server side, the most recent version of my works must be sent to the repo (otherwise, how would GitLab be able to see it?): time for pushing.

![](../assets/img/04-branches-issues/E-merge-requests/merge-2.jpg){: .mx-auto.d-block :}

As I push, I get once again a URL that I can just copy-paste in my browser to get on the right page. I can also go to the page of the project, click on "Merge requests" in the left-hand menu...

![](../assets/img/04-branches-issues/E-merge-requests/merge-3.jpg){: .mx-auto.d-block :}
> ...the one in "Code", **not the one in "Pinned"!** Once again, "Pinned" is for all projects, the rest is for the current project only.

...then click on one of the big blue buttons there. As I just pushed on a branch, GitLab actually suggests something:

![](../assets/img/04-branches-issues/E-merge-requests/merge-4.jpg){: .mx-auto.d-block :}

This leads me exactly where the URL from before would have landed me. Otherwise, there is still a more generic "New merge request button" that leads me here:

![](../assets/img/04-branches-issues/E-merge-requests/merge-5.jpg){: .mx-auto.d-block :}

From here, I just have to pick `add-quantities` as the source branch and `main` as the target branch, indicating that the changes that appeared in the former should be merged *into* the latter, and I land in the same place:

![](../assets/img/04-branches-issues/E-merge-requests/merge-6.jpg){: .mx-auto.d-block :}

* I can write a more descriptive title than the one proposed by GitLab (usually, the name of the commit if there is a single commit on the branch, or a slightly more readable version of the name of the branch itself), and I definitely should.
* If there is still some work to be done on the branch, I can mark the MR as draft, which will prevent its merging for now. At some point in the future, I will mark it as ready, so that merging will be possible. 
* I should add a description (once again, in Markdown format) in order to explain what I did: typically, what the purpose is, what the main changes are, which issues it solves or may solve...

In the description, I should absolutely mention issues `#1` and `#2`, as the actual merging will resolve `#2` and probably help with `#1`. We will say that it will **close** `#2` and is **related to** `#1`. And, once again, GitLab can help me with this:

* If I write `#1` and `#2` anywhere in the MR description, a link to this MR will appear on the pages of both issues...
* ...but wait, there's more! If I write something like `Closes #2` in the description, then this issue will be automatically closed when the merging occurs.

This is not a magic trick, but a GitLab feature that is enabled by default. Indeed, delving into the settings of the project (*Settings > Repository > Branch defaults*), this is what I can find:

![](../assets/img/04-branches-issues/E-merge-requests/merge-7.jpg){: .mx-auto.d-block :}

Unless somebody (with the rights to do so) unchecked this box, this is the default behaviour:

{: .box-success}
When an MR has the default branch as target, **all issues that are mentioned in the description of this MR using `Close #xx`, `Fix #xx`, `Resolve #xx`, `Implement #xx`, or [variations on these keywords](https://archives.docs.gitlab.com/15.11/ee/user/project/issues/managing_issues.html#default-closing-pattern) will be automatically closed when the MR is closed**, i.e., when merging is performed. (Without specific action from the maintainers, the `main` branch, being the only branch created in a fresh project, is the default branch.)

This is what my MR could look like at this point:

![](../assets/img/04-branches-issues/E-merge-requests/merge-8.jpg){: .mx-auto.d-block :}

> Notice that I chose not to use bullet points in the "Related issues" section. The reason is actually pretty sad: `* Closes #2` is currently not handled as it should by GitLab. The word "Close", or "Fix", etc. has to be at the very beginning of a line.

You may also mention other related MRs, here or in issues:

{: .box-success}
**Mentioning MRs** is made using the `!` character. As for issues, this can be done in any issue or merge request, be it in the description or discussion. Note that issues and MRs have independent numberings: we will be opening MR `!1` here, and we marked it as related to issue `#1`.

Of course, any other information that could help my collaborators understand what is in this MR (which changes are core changes, which pieces of my code could probably be improved and deserve specific attention, which algorithmic choices were made and why...) should be written down. In my case, there is no such need: changes are pretty straightforward and limited to a single file, with no side effects.

But my work is not over: scrolling down, I notice that there are a few more hoops to jump through before opening the MR.

![](../assets/img/04-branches-issues/E-merge-requests/merge-9.jpg){: .mx-auto.d-block :}

* The **assignee** is the one person who will manage this MR, check that everything goes right and, most likely, be the one to finally click the "Merge" button. Depending on the project, it can make perfect sense to assign "your" MR to someone other than yourself. Here, I will just assign it to myself.
* The **reviewer** will get a notification, asking them to, well, review the code. They should be either an expert on the specific topics addressed by the changes to be merged, or a maintainer who knows the whole codebase like the back of their hand -- better yet: both at the same time. The reviewer of a MR should never be someone who actively contributed to it. Here, I will be asking my evil doppelganger to review the code.
  * Even when you are working on a solo project, it can make sense to pick yourself as reviewer, as the review request will then be part of your GitLab to-do: you can then come back to it a few hours/days/weeks later and give your changes a fresh look.
* **Milestones** and **labels** serve the exact same purpose as for issues, and the milestones/labels at your disposal for MRs are the same as for issues.
* You can choose to automatically **delete the branch after merging it**, which makes sense when you created it specifically for developing a feature to be integrated to the main branch. Still, for future purposes, I will uncheck this box for now. (Deleting a branch that was merged is very easy from the GitLab web interface anyways, so that we can deal with this later.)
* You may want to **squash commits**, that is, turn all the commits you made on the feature branch into a single one. This makes perfect sense in my case, because my changes would have nicely fit into a single commit (the only reason I have two is that I had to switch back to the main branch at some point and wanted to make sure that I would not lose some work or make some mistake in the process). Let us check this box.

{: .box-success}
If important changes were made in several files, squashing commits is probably a bad idea, as it will make it way harder to point to a specific change if you need to do so in the future (which happens more than you would hope). However, cluttering the history of the project with tens of tiny commits is also a terrible idea. (If you worked alone on a branch, there is a way to reorganize all commits on the branch *before* pushing it and opening a MR, which [we will see later]({{'/09-advanced#irebase' | relative_url }}).)

{: .box-note}
All of this is related to the core idea that **commits should be atomic**: ideally, each commit would accomplish a clear, well-delimited, easily explainable task or subtask.<br/>On the one hand, a commit that adds several unrelated functions, possibly in different files, at the same time, should probably have been split (and if it also makes some of those accessible from the API of the software and changes the layout of the user interface in irreversible ways, well... good luck with that if anything goes wrong in the future).<br/>On the other hand, creating one commit in which you define the template of a new method, one in which you half-implement it, a third one in which you define unit tests for it, and a fourth one where you implement the corner cases, will just clutter the history of the project.

One click later, here it is: our first MR, in all its unfathomable glory! A pinnacle of purity, a work of art the likes of which the Earth has never-- uh oh.

![](../assets/img/04-branches-issues/E-merge-requests/merge-10.jpg){: .mx-auto.d-block :}

## Solving merge conflicts

Did you think that pull conflicts would be the only conflicts we would encounter? Time for **merge conflicts**. In this case, changes were made on the main branch that conflict with our own.

GitLab gives you a choice between "Resolve locally" and "Resolve conflicts". The former option basically amounts to GitLab opening a small window with instructions about what you should do, on your computer, to resolve things. This is made easier when using [an IDE like VSCode]({{'/07-vscode#branch' | relative_url }}), but for the moment, we will avoid that. Let us click on "Resolve conflicts" instead and hope that GitLab will hold our hand:

![](../assets/img/04-branches-issues/F-merge-conflicts/merge-conflict-1.jpg){: .mx-auto.d-block :}

Well, it does! There is actually a single conflict, GitLab shows the version on `add-quantities` and the version on `main` side by side, and if I just want to pick one version and discard the other one altogether, I just have to click a button.

However, I would like to keep my version, but with the Fahrenheit-to-Celsius bit integrated to it. Notice, however, that this is the "Interactive mode", with colors and buttons and stuff. What if I pick "Edit inline" instead?

![](../assets/img/04-branches-issues/F-merge-conflicts/merge-conflict-2.jpg){: .mx-auto.d-block :}

Alright, now we're talking! I can now edit this the old-fashioned way, like I did [when I got that pull conflict earlier]({{'/03-linear-git-project#conflict' | relative_url }}):

![](../assets/img/04-branches-issues/F-merge-conflicts/merge-conflict-3.jpg){: .mx-auto.d-block :}

> At this stage, GitLab will not let me go back to the interactive mode, because that would discard the changes I just made. As a rule of thumb, if every conflict can be solved by picking one of the two conflicting versions, you should go full interactive mode for efficiency. Otherwise, you will have to solve every conflict "by hand" in the inline edition mode.

At the bottom of the page, I just have to edit the commit message if I want to, then commit.

![](../assets/img/04-branches-issues/F-merge-conflicts/merge-conflict-4.jpg){: .mx-auto.d-block :}

There we go! Not that terrifying, right? Now we have GitLab's greenlight:

![](../assets/img/04-branches-issues/F-merge-conflicts/merge-conflict-5.jpg){: .mx-auto.d-block :}

However, this greenlight is to be taken with a grain of salt: we are still waiting for a review before merging. There's a "Merge" button, just below, that looks very tempting, but we will have to wait.

Enter... the reviewer.

## Reviewing a MR

A while ago, I showed you three buttons that appear on the upper-right corner of the GitLab webpage, for the issues, MRs and other to-do list items that have to be handled, respectively. Well, our reviewer has them too:

![](../assets/img/04-branches-issues/G-merge-review/review-1.jpg){: .mx-auto.d-block :}

And of course, among the review requests, they have this:

![](../assets/img/04-branches-issues/G-merge-review/review-2.jpg){: .mx-auto.d-block :}

> They may also access it from the "Code" category on the project page, of course.)

Clicking on the link will just send them to the MR page itself, but what they can do to start the review process is just go to the Changes tab, below the title and basic information:

![](../assets/img/04-branches-issues/G-merge-review/review-3.jpg){: .mx-auto.d-block :}

We haven't talked about this yet, but there are indeed three different tabs on a MR page:

* The Overview tab is where activity is displayed and collaborators discuss the MR.
* The Commits tab gives you, what else, a list of all commits that are to be integrated to the target branch.
* The Changes tab gives you a nice and clean view of these changes and lets you write remarks, and even suggest fixes. This is where the reviewer does their job.

![](../assets/img/04-branches-issues/G-merge-review/review-4.jpg){: .mx-auto.d-block :}

A few remarks here:

* The number displayed aside the name of the tab is the number of files that were changed in one way or another (added, deleted, or modified contents). The number of actual red and/or green blocks that have to be read tends to be way greater, as there might be several changes in a single file, of course.
* Changes are all displayed below one another on the same page. For the reader's convenience:
  * If there are two or more changed files, they are displayed in a hierarchical way in a sidebar on the left-hand side of the window.
  * If there are more than 6 unchanged lines between two changes in the same file, only 3 lines around each change are shown for basic context, and clickable arrows make it possible to display more unchanged lines in case of need.

![](../assets/img/04-branches-issues/G-merge-review/review-5.jpg){: .mx-auto.d-block :}
> Here is an example taken from another project (it is okay if you have no idea what the actual code is about). Clicking the arrow just below "101" on the left, or below "104" on the right, would show me around 20 more lines (lines 102-122 for the file displayed on the left, which are the exact same lines as lines 105-125 on the right).

These are useful for navigating the source code, but then, when you actually want to review the MR, there is basically one thing you have to know: a button will appear when you move your cursor in one of the columns that display line numbers.

![](../assets/img/04-branches-issues/G-merge-review/review-6.jpg){: .mx-auto.d-block :}

By clicking it, you will be able to type a comment about the specific line you picked. For instance, if you want to ask a simple question about this change from "warm" to "lukewarm" on line 1, just click on the button that appears when you hover over line 1 in the right column: this will open a text box for you to type your question.

![](../assets/img/04-branches-issues/G-merge-review/review-7.jpg){: .mx-auto.d-block :}
> The buttons above the text box itself are for tex formatting, except one that we will learn more about in a few seconds.

This question will be the first item in your code review, so just click the "Start a review" button, *et voilà*:

![](../assets/img/04-branches-issues/G-merge-review/review-8.jpg){: .mx-auto.d-block :}

> Note that clicking the "Add comment now" would just comment on this line, which would appear in the Activity page of the MR, but not as part of your review. More often than not, this is not what you want to do, because review items are more than simple comments, as we shall see.

Further down the changes, there is something else that bothers you: multiple uses of the full word "teaspoon" in lines 3 to 5. This could be replaced with "tsp." everywhere, right? Well, let us help as much as we can.

First, you can select lines 3 to 5, not just a single line: instead of clicking the button, just drag it to select multiple lines.

![](../assets/img/04-branches-issues/G-merge-review/review-9.jpg){: .mx-auto.d-block :}

Second, once you have stated what should be done in your opinion, you can also directly help by suggesting a change. For that, you can click on the very first button above the text box:

![](../assets/img/04-branches-issues/G-merge-review/review-10.jpg){: .mx-auto.d-block :}

This will insert the text/code you have selected before, surrounded with specific tags that you can absolutely ignore. Fix what you want to fix in this part:

![](../assets/img/04-branches-issues/G-merge-review/review-11.jpg){: .mx-auto.d-block :}

then "Add to review":

![](../assets/img/04-branches-issues/G-merge-review/review-12.jpg){: .mx-auto.d-block :}

I promise the person who requested the review will thank you for that, more than you probably think. For now, as you go on scrolling down the page, you see nothing more that should be improved or fixed, and end up stubling on this, at the very bottom of the page:

![](../assets/img/04-branches-issues/G-merge-review/review-13.jpg){: .mx-auto.d-block :}

You may have a look at the comments that are part of the review if you wish. Once this is done, it is time to click on "Finish review":

![](../assets/img/04-branches-issues/G-merge-review/review-14.jpg){: .mx-auto.d-block :}

You can provide a summary comment if you want, or maybe approve the MR. By default, the latter serves no purpose, but a project can be set in such a way that approvals are required before merging. Let us just type a nice comment and approve for the sake of it:

![](../assets/img/04-branches-issues/G-merge-review/review-15.jpg){: .mx-auto.d-block :}

{: .box-note}
If you want more information about what should and should not be done in the context of a code review, I highly recommend [this article by Simon Tatham](https://www.chiark.greenend.org.uk/~sgtatham/quasiblog/code-review-antipatterns/), a software engineer whose website is a huge collection of great stuff.

One click later, our review is submitted, and our job is done! What does it look like from the outside?

## Fixing and accepting a MR

Okay, someone reviewed my MR, **what a surprise**. So, back to the MR page, I already have some information that appeared on the upper-right corner: there are two *unresolved threads*.

![](../assets/img/04-branches-issues/H-accept-mr-and-threads/mr-threads-1.jpg){: .mx-auto.d-block :}

{: .box-success}
**Threads** are a way of discussing specific points in an issue or MR. A thread can be opened by posting a comment and specifying that it has to open a thread, and ***each comment that is part of a review opens a thread***. The page of an issue or MR always indicates the number of unresolved threads, if any.

> Threads can also make our workflow more secure. Let me explain: noticed how our current MR is still marked as "Ready to merge", despite the threads that were opened by the reviewer and are not resolved yet? If you want to avoid this (and this is probably what you should do, no matter what), go to the MR part of the project settings:

![](../assets/img/04-branches-issues/H-accept-mr-and-threads/mr-parameters-1.jpg){: .mx-auto.d-block :}

> then find and check this box:

![](../assets/img/04-branches-issues/H-accept-mr-and-threads/mr-parameters-2.jpg){: .mx-auto.d-block :}

> It does exactly what it says: merging is prohibited as long as threads are open, so that, in particular, every part of the review has to be taken into account!

Okay, back to our reviewing task. Let us scroll down a bit:

![](../assets/img/04-branches-issues/H-accept-mr-and-threads/mr-threads-2.jpg){: .mx-auto.d-block :}

This is just a simple question, and I am sure of the answer. I shall just answer "yes, indeed" and mark the thread as resolved.

![](../assets/img/04-branches-issues/H-accept-mr-and-threads/mr-threads-5.jpg){: .mx-auto.d-block :}

A simple click on "Add comment now", and I am done with this one (as the "Resolve thread" box is checked):

![](../assets/img/04-branches-issues/H-accept-mr-and-threads/mr-threads-6.jpg){: .mx-auto.d-block :}

Anyone will be able to unresolve this thread if there is any concern about this specific point.

Scrolling down again, the next item in the review is a bit more involved:

![](../assets/img/04-branches-issues/H-accept-mr-and-threads/mr-threads-3.jpg){: .mx-auto.d-block :}

I could be worried, but this "Apply suggestion" button is pretty good news! Let us scroll down a bit:

![](../assets/img/04-branches-issues/H-accept-mr-and-threads/mr-threads-4.jpg){: .mx-auto.d-block :}

Remember this suggestion thing? This is how it helps collaborators: changes that are suggested by reviewers can be directly applied, as one or several commits, and the corresponding threads are automatically closed! And this is why, as a reviewer, you should always consider this option. By doing this, you are not forcing anyone to accept your suggestion: they can still discuss it with you, in the dedicated thread, in order to reach a compromise or improvement... or they can make their own changes, commit and push them, then request a second review from you.

<div class="box-note" markdown="1">
Note that the green and red colors in the two images above do not represent the same thing.

* On the top part, the code block represents the part of the MR that the thread is about: it shows the difference between the source branch and the target branch (before the suggestion was made).
* On the bottom part, the code block is all about the suggestion made by the reviewer: it shows the additional changes that are suggested on the target branch (i.e., the code from the source branch does not appear here).
</div>

I completely agree with this suggestion, so that I just have to click the "Apply suggestion" button:

![](../assets/img/04-branches-issues/H-accept-mr-and-threads/mr-threads-7.jpg){: .mx-auto.d-block :}

> Instead of clicking the button itself, you may choose to click the arrow on the right of it and choose option "Add suggestion to batch". Doing so with other suggestions makes it possible to group these suggestions into a single commit.

A fresh commit is created from this suggestion, ready to be pushed. It just needs a proper "name":

![](../assets/img/04-branches-issues/H-accept-mr-and-threads/mr-threads-8.jpg){: .mx-auto.d-block :}

I can now apply it, which results in this:

![](../assets/img/04-branches-issues/H-accept-mr-and-threads/mr-threads-9.jpg){: .mx-auto.d-block :}

The thread was automatically closed, and a new commit named "Replace teaspoon with tsp" has just appeared in the MR:

![](../assets/img/04-branches-issues/H-accept-mr-and-threads/mr-threads-11.jpg){: .mx-auto.d-block :}

Also notice the "All threads resolved!" thingy. As I basically agreed with everything my reviewer had to say, and no other problem or possible improvement or disagreement was identified (otherwise, at least one thread would still be open!), I may choose to merge right now. Depending on your actual workflow, you may also request a review from another collaborator, or change your project settings so that one or several approvals are necessary before merging, or... This is pretty much up to you, at this point! But you should, **at least**, always request a review before merging anything to the main line of development.

For now, time to merge!

![](../assets/img/04-branches-issues/H-accept-mr-and-threads/apply-merge-1.jpg){: .mx-auto.d-block :}

What I have decided so far: I will squash my commits (they are all about a single "feature" and do not change a large number of lines and/or files), and I will not delete the source branch (...just in case). I may as well check the "Edit commit message" box at this stage:

![](../assets/img/04-branches-issues/H-accept-mr-and-threads/apply-merge-2.jpg){: .mx-auto.d-block :}

The contents of these text boxes are the messages that GitLab wants to use. If I am OK with those, I can just go on without a care. Otherwise, I can of course change them.

Note that, despise the absence of a final "s" in "Edit commit message", two commits will actually appear. Alright, I guess, as long as the final result is the one we expect. Also, issue `#2` will be automatically closed (nice), and our MR will still be mentioned in issue `#1`.

Also, ten points if you correctly guessed what appears directly below these text boxes:

![](../assets/img/04-branches-issues/H-accept-mr-and-threads/apply-merge-3.jpg){: .mx-auto.d-block :}

GitLab and its big blue buttons 💓 Click it, and and after a few seconds, you will get this:

![](../assets/img/04-branches-issues/H-accept-mr-and-threads/apply-merge-4.jpg){: .mx-auto.d-block :}

And, back to the issue list page:

![](../assets/img/04-branches-issues/H-accept-mr-and-threads/mr-closed-issue.jpg){: .mx-auto.d-block :}

Our first closed issue, and we did not click a single "Close" button. Depending on how our discussion with Mike turns out on issue `#1`, we might decide to close that one "by hand" (typically, if solving issue `#2` indeed solved Mike's problems).

# Commit graph {#graph}

If you can visualize what our project now looks like and how branches interact with each order, congratulations! Otherwise, do not worry: Git knows, and GitLab will ask Git and provide you with even nicer drawings.

## Getting the graph from Git

First, let us have a look from our personal computer:

{: .box-success}
The **history**, or even the **graph**, of a Git project can be displayed in a terminal by using variants of the `git log` command. A common (and convenient) command is `git log --graph --oneline`.

There would be a lot to unpack here, but in a nutshell: `git log` is how you ask for the history (as seen from the branch you are currently on), option `--graph` asks Git to draw a basic graph with this information, and `--oneline` specifies that each commit should be displayed on a single line. (Honestly, `git log --graph` without the `--oneline` option yields something that is pretty hard to read.)

<div class="box-note" markdown="1">
A good practice, if you want to get a nice commit history by typing a command in your terminal, would be:

1. Playing with the `git log` command and its many options ([here is a nice reference](https://git-scm.com/book/en/v2/Git-Basics-Viewing-the-Commit-History)), until you find the format that suits you most.
2. Creating an alias for the whole (probably pretty long) `git log`-based command that you enjoy. If you stumble upon somebody typing a Git command that you never heard about (most probably `git hist` or a variation of it) and getting a commit graph in return, this is exactly what they did. [Click here to see how to set Git aliases](https://git-scm.com/book/en/v2/Git-Basics-Git-Aliases).

On my computer, I used a command that looked like `git config --global alias.hist 'log --pretty=format:'%h %ad | %s%d [%an]' --graph --date=short'`, so that I can use `git hist` as an alias for this Gosh-awful command (and the `--global` option means that I can use this alias on all my projects).
</div>

Alright, so, for now, `git log --graph --oneline` it is:

![](../assets/img/04-branches-issues/I-graph/graph-1.jpg){: .mx-auto.d-block :}

> These 7-character strings at the beginning of each line are **commit hashes** -- for all intents and purposes, automatically assigned commit identifiers. It is actually very nice to have them when you want to use more advanced features, like [interactive rebasing]({{'/09-advanced#irebase' | relative_url }}) or [cherrypicking]({{'/09-advanced#cherrypick' | relative_url }}).

I know, it has been a long time since we left our working copy, but we were still on branch `add-quantities` indeed! Let us switch to the main branch, use our log command again, and see how it goes. (Ten more points if you already guessed that something would be wrong *again*.)

![](../assets/img/04-branches-issues/I-graph/graph-2.jpg){: .mx-auto.d-block :}

Right, our own personal Git has no reason to be aware of the changes that were made since the last time we actually fetched changes! We know what to do about this: pull from the repo, then back to logging.

![](../assets/img/04-branches-issues/I-graph/graph-3.jpg){: .mx-auto.d-block :}

At last! We can now see the feature branch, but also how it got squashed into a single commit, and how this was merged back into the main branch.

"Hey", you ask, "why do we have this triangle thing at the top?" Because that is exactly what we asked for: all commits replaced with a single one in our feature branch, and then, this branch being merged into the main branch.

We could have made this history perfectly linear: our `cfc35aa` commit would then be on top of all others, in a nice straight line, with no additional merge commit. This would be called a "fast-forward merge", and honestly, this 101 tutorial has already delved into 201 territory several times 😉 More on that [in the extra contents]({{'/09-advanced#rebase' | relative_url }}) for those of you who *have* to know.

This was how you get commit graphs from your terminal. Let us now have a look at the graph given by GitLab.

## Getting the graph from GitLab

Just go to the left-hand menu, from any page of the project, and go to *Code > Repository graph*:

![](../assets/img/04-branches-issues/I-graph/graph-4.jpg){: .mx-auto.d-block :}

Let's say the graph you get there is... not exactly the same?

![](../assets/img/04-branches-issues/I-graph/graph-5.jpg){: .mx-auto.d-block :}

So, a few things to unpack there:

* The only thing that appears here, but not in the last commit graph we got using `git log`, is this purple branch. Our `git log` command did not show it because **the `add-quantities` branch is no longer part of the actual history of the `main` branch**. What GitLab did when merging, with the "squash" option enabled, was create a fresh commit on a new unnamed branch, and merge *that* branch into `main`. The GitLab graph will still show us the `add-quantities` branch: to him, this branch is part of the history of the main branch, by virtue of simply stemming from it, even though not a single commit of branch `add-quantities` was integrated to the main branch.
* The graph may look like branch `add-quantities` was born twice, but this is not the case: the branch was created from the commit with message "Fix some conjugation" (which is represented with a full line), *then*, at some later point, the main branch was merged into it (with is represented with an arrow, at the level of the commit named "Keep the Celsius conversion in the recipe"). This is the moment when we had just opened our MR, and GitLab warned us about merge conflicts: *what we did was basically merge parts of the main branch into our feature branch, so that our feature branch could then be safely merged into the main branch.* Yup, we're really glad that GitLab helped us through this mess.
* Last but not least, to be perfectly honest, keeping the `add-quantities` branch does not serve any purpose now. Maybe we should have asked GitLab to delete it just after merging, after all. (Spoiler: we did not because I really wanted to show you this graph.)

# Extra: Cleaning up {#clean}

The safest way to remove a branch from a project is from the GitLab web interface. As usual, it is somewhere on the left:

![](../assets/img/04-branches-issues/J-cleaning-up/branch-cleanup-1.jpg){: .mx-auto.d-block :}

See that "Branches" button? Yup. That's the one.

![](../assets/img/04-branches-issues/J-cleaning-up/branch-cleanup-2.jpg){: .mx-auto.d-block :}

There is a button to delete merged branches, which can be pretty surprising, but (trust me) when you know more about [the internals of Git]({{'/08-internals' | relative_url }}), you understand why this is basically a risk-free and incredibly cheap operation. For the moment, we want to delete branch `add-quantities`, and there is this trash can icon on the right. How tempting is *that*?

![](../assets/img/04-branches-issues/J-cleaning-up/branch-cleanup-3.jpg){: .mx-auto.d-block :}

Remember what we just saw about `add-quantities` not having been actually merged into the main branch because of the squashing thing? GitLab sees that this branch was not merged, but does not remember why. We do, however, so let's just proceed:

![](../assets/img/04-branches-issues/J-cleaning-up/branch-cleanup-4.jpg){: .mx-auto.d-block :}

Alright, done, too late for regrets. However, guess who does not know that this branch disappeared?

![](../assets/img/04-branches-issues/J-cleaning-up/branch-cleanup-5.jpg){: .mx-auto.d-block :}

Of course, your local Git did not get the news: you have to force-feed it. Neither `git pull` nor `git fetch` will help us there, but there is an optional argument to `git fetch` that will.

{: .box-success}
Command `git fetch --prune` fetches content from the repo (the way `git fetch` does), but also deletes all local references that no longer exist on the repo, including branches that were removed. Note that branches that never existed on the repo will *not* be subject to pruning: local branches that you did not push yet are safe.

What if I use it right now?

![](../assets/img/04-branches-issues/J-cleaning-up/branch-cleanup-6.jpg){: .mx-auto.d-block :}

Okay, so. My working copy was on branch `add-quantities`, that is, my very own *local copy* of the branch, set to track the branch of the same name on the repo. Now, branch `add-quantities` does not exist on the repo anymore, and `git fetch --prune` made my local Git painfully aware of that: the branch I am working on is the ghost of a dead branch of the project.

I guess I should just switch back to `main`, and remove my local `add-quantities` branch, then:

![](../assets/img/04-branches-issues/J-cleaning-up/branch-cleanup-7.jpg){: .mx-auto.d-block :}

...well, GitLab heavily relies on Git, and the apple never falls far from the tree. I am sure, my friend:

![](../assets/img/04-branches-issues/J-cleaning-up/branch-cleanup-8.jpg){: .mx-auto.d-block :}

If you need it at some point, there is a way of getting a list of existing branches on your working copy, with some useful information attached, and it is this:

```shell
git branch -vv
```

For each branch in your working copy, you can see whether it is only local, linked to a branch in the repo, or linked to a branch that *used to* exist on the repo but is now gone. This is a simplified output I got for a project I work on, shown here for illustrative purposes:

```raw
  fix-time-shift       4092ee0 [origin/fix-time-shift: gone] Check Internet connection
  fix-deployment       68c6200 [origin/fix-deployment] Handle playbook updating
  fix-licences         126421b Update license years
```
Here, branch `fix-time-shift` used to be linked to a branch of the same name on the `origin`, but that branch is gone (probably because it was merged). Branch `fix-deployment` still has its counterpart on the origin. Finally, `fix-licences` is only local. (The remaining information is about the commit that the branch points to.)

Depending on the merging options you use in your MRs, you might end up with a lot of branches whose `origin` counterparts were deleted. [A post on Stack Overflow]() proposes a pretty radical solution:

```shell
git fetch --prune
git branch -vv | grep ': gone]'|  grep -v "\*" | awk '{ print $1; }' | xargs -r git branch -d
```

We already know the first half: make Git aware of which branches were deleted from the `origin`. What the second one does is using `git branch -vv` to get the same information as above, keep only the lines for branches whose `origin` counterpart is gone and that do not contain an asterisk (like the line for the branch you are currently on), fetch the corresponding branch names and call `git branch -d` on these.

Of course, **use this trick with caution** (but note that it will never do anything to branches that are only local).

# Extra: a note about forks {#fork-note}

Just so you know, there is this thing called *forking*, which essentially amounts to creating an exact copy of an existing project (history, branches and all) while keeping information about where it comes from. This makes it possible to exchange data both ways: the fork can be updated to follow the changes in the source project, and changes made in the fork can be pulled in the original project if they work, make sense, bring interesting features, etc.

This section is already pretty long as it is, but you should know that, even though using issues and MRs inside a single project already leads to efficient collaboration, fork-based collaboration can be incredibly powerful, especially for large projects. There is [a full page about forks and Git workflows]({{'/06-forks' | relative_url }}) to learn more about all of this... when time comes. If you already heard about forks, be reassured: I did not forget about them!

----

This was a lot of information (and both Git and GitLab offer way more features than what we have seen). With all these tools at our disposal, and the huge degree of freedom that Git actually leaves its users, it is pretty easy to still "do the wrong things". Good practices are required to ensure that our project can live and grow in the best possible way, and [**this is what the next part is about**]({{'/05-good-practices' | relative_url }}).

You may also go back to the [detailed table of contents]({{'/index#toc' | relative_url }}).
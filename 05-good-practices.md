---
title: "Good practices"
subtitle: "Where things get less messy."
tags: 
---

Congratulations: you now know enough about the use of Git and GitLab to manage your first collaborative projects 🎉

However, as a corollary, you may now have an idea of how things may get messy. Here is a "short" list:

* By default, all developers can push on all branches, including the `main` branch. What this means in practice is that, even though you explicitly told each and every one of your collaborators to create a dev branch every time they want to make a change, solve an issue, and so on, **people *will* be pushing "bad" commits on the `main` branch**.
    * Maybe someone will forget to switch on a feature branch and accidentally make changes that they will push to `main`, even though the feature is still unfinished and will make the software crash.
    * Or maybe they will be tempted not to use branches at all -- after all, they know how to write code, so there's no reason anything could go wrong... right?
    * As a bonus, you will definitely get things that are pushed on the `main` branch that do not come with very **informative commit messages** -- and yes, this definitely includes your own commits! The messages you wrote were perfectly clear to you at the very moment you wrote them, but fast-forward a few months and you will wonder what exactly this "Rewrite the `Blang3` module" commit was and how it addressed an actual problem that needed to be addressed.
* Assuming that the problem above was solved, and everyone uses issues and merge requests as they should, there is a question you need to address: **how can we trust a MR?** Asking your collaborators to follow a given set of good practices is a first step, but rules are meant to be broken, and not always because of bad will: urgency might lead you to merge changes to the `main` branch without jumping through the usual hoops, and maybe these changes will actually break your code in a subtle way. Actually enforcing some of those rules would be great -- but what if we could go even further by implementing some kind of **automated testing** or our code before it is merged in the main branch?
* Assuming that everyone uses issues and merge requests, those get archived when they are closed. This is very convenient when you have to trace back the changes in your project and see whether or not a specific problem was already addressed months or years earlier. However, **if issue and MR descriptions are not informative and/or unsorted, you will be in trouble when browsing the history of your project**.
* If you are writing researchware, you will very likely rely on some version of your software for the "Experimental results" section of a paper. Months later, you may have to face **reproductibility issues**: the codebase has changed so much that you cannot reproduce the results you already published, and finding which previous version was used for that specific paper might be pretty difficult.

These are **all** problems that I have faced, and GitLab actually helps you solve them. This is why this section is about, and it will be split into two subsections: the first one is about the settings of the project, while the second (way shorter) one is about what you can do or think about, on an individual level, when working on a project.

# GitLab project management {#project-management}

There are a lot of very convenient things that can be set in GitLab in order to prevent a lot of the problems discussed above from happening. Most of what we will see in this section will be happening somewhere around here:

![](../assets/img/05-good-practices/settings.jpg){: .mx-auto.d-block :}

You should not hesitate to take a look at everything here and think about whether some of the options may be of interest to you. In this tutorial, we will show some of the most useful ones... as well as a few things that do not involve this menu.

## Issue templates and labels {#issues}

At this stage, you probably got the message, but to reiterate, using issues is the safest way to make progress on your project:

* You can communicate efficiently, in a single place, about bugs, feature proposals, and so on.
* The fact that issues can be explicitly assigned alleviates the risk of several people working on the same thing without knowing it.
* You can choose which ones should be closed before the next milestone (typically, the next release), and GitLab provides you with simple tools for easy monitoring. (Milestones are not addressed in this tutorial, but they are very simple to use and pretty convenient. You may try as soon as your project actually approaches a milestone -- for preparing the release of your v0.1 for instance!)
* Once issues are solved (e.g., bugs were solved, features were implemented...), every piece of information about them is archived at the same place, for future reference.

However, if issues are hastily written and left to rot in a heap, all these efforts amount to nothing. To solve these problems, let us see how to use *templates* and how to tag issues with *labels*.

{: .box-warning}
I would argue that **templates are the most important feature to set up early in your project**. You may leave tags for later, but remember that they will be very useful as soon as your project starts to grow in size, age, and/or number of collaborators.

### Issue templates

If you try to open a new issue in your project, have a look at the small message that you get below the Description text box:

![](../assets/img/05-good-practices/A-issue-template/issue-templates-1.jpg){: .mx-auto.d-block :}

You even have a nice link to click, bringing you to a page on which you are explained how to [create an issue template](https://archives.docs.gitlab.com/15.11/ee/user/project/description_templates.html#create-an-issue-template). It is basically just a Markdown file, with the default contents you want to see on some issues, that you will commit to the main branch. That's it.

Or is that? You can have several issue templates, which is very convenient if you want to help your collaborators write informative issues for bugs, features proposals, refactoring tasks, or anything else!

Here is a simple example of a template for a feature request:

```markdown
## Requested feature

_Please make your request both concise and precise. Every piece of information related to the **why** should be in the next section; everything about the **how**, in the section after that._

## Needs

_As a user: what would you like to be able to do with the software?_

_As a developer: what do you need a new module/function/feature for?_

## In practice

_Every idea you may have about the feature, from code fictions to implementation details, is welcome here._
```

The contents of this file are stored in the aptly named `Feature Request.md`. Here is another one, called `Bug.md`:

````markdown
## Description

_Describe the issue here._

### Example

```
Minimal  example
on which the bug
occurs  (if  the
example  is  too
large,    please
upload  it   and
link  it  here).
```

### Expected output

_If you got an unexpected error, you may remove this subsection. Otherwise, try and be as precise and clear as possible._

### Actual output

_If you got either an error message, or a text output that was unexpected/undesired, you may provide it here:_

```
My output
```

_For other types of outputs, you may either provide a clear description, or attach a file._

## Possible causes / solutions

_If you have an idea of what exactly went wrong and/or how to fix it, please do not hesitate to provide details. Otherwise, you may remove this section._
````

These files are placed in the `.gitlab/issue_templates` subfolder of my project; I have to create both the `.gitlab` folder and its subfolder `issue_templates` at this stage. Now my `git status` is pretty clear:

![](../assets/img/05-good-practices/A-issue-template/issue-templates-2.jpg){: .mx-auto.d-block :}

Great, I am already on the default branch and I just have to add the `.gitlab` folder as a whole:

![](../assets/img/05-good-practices/A-issue-template/issue-templates-3.jpg){: .mx-auto.d-block :}

Oh no. I am on Mac OS (...shameful, I know), which creates a lot of `.DS_Store` files everywhere, and I do not want them on the repo, do I? Fortunately, Git tells me what to do to unstage a file. Let's follow the instructions (that's `git restore --staged <file>`), and remember that we would *really* like some files like this one to be left out of the repo by default (who knows, maybe we will do something about it [pretty soon]({{'/05-good-practices#gitignore' | relative_url }})).

![](../assets/img/05-good-practices/A-issue-template/issue-templates-4.jpg){: .mx-auto.d-block :}

Okay, better. After a simple `git commit -m "Add issue templates"; git push`, I can go back to the webpage of my project and create an issue. Then, the "Add description templates" lines has disappeared, and I can use a nice dropdown for my description:

![](../assets/img/05-good-practices/A-issue-template/issue-templates-5.jpg){: .mx-auto.d-block :}

The names of the templates are the exact names of the files I pushed, without the extension. If I choose "Feature Request", the content of the template instantly appears in the Description text box:

![](../assets/img/05-good-practices/A-issue-template/issue-templates-6.jpg){: .mx-auto.d-block :}

Let us preview it:

![](../assets/img/05-good-practices/A-issue-template/issue-templates-7.jpg){: .mx-auto.d-block :}

How nice. I can now just go back to the Write tab and replace what I have to replace, and... done!

The better the templates, the better the issue descriptions: a good template should be clearly organized (use titles and subtitles -- the `#` character is your best friend in Markdown) and ask the right questions in the right order, so as to ensure that the person opening the issue will provide everyone with all needed information and/or explanations.

Green box time:

{: .box-success}
**Issue templates** are simple templates that anyone who opens an issue can choose from. They help users write issues efficiently, enforce a standard format for issue descriptions, and help distinguish between various types of issues (bugs, feature proposals, improvements, etc.)<br/>To create an issue template, just write it as a Markdown document, save it (as a `.md` file) in the `.gitlab/issue_templates` subfolder of the project, and commit to the default branch (which is `main` if you did not explicitly change it).

### Labels

Our issues can also be tagged, which is a very convenient feature of GitLab (among others). One or several tags can be added to each issue, they can be easily changed if needed, and not everyone agrees on how to put these to use.

The most common use of tags is to state which parts of the software and/or its life cycle an issue refers to. These can be pretty generic tags like:

* `backend` / `frontend` / `doc`;
* `feature proposal` / `bug` / `performance` / `memory usage` / `CI/CD` (more on this last one [later on]({{'/05-good-practices#cicd' | relative_url }}));
* a pointer to the most simple and quick tasks;
* tags precisely identifying parts/features of the specific software you are working on, like module names, or steps in the particular data flow the software implements...

Your project, your rules! Just remember a few basics about issue tags:

* The tags themselves should be short but precise enough, so that choosing which tags apply to a given issue is always a simple task. Also, searching for all issues that have a given tag is very simple: this is a nice way for developers to only see those issues that they can easily contribute to.
* When you create a tag, you will be able to choose a color for it. Think about a simple color code that will be easy to parse. For instance, red should only be used for bugs and/or urgent issues. Also, maybe you want to use the same "non-hostile" color for all tags that describe which parts of the software an issue is about.

{: .box-warning}
I have seen projects in which `todo`,`doing` and `done` tags were created, but one could argue that these are supposed to be the statuses of open issues with no assignee, open issues with an assignee, and closed issues, respectively.

{: .box-note}
If you want to use [GitLab Issue Boards](https://docs.gitlab.com/ee/user/project/issue_board.html) (which give you a [Kanban](https://www.atlassian.com/agile/kanban/boards)-ish view of project issues), tags like `In dev` and `In review` can be used. The Issue Board can then be set up so that four columns are displayed: the Backlog (with all unassigned open issues), the "In dev" and "In review" columns (with all open issues with the corresponding tags), and the "Closed" column.

In practice, creating and using tags is very simple. To create and manage tags, from the menu on the left side on your project page, go to *Manage > Labels*:

![](../assets/img/05-good-practices/B-issue-labels/issue-label-1.jpg){: .mx-auto.d-block :}

If you did not define a single label in your project yet, this is what you should see:

![](../assets/img/05-good-practices/B-issue-labels/issue-label-2.jpg){: .mx-auto.d-block :}

You may then create your first label:

![](../assets/img/05-good-practices/B-issue-labels/issue-label-3.jpg){: .mx-auto.d-block :}

Alternatively, if this is your first time and you lack inspiration, you may try to click the other button you had at your disposal, which will create a default set of labels:

![](../assets/img/05-good-practices/B-issue-labels/issue-label-4.jpg){: .mx-auto.d-block :}

You can always change an existing label, add a new one, reorder them (by dragging and dropping them using the ⋮ buttons), and so on. Among the "bonus" features you get, you can also subscribe to one or several specific labels, so that you get a notification every time this label is used: this is very convenient if you are the official "critical bug solver" of the team, or if you were the sole designer of the whole code that deals with a specific feature of the software and can help with any issue addressing this feature!

Now, anyone creating a new issue will be able to pick one or several labels:

![](../assets/img/05-good-practices/B-issue-labels/issue-label-5.jpg){: .mx-auto.d-block :}

If you think that the labels on an already existing issue should be changed, notice that, on the far right of the issue page, there is a sidebar that you can actually expand:

![](../assets/img/05-good-practices/B-issue-labels/issue-label-6.jpg){: .mx-auto.d-block :}

If you do so, you will be able to change a few things to the issue, including its labels:

![](../assets/img/05-good-practices/B-issue-labels/issue-label-7.jpg){: .mx-auto.d-block :}

As for searching for labels, nothing is simpler. Just go to the Issues page, click in the search bar, and...

![](../assets/img/05-good-practices/B-issue-labels/issue-label-8.jpg){: .mx-auto.d-block :}

If I select the "Label" option:

![](../assets/img/05-good-practices/B-issue-labels/issue-label-9.jpg){: .mx-auto.d-block :}

I may then click "is", so that I will only keep issues with a given label:

![](../assets/img/05-good-practices/B-issue-labels/issue-label-10.jpg){: .mx-auto.d-block :}

And once I have chosen a label, I will only see those issues that actually bear this label! Not that useful when you just have a handful of issues, but as soon as your project stops being a very young project with very few issues (or as soon as the number of collaborators exceeds 3), this can prove very convenient.

## Preventing files from being committed {#gitignore}

Okay, while we're at it... Remember this pesky `.DS_Store` file? These automatically generated system files could end up on the repo if you are not cautious enough, and no one needs or wants that to happen.

One can also think of personal files that have no reason to be shared. I am not talking about your `passwords.txt` file. (Even though we will have to talk about it at some point, I mean... Dude.) But stuff like `.workspace` files, for all the VSCode users out there, should be yours and only yours.

Finally, maybe even more importantly, your Git repo was probably created for one or several software pieces, which you will compile and run on your computer at some point. By doing this, you will create a lot of files and folders that should not be committed: subfolders like `/build` or `/packages`; compiled code with extensions `.o`, `.byte`, `.pyc` and others; files generated at runtime with extensions `.log`, `.tmp`, `.lock`...

How to prevent these files from being committed? The main answer is called `.gitignore`. It is a simple file, at the root of the project, in which you declare which files should remain untracked. Here are several examples:

* In subfolder `src`, you have this file called `source.log` that keeps appearing but no one wants. Well, just write this line in `.gitignore`:

```sh
/src/source.log
```

* You and some of your collaborators have `.workspace` files at the root of the project, with different names. Well, the standard wildcard `*` will definitely help you:

```sh
/*.workspace
```

* As for those `.DS_Store` files, they may appear in any folder. To prevent all of them from being committed, we will have to drop the `/` (which explicitly refers to the folder in which the `.gitignore` file stands, i.e., the root of the project). Just add this line to exclude all files named `.DS_Store` (and only those: if you create a file called `something.DS_Store`, it will not be excluded):

```sh
.DS_Store
```

* An option I really enjoy is that you can exclude all files with a specific name, *except* for a few specific ones, using `!`. For example, if you want to update file `/tests/initial_campaign.log` on a regular basis, while excluding all other `.log` files:

```sh
*.log
!/tests/initial_campaign.log
```

Other possibilities (if you happen to ever need more options) are provided in [this nice page](https://www.atlassian.com/fr/git/tutorials/saving-changes/gitignore).

Once your `.gitignore` file contains what you want it to contain (no worry though, you can change it anytime you have to), just commit and push it to the main branch, so that it is available to everyone.

This is now what happens if I try to stage a `.DS_Store` file:

![](../assets/img/05-good-practices/C-gitignore/gitignore-1.jpg){: .mx-auto.d-block :}

Note that, if you *really* have to bypass an exclusion rule set by `.gitignore` for a specific file, you can use the `-f` option of `git add` to do so.

Exclusion rules also prevent you from *unwillingly* committing some files, when adding a whole folder for instance. To illustrate this, I have created a new folder called `files`, with dummy files in it, in my working copy. This screenshot of my terminal should speak for itself:

![](../assets/img/05-good-practices/C-gitignore/gitignore-2.jpg){: .mx-auto.d-block :}

(These changes will be discarded before moving on to the next topic.)

{: .box-success}
By listing all files, folders and file/folder "name templates" (the exact term is [globs](https://linux.die.net/man/7/glob), if you are a Unix nerd) in a file called **`.gitignore`** and placed in the root folder of the project, you prevent those files from being staged, committed and pushed.

{: .box-warning}
It is actually possible to create several `.gitignore` files in several subfolders of your project, but it makes exclusion rules less clear, clutters your project, and is generally considered bad practice.

On the other hand, one should note that the `.gitignore` file is meant to be pushed to the repo, thus providing rules for which files should be ignored *in every working copy*. Fair warning: this means that your collaborators might have one of their files kept out of the repo because of one of the entries in the `.gitignore` file. You might also want to exclude some of your own files, that you moved in your local copy for convenience but should not push (and yes, this includes your `passwords.txt` file... we definitely have to talk about this). In this case, what you want is exclusion rules that are local to your working copy.

{: .box-success}
You can use the exact same way of declaring what should be left out of versioning, but write it in your "local exclude file": **`.git/info/exclude`**. (If it does not exist, just create it.) As is the case for the whole `.git` folder, this file will not be pushed to the origin in any case, but it will provide additional instructions to Git on which files/folders from *your* working copy should be ignored (for instance if you `git add` a folder that contains some of them) without setting fixed rules for all developers on your project.

## MR templates and parameters {#mr}

Badly written issues were a problem we did not want to face, which is why we created issue templates. For MRs, we want to avoid the exact same problem, and we can use the exact same solution!

### MR templates

{: .box-success}
**MR templates**, just like issue templates, help users write MRs efficiently and enforce a standard format for their descriptions.<br/>To create an issue template, just write it as a Markdown document, save it (as a `.md` file) in the `.gitlab/imerge_request_templates` subfolder of the project, and commit to the default branch (which is `main` if you did not explicitly change it).

Here is a MR template I am currently using on one of my projects:

```markdown
## General aim

_Please try and be both concise and precise about the general aim of your merge request. More specific information is to be given below._

## Main changes

_Please **avoid paraphrasing the code**, as it will be reviewed before merging anyway. You should rather provide simple explanations about your works: what are the key changes? Did you have to drastically change existing functions, or to just add a layer on top of existing code? What should the reviewer(s) start with, or focus on?_

## Related issues

_Use `Closes #issuenumber` (or, if several issues are solved at once, `Closes #firstissue, #secondissue, #thirdissue`) so that corresponding issues are automatically closed by merging. If other issues are related, you may add a second line here: `Related to #firstissue, #secondissue...`_

## Checklist before submission

_If any of the boxes below is unchecked, the title of your MR should be prefixed with `Draft:`._

* [ ] _The code compiles._
* [ ] _The code is documented, and the docstrings for all fresh functions comply to the [Ocamldoc syntax](https://ocaml.org/manual/ocamldoc.html#s%3Aocamldoc-comments)._
* [ ] _Release notes were updated._
* [ ] _If the changes imply a change in UI/UX: both `README` documents were updated._
* [ ] _Related milestone(s) are mentioned (if any)._
* [ ] _At least one reviewer has been picked._
```

The item list at the very end does not rely on basic Markdown syntax, but I recommend using it in GitLab as soon as it makes sense: it creates list with check boxes.

![](../assets/img/05-good-practices/D-mr-templates-and-options/mr-template.jpg){: .mx-auto.d-block :}

### MR options

Maybe, one day, it will be possible to prevent a MR from being merged before all boxes are ticked. (At least, [the issue about this on GitLab](https://gitlab.com/gitlab-org/gitlab/-/issues/19830) gets frequent updates. It is something that can be done on GitHub and a feature that many users, including premium ones, have been asking for years.) As it is, it still serves as a useful reminder for developers, which is why I decided to include general but useful things in my template, from the very obvious ("only code that compiles") to the easily forgettable ("update all documentation").

Still, there are already a few (too few?) GitLab options that make it possible to enforce specific guardrails, so that you can decide under which conditions a MR can or cannot be merged. Let us click on the *Settings > Merge Requests* option of the project page:

![](../assets/img/05-good-practices/D-mr-templates-and-options/thread-lock-1.jpg){: .mx-auto.d-block :}

Once you know more about GitLab, you may have a look at all options there in order to really control your workflow.

<div class="box-note" markdown="1">
Among these options, one may:
* choose whether merging can be performed when the source branch is behind the target branch, and under which conditions;
* setup several MR options (close outdated threads in the MR, show the URL of the MR on the terminal when pushing on the source branch, enable the "Delete source branch" option by default);
* choose whether commits can/will be squashed when a MR is merged;
* set the default commit messages for merges...
</div>

The options that I really want to focus on for now are those in the "Merge checks" section:

![](../assets/img/05-good-practices/D-mr-templates-and-options/thread-lock-2.jpg){: .mx-auto.d-block :}

* The first two are about CI pipelines, which are briefly described [later in this tutorial]({{'/05-good-practices#cicd' | relative_url }}). In a nutshell, CI (Continuous Integration) pipelines consist of automated jobs that are performed on the codebase under certain conditions. Standard jobs include compiling the software, running it through a set of automated tests, or building the documentation pages. If you have set up a CI pipeline, you should **absolutely** check the first box, and probably leave the second one unchecked.
* The last one should be checked by default; **if it is not, check it**. As a reminder, threads are special types of comments that can be posted on the page of a MR, and they are typically used to discuss an issue with the current version of the code on the source branch, a missing feature, or other topics that should be addressed before merging (especially if the target branch is `main`). Hence, you do not want to merge a MR if threads are still open (i.e., potential problems are left unsolved).

Other options are made available to premium users. For now, we unfortunately have to be content with these few settings. Fortunately, we still have branch protection.

## Protecting branches {#protect}

There is still a huge glaring problem that we want to solve: anyone can push *anything* to the `main` branch. Thus, why would anyone go through the hassle of creating a MR, instead of just pushing their changes? They know it *should* work fine, I guess. Hopefully.

...yeah, this is how catastrophes are born.

Let us solve this problem by protecting the main branch.

{: .box-success}
A **protected branch** is a branch on which no direct push can be performed: the only way to push changes to a protected branch is by merging another branch via a MR.

<div class="box-note" markdown="1">
**For most projects, protecting the `main` branch is a necessity.** These are a few ways this will help:
* By preventing direct pushes on `main` and only enabling MRs to impact this branch, it ensures that **the branch- and issue-based workflow will be enforced**. This helps with project tracking, thanks to all the tools available in GitLab, and prevents developers from fixing problems/bugs that were never documented.
* **Project tracking is also made easier** by the fact that commits on `main` come in batches, each batch coming from a MR that was created to solve a specific issue. On an unprotected branch, commits related to different bugs/features may be intertwined, which makes the history of the project opaque.
* Coupled with testing, it ensures that **every version of the software that reaches the `main` branch works as expected**. Of course, no test suite is 100% complete, but the code on `main` should at least meet basic requirements, which cannot be enforced on an unprotected branch.
</div>

To protect a branch, just go to the "Repository" settings from the page of your project, then expand the "Protected branches" section:

![](../assets/img/05-good-practices/D-mr-templates-and-options/settings-protect-1.jpg){: .mx-auto.d-block :}

You may pick a branch here (any branch) and choose how it should be protected.

...but hey, wait a minute. Let us scroll down a bit:

![](../assets/img/05-good-practices/D-mr-templates-and-options/settings-protect-2.jpg){: .mx-auto.d-block :}

"What should I protect a branch that is already protected?", you ask. Indeed, the `main` branch is protected by default... but not as much as you would like. See, maintainers still have the right to push to the branch, and they can make mistakes too! If you really want to protect your branch, you should pick another option in the "Allowed to push and merge" dropdown:

![](../assets/img/05-good-practices/D-mr-templates-and-options/settings-protect-3.jpg){: .mx-auto.d-block :}

Now, no one *at all* can push on `main`. The only way to "push" changes on `main` is merging, and MRs can only be merged by maintainers. A very important consequence is that, now, you can ask for reviews and/or approvals for *every single change in the codebase*, meaning that every piece of code can be proofread by at least one person other that the people who actually wrote it.

You may think this is a waste, but it is actually quite the opposite. If every change in the codebase is reviewed by at least one person, then there will be less undetected bugs and less "malpractices" (such as bad indentation, obscure names for methods or variables, methods that are not declared in the right class, overly long methods that should have been split, bad separation of concerns...). This results in reduced debugging efforts and lower technical debt, so that, overall, **you are actually wasting less time and effort by adding (the right) hoops to jump through**.

{: .box-warning}
If there are very few people working on a project, physically working from the same place and interacting on a daily basis, with a very linear development process, for a software that has no users yet, then you *might* want to skip this step, so as to keep your workflow as simple as possible. **In any other case, you should *absolutely* protect `main` from any direct push.**

To impact the `main` branch of every project in a group, you can go to the settings from the page of the group. In the "Repository" settings, expand the "Default branch" section:

![](../assets/img/05-good-practices/D-mr-templates-and-options/settings-protect-4.jpg){: .mx-auto.d-block :}

You can choose another name for your initial branch (choosing a name other than `main` can make sense for [some Git workflows]({{'/06-forks#workflows' | relative_url }})), and pick default protection options for every project in this group -- these options can be overridden for any individual project, and are just the "default" options that are applied to a project when it is created in this group or one of its subgroups.

However, notice that even the strongest level of protection proposed here is the one that I insisted was *not strong enough* for our needs: maintainers can push on `main`! If you want to prevent anyone from pushing to `main`, you will have to change the settings of every individual project. Sorry about that. Still strongly recommended, and it's just a few clicks for each project, so why not?

## Tags {#tags}

In the life cycle of any software, there will be some versions that are, in a way, more important than others -- "milestones" of the project, if you will. If your software is deployed on a server somewhere and is used by people outside your team, these include the versions that are actually released. For pure researchware, the versions that are used for filling up the "Experimental results" sections of published articles are definitely important.

So, it makes sense that you would want to tag these versions somehow and, then, be able to efficiently retrieve them. Git knows. And Git provides. Let us come back to our dummy (...yummy?) project. After a `git status` to check that we have nothing waiting to be committed, let us have a look at the graph of the project:

![](../assets/img/05-good-practices/E-tags/git-tag-1.jpg){: .mx-auto.d-block :}

Oh so lovely. At this stage, I really think that the current version should be tagged. Possible reasons are aplenty: for example, maybe I just want to keep a short-lived tag so that I will be able to go back to it if anything goes wrong in the near future, or maybe this should actually be tagged as the first actual released version.

In the first case, I may use what is called a **lightweight tag**. It is, basically, just a pointer to a commit, with a nice little name. The command is simple enough: `git tag <some_tag_name>`. See for yourself:

![](../assets/img/05-good-practices/E-tags/git-tag-2.jpg){: .mx-auto.d-block :}

I was not asked my SSH passphrase, and by now, I know what that means: this tag only exists in my working copy. I could have created a commit, tagged it, and then pushed, right? Well, yes, but the situation would be the same: tags are not pushed by default by `git push`. We will have to explicitly push our tag to the repo:

![](../assets/img/05-good-practices/E-tags/git-tag-3.jpg){: .mx-auto.d-block :}

Now, anyone who pulls will get the `checkpoint` tag along with the commits. Great. Or not: maybe you wanted to keep this tag to yourself, as it serves no purpose to other people. You can absolutely do that! This is why creating a tag and pushing it to the server are two different things. This is also why you have two different commands for deleting the local tag and deleting the remote tag. This can be confusing at first, but we can do it.

Deleting a tag? Easy peasy: just add `-d` (yup, `d` for **d**elete) to your `git tag` command:

![](../assets/img/05-good-practices/E-tags/git-tag-4.jpg){: .mx-auto.d-block :}

However, if you check *Code > Tags* on the project page...

![](../assets/img/05-good-practices/E-tags/git-tag-5.jpg){: .mx-auto.d-block :}

...this is what you will get:

![](../assets/img/05-good-practices/E-tags/git-tag-5bis.jpg){: .mx-auto.d-block :}

The tag was deleted from the working copy, but still exists on the remote. You can delete it from the *Tags* webpage, using the small wastebin icon on the far right, or you can do this locally:

![](../assets/img/05-good-practices/E-tags/git-tag-6.jpg){: .mx-auto.d-block :}

Okay, cleaned up. Now, what if I want to tag this commit as my `v0.1`? I should have a bit more than just a pointer to this commit, and this is where **annotated tags** should be used. An annotated tag will contain more information, like the name and e-mail of the person who created it, a timestamp, and a tagging message. The base command is the same, but you have to add `-a` (for **a**nnotated, of course), and as a message has to be provided, `-m "My message"` also has to be added, as for commits.

Of course, this tag is important project-wise, not just for you, so do not forget to push it:

![](../assets/img/05-good-practices/E-tags/git-tag-7.jpg){: .mx-auto.d-block :}

Finally, just use `git tag` by itself, without extra arguments, to list tags, and `git show <tag_name>` to get more information about a specific tag:

![](../assets/img/05-good-practices/E-tags/git-tag-8.jpg){: .mx-auto.d-block :}

Branch `main` can still grow: you made sure that this specific version of our codebase can be easily retrieved. If, at some point, you want to have a look at it, you can use `git checkout <tag_name>`, although this will give you some weird message:

![](../assets/img/05-good-practices/E-tags/git-tag-9.jpg){: .mx-auto.d-block :}

The question of what "detached HEAD" means is one that pops up *a lot* in discussions around Git. If you just want some very pragmatic recap, here it is:

* You can basically "just have a look" at the tagged version of the code (or any commit for that matter, as checking out a commit using its hash -- the hexadecimal thingy associated with it -- is possible but will get you in a "detached HEAD" state).
* However, *changes you commit in a "detached HEAD" state will **not** be saved when you switch back to a branch, unless you create a new branch or tag that references your commit*.

If you want more information, the following info box is for you. You may skip it if you want. No hard feelings.

<div class="box-note" markdown="1">
Git deals with a data structure that is at its core a directed graph of commits, where each commit points to its parent commit (or commit*s*, as is the case for merge commits for example). To keep this graph as small as possible, Git implements a **garbage collection** which, among others, removes all unreachable objects; in particular, "hanging" commits are deleted.

The basic idea of garbage collection is reclaiming memory that is allocated, but not (or no longer) referenced. A [very common way](https://en.wikipedia.org/wiki/Tracing_garbage_collection) of doing this is by tracing which objects are reachable from certain *root objects*, and purging everything that is left (i.e., unreachable).

This is what Git does, and its root objects are *branches and tags* (both of these objects are references to specific commits). When you add a commit to a branch, the branch itself (as in "the underlying object in memory") is updated so that it now references your fresh commit. A tag, on the other hand, will always point to the same commit.

When you checkout anything other than a branch, any commits you add from this point are unreferenced: there is no branch or tag pointing to the new chain of commits you created. This is what "detached HEAD" means: where you currently are (your HEAD) is not attached to a specific branch or tag. As a result, your new commits are hanging, and if you do not solve this by creating a branch or tag that is "attached" to your latest commit, the garbage collector will remove these fresh commits next time it runs.
</div>

At this stage, we may just switch back to the main branch:

![](../assets/img/05-good-practices/E-tags/git-tag-10.jpg){: .mx-auto.d-block :}

{: .box-success}
With Git, **commits can be tagged**. There are two types of tags: lightweight tags, created with `git tag <tag_name>`, and annotated tags, created with `git tag -a <tag_name> -m "<tag_message>"`.<br/>Tags are created locally; to push them to the repo, use `git push origin <tag_name>`.<br/>You can delete a tag from your working copy with `git tag -d <tag_name>`; to delete it from the repo, you can either use `git push origin --delete <tag_name>`, or go to Repository → Tags on the GitLab page for your project.

## CI/CD pipelines {#cicd}

If you followed every step so far, you now have, among others:

* templates for both the issues and the MRs, so that they will be easier to create and should contain all relevant information;
* and a `main` branch that is protected against pushes, so that only changes made through a MR can reach it.

This does not guarantee, however, that everything that will reach the `main` branch will work as intended. Even if you really insist on the virtues of testing code before pushing it, mistakes can happen, deadlines closing in may result in people just forgetting to test their code (and other collaborators still supposing that it *must* have been done -- otherwise, the MR would be a Draft, right?), and bugs may be missed even by careful reviewers.

What if there were a battery of automated tests that ran every time some code is pushed, and prevented any faulty code from reaching the `main` branch?

Well, hold my beer.

{: .box-success}
A **CI pipeline** (where CI stands for *Continuous Integration*) is an automated process that drives software through building and testing. *The default GitLab project settings prevents a MR from being merged if the CI pipeline failed.*<br/>A **CI/CD pipeline** also handles the creation, packaging and documentation generation of a release on the repository (in this case, CD stands for *Continuous Delivery*), and maybe the deployment of this release (guess what the D stands for in this case?).

On the one hand, for most projects, the CD part is basically an FYI (just know that the basic setup for a CD pipeline is very similar to that of a CI pipeline). On the other hand, one should really consider setting up a CI pipeline as soon as possible.

The most basic requirement you should have is pretty simple: the code should compile. However, you should also consider tests, like, *a lot of them*, and you may very well plug other tools: Python developers are pretty fond of [code formatters](https://github.com/life4/awesome-python-code-formatters), for example.

Let us take a minute to talk a tiny bit more about tests, because this is something that you have to plan ahead.

* Depending on how critical it is that your code works as expected, you may require that unit tests be provided for every new piece of code (in this case, consider writing this in your `CONTRIBUTING.md` and adding a checkbox in your [MR template]({{'/-good-practices#mr' | relative_url }})). Practices like [test-driven development](https://en.wikipedia.org/wiki/Test-driven_development) can also be implemented, at least for the core features of your software.
* Unit tests are great, but you may also want other kinds of tests, such as integration and acceptance tests. The former check that your modules or classes interact as they should, while the latter check that the requirements of the software are met (if you know the exact output that you should have for a given fixed input, you can use that as a simple acceptance test). [This reply on Stack Exchange](https://stackoverflow.com/a/7672580/21698549) is a short description of the most common types of tests.

Now, considering that you have test suites that you can already run on your machine, how do you set up a CI pipeline that runs these tests? Fortunately, Inria provides you with tools and documentation to help you in your quest. [The Inria tutorial on Gitlab-CI](https://ci.inria.fr/doc/page/gitlab/) is probably where you should start, and you may cherrypick the parts you want to read, so that you just go through the most simple process:

* [Enable CI/CD for your project](https://ci.inria.fr/doc/page/gitlab/#enabling-ci-on-a-gitlab-project) (somewhere in its settings, of course);
* [Create a runner dedicated to your project](https://ci.inria.fr/doc/page/web_portal_tutorial/#getting-started) (basically, a virtual machine on which all tests will run), or [to the group itself](https://docs.gitlab.com/ee/ci/runners/runners_scope.html#group-runners) (it is shared between all projects in this group)... or do not create a runner, and just [use one that is shared by the whole `gitlab.inria.fr` instance](https://ci.inria.fr/doc/page/gitlab/#using-shared-runners-linux-only) (typically, if you know that you will only launch short jobs once in a while and can wait a bit more for the results);
* [Install `gitlab-runner` on your runner](https://ci.inria.fr/doc/page/gitlab/#installing-runners-linux-windows-or-mac-os): this is the software that will be able to connect to the GitLab project, fetch the sources, run the tests and send the results back to GitLab;
* [Register the runner](https://docs.gitlab.com/runner/register/) (you have to explicitly tell `gitlab-runner` which project it should access);
* [Start working on your CI pipeline](https://docs.gitlab.com/ee/ci/quick_start/index.html#create-a-gitlab-ciyml-file): this is done through a YAML file (the syntax is pretty simple, actually), that has to be called `.gitlab_ci.yml` and put in the root folder of the project, in which you declare the "stages" of your pipeline, and the "jobs" that compose these stages.

Your best sources of information (at least, for starters) include the [official CI/CD pipeline page](https://docs.gitlab.com/ee/ci/pipelines/) on the GitLab docs, the [keyword reference page](https://docs.gitlab.com/ee/ci/yaml/index.html) on the same docs, and the whole [CI Inria portal](https://ci.inria.fr/).

A few simple examples of job declarations for the `.gitlab-ci.yml` file are given [near the end of this very tutorial]({{'/09-advanced#gitlab-ci' | relative_url }}), but you should *absolutely* have a look at the more numerous (and better!) examples available in the [GitLab CI/CD gallery](https://gitlab.inria.fr/gitlabci_gallery). Made for you, with love.

# Everyday use {#everyday}

Let us assume that we basically set everything above. What a beautiful, nearly professional project we have. However, this does not mean that we can now do whatever we want and everything will be fine in the long run, because magic does not exist (at least, not in the world of Git and GitLab):

* Issue and MR templates are very helpful, but even if you use them, you have to think about what you will write to fill in the blanks, how you will write it so that everyone gets on the same page... and **this is even worse for commits**: there is nothing (except sophisticated [pre-commit hooks]({{'/09-advanced#git-hooks' | relative_url }}), but hey) that can be put in place in order to ensure that commit messages are "informative enough", or "clear enough".
* The most important branches of the project are protected, but you may still be able to do whatever you want everywhere else, including **absolutely messing up with the history of your feature branches**. Depending on your project setup, this may not be very risky (because of main branch protection and automated testing), but this will increase the rate at which you will have to solve conflicts and make them way harder to solve. In other words, you will be wasting time and resources on preventable problems.
* You have a functioning CI pipeline? Great! Just remember that **tests do not write/update themselves**. If you forget this simple fact, your CI pipeline will end up testing a past, probably way simpler, version of your code, which will help no one.

So, here we go: good commit messages, clean history, testing. Let's do this.

## Writing informative commit messages {#messages}

This goes without saying... which is why the only people on this Earth who never wrote terrible commit messages are those who never used Git in the first place.

{: .box-warning}
Writing good commit messages is indeed important, as there will come times when you will have to scroll down the repo history to find a specific change.  This especially includes the pretty common occurrence of discovering a bug, having to get to the exact commit that introduced it, and **understanding what this commit was about** so that you can safely fix the bug without breaking something else.

This also proves pretty difficult sometimes: commits might get terrible messages as a developer starts to lose their mind trying to fix bugs, or painstakingly refactoring a module or class plagued with years of technical debt.

![Git messages by XKCD](https://imgs.xkcd.com/comics/git_commit_2x.png){: .mx-auto.d-block :}

Please do not do this. If anything goes wrong in the future and these commits need to be inspected, someone (probably yourself) will hate you. So, once you reached a stage when you think your changes should be committed, take a few seconds to close your eyes, breathe deeply, appreciate the fact that you are making progress even if you are not realizing yet, maybe burn some incense (if you are working from home), and reflect on the changes you made. How could you describe them, clear as day, to someone other than yourself? This is what you should write after `git commit -m`.

{: .box-success}
There are several ways of writing "good" Git commits. A pretty popular one (for good reasons, in my opinion) is to think about how you would explain your commit in a single sentence starting with ***"Once applied, the changes in this commit will..."***. This forces you to use action verbs, which is an effective way of describing the changes in terms of what their direct impact on the code and/or app is.

If you can only come up with a message that is longer than a tweet (this is what they were called before Twitter became X) and/or contains three or more action verbs, this can mean two things: either you are still making it too complex by delving into technical details, or you made too many "small" changes that probably could have been contained in several commits. (It is not too late to unstage your changes, then create several "atomic" commits.)

## Keeping a clean history {#history}

Pretty advanced users who *really* start having fun with Git, or intermediate users who work on tens of heavily intertwined features at once, might get a bit too excited and run head first into the Git-ar Hero situation:

![](../assets/img/05-good-practices/guitar-hero.jpg){: .mx-auto.d-block :}

Fixing such a situation is not made any easier by uninformative commit messages that do not make it clear what each commit is actually about (see above).

The basic principle would be: **keep branching simple**. You probably do not need to open that many subbranches from a feature branch, and if you absolutely have to, your subbranch will probably be short-lived: implement a specific subfeature, test it (more on that later), then merge onto the feature branch as soon as you can.

{: .box-success}
**The clearer your development process is, the simpler your branching will be.** Basically, for implementing a specific feature, you should be able to partition the global effort into a small number of tasks (implementing prerequisites, then subfeatures), and split those tasks into simple ("atomic") subtasks. You may open short-lived subbranches for the tasks, and the commits in each subbranch should match the subtasks.

If you do so, the "commit message" problem also solves itself.

Note that opening subbranches may not even be required in the first place. For example, solving a simple issue by fixing a few typos, writing a single new method, and refactoring before pushing, does not require parallel work by several people for a whole month. In such cases, forget about subbranches: just open a feature branch, do what has to be done, and open a MR.

## Changing your last commit {#amend}

There is a very simple and safe way to fix a mistake you just did in a commit (like, I don't know, giving it a terrible name, committing something that should not be there or, on the contrary, forgetting a file). Let me introduce you to `git commit --amend`.

You want to change the commit message? Easy: that's `git commit --amend -m "Better message"` for you.

You want to add or remove a few files? Also easy: just `git add` or `git rm` anything you want, and then, instead of creating a new commit with `git commit -m "My message"`, try `git commit --amend --no-edit`. The "no edit" option just means that you do not want to change the commit message.

Of course, if you want to do both at the same time, you can replace the `--no-edit` with `-m "Here is my new commit message"`!

{: .box-warning}
As usual, **please do not do this with an already pushed commit**. You would end up in a situation where the origin and your working copy diverge, as the last commit would be different from one to the other.

In other words, this is perfect for situations when you commit changes and immediately go "aw crap" (or whatever you actually say out loud when you screw up, Git is curse-word-agnostic).

## Going further with rebasing {#further}

Once you feel at ease with Git, you may want to have a look at a pretty nice feature of Git that may help you with both issues at once: **rebasing**.

Rebasing is the act of taking a sequence of commits and moving them on top of a given branch. It can be, for example, used as an alternative to merging: instead of merging a feature branch to `main`, you can take all commits from the feature branch and move them on top of the `main` branch (with, of course, possible conflicts to resolve, as usual).

![](../assets/img/05-good-practices/merge-vs-rebase.jpg){: .mx-auto.d-block :}

> Merging versus rebasing. Source: [Filiz Senyusluler on `dev.to`](https://dev.to/filizsenyuzluler/git-rebase-vs-merge-1ph0)

This is a nice way of ending up with a linear history instead of one where several branches are opened then merged, and some projects will only allow rebasing for branches. But there is more to that: rebasing a feature branch onto its source branch will basically move your changes on top of the newest content of the source branch... which is probably where you will want to merge your changes at some point! Doing this on a regular basis (especially for long-lived development branches in active projects) is how you guarantee that your feature branch does not fall too far behind the source branch, which means, among others, that you will not have to struggle with a thousand conflicts when it is time to merge. (It also reduces the odds of having several people writing similar functions without even knowing.) The [Atlassian documentation](https://www.atlassian.com/git/tutorials/rewriting-history/git-rebase) is a pretty good starting point on this topic.

Let us go even further. Pretty conveniently, `git rebase` provides you with option `-i`, or `--interactive`, in which you are able to reorder, rename, squash and discard the selected commits before moving them on top of the specified branch. This helps make the repo history clearer, and as long as said commits were still in your working copy only, no one gets hurt.

Now, imagine you use `git rebase -i` for moving commits *where they already are*. Well, this will just let you manage your commits and clean up your history, which can be pretty nice before making your work public, right? In other words, you may very well implement things in disorder, make stupid mistakes that you fix five commits later, come back to a task because you forgot to implement unit tests... and, once everything works as expected, "tidy up your room" so that the branch you will actually be pushing has the best possible commits with clear messages. [This article on the GitLab blog](https://about.gitlab.com/blog/2020/11/23/keep-git-history-clean-with-interactive-rebase/) provides pretty good explanations. (And, before you ask, yes, this is also a way of turning terrible commit messages into readable and informative ones.) 

Ask your doctor if rebasing is good for you! Which brings us to some important advice:

{: .box-warning}
**Do not rebase something that was already pushed!** Changing the public history of a project is not only widely considered rude, but also a source of conflicts (between what a branch looks like and what your collaborators' computers think it looks like) that will only result in an even more confusing project history once the immediate problems are solved.<br/>(Advanced: The only exception to this rule is when a project was set up so that merging branches is only allowed in *fast-forward* mode. In this case, a rebase can be performed **just before merging**, and deleting the branch as soon as it is merged is then a good practice.)

You can find more about rebasing (among others) in [the "Advanced" appendix]({{'/09-advanced#juggling' | relative_url }}).

## Software development practices {#practices}

There are [a lot of good practices](https://opensource.com/article/17/5/30-best-practices-software-development-and-testing) for software development. I could provide you with hundreds of links to pages that agree on a lot of points, contradict each other on some other topics, and sometimes delve into a huge lot of low-level technical details that only the top 5% of developers will understand.

Please allow me to just write a few ones that I think everyone should know and keep in mind. The first main item on this list applies to any project that involves writing code at some point. The second one becomes more and more important as your code grows. The third one is more advanced, but it should be thought about as soon as your project has external users, and is absolutely vital if some of these users are industrial users.

* **Apply the basic principles:** YAGNI, DRY, KISS.
    * *YAGNI: **Y**ou **A**in't **G**onna **N**eed **I**t.* Do not write code "just in case we need it later". If you do not need it right now, it should not exist. Forgetting about YAGNI is how you bloat your code.
    * *DRY: **D**on't **R**epeat **Y**ourself.* The first time you have to copy-paste existing code may be fine. The second time you copy this code, paste it into a fresh function or method, and call this function/method in the rest of your code. Forgetting about DRY is how you end up having to fix bugs by applying the same change in tens of places in your codebase (and you will definitely miss one or two occurrences, which will come back to bite you at some point).
    * *KISS: **K**eep **I**t **S**imple, **S**tupid.* This applies at basically every level: do not make implementations uselessly complex, keep simple interfaces (including GUIs)... This also includes the two principles above.
* **Refactor whenever you feel like you need to.** Refactoring code may seem like a waste of time ("why would I spend two hours refactoring this module, it will just take me 10 more minutes to implement what I have to implement right now"), but it reduces [technical debt](https://en.wikipedia.org/wiki/Technical_debt). Not refactoring is how you increase that debt, and this debt not only has to be paid with interest, but also compounds itself: if you let entropy find its way and lay its eggs into several modules, the day when you will have to solve a bug that involves two or more of those modules will be a day of reckoning.
* **Write tests, and make them fail.** Okay, this one may sound weird, but I promise it makes perfect sense. Just... just bear with me here.
    * Testing your code is always a good practice. Even if testing is not fully automated via a CI pipeline, you should at least have a suite of "core" unit tests that you can run on your computer via a few simple commands (like `make test ; run test`).
    * When you add a fresh method/function, you should also (at least) write unit tests for it, and check that they are indeed run by these commands. Try and think about all corner cases that should be tested for: What if this argument is `NULL`/`None`? What if I have a special character in my string argument? What if this number is negative? What if I provide a float instead of an integer?
    * For each test that you wrote, write something that will make it fail, and check that this is indeed the case. If a test never fails, it is not a test. You should see your test fail, at least once. (I told you this would make sense.) Of course, the piece of code that makes your test fail will be erased as soon as you have indeed... "tested your test"?
    * One development practice that checks all boxes here is [test-driven development](https://en.wikipedia.org/wiki/Test-driven_development), or TDD, a process in which you: 1) write a unit test corresponding to the next thing you should implement (even if it is just "return an error if the first argument is `None`"), 2) run all tests to check that your fresh unit test fails as expected, 3) write the simplest possible code that will make your test pass, 4) run tests to check that your fresh code works, 5) refactor if needed, 6) iterate until your new code is ready to be pushed. You do not *have* to adopt TDD in all of your projects, but it is good to know that it exists.

----

Time to go a bit further on the topic of collaboration. [**The next chapter is about collaborative workflows using Git.**]({{'/06-forks' | relative_url }})

You may also go back to the [detailed table of contents]({{'/index#toc' | relative_url }}).
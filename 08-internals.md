---
title: "Extra: Git internals"
subtitle: "Where the blobs live."
tags: 
---

There already are a lot of resources about how Git works under the hood. If you have some time to delve into it, you should check out [Scott Chacon's "Git Internals"](https://github.com/pluralsight/git-internals-pdf), an excellent (although slightly outdated) tutorial.

I will try not to go too far here: my objective is to give you an overview, so that, at long last, you will have an answer to the question "Why are all these people insisting that I never track PDF files?".

# What's on my computer? {#computer}

This is the first thing you might be tempted to do: just see what you have in your working copy.

Remember that you can switch branches without having to request the repo itself, which means that your working copy contains all required information to do so. It may be outdated (this is where commands like `git fetch` or `git pull` come into play) or, more generally, out of sync (you may have local contents that are unknown to the repo, and `git add/commit/push` will help you solve this), but you already have a lot of information about commits and branches somewhere on your computer.

And by "somewhere", I mean "look no further than the hidden objects at the root of your working copy":

![](../assets/img/08-internals/git-internals-1.jpg){: .mx-auto.d-block :}

This `.git` folder appeared as soon as this folder became a working copy, either when you created it by cloning an existing project, or when you ran `git init` there. It contains quite a lot of stuff, but I have good news: it will make sense in a pretty short time.

# Git objects {#objects}

Okay, time to start from the low level.

## Blobs

The basic object stored by Git is a *blob*. A blob stores the contents of a file (it is not the file itself, though). For Git, it makes sense: if a commit changes one file out of the two thousand files of your project, why would you duplicate 1,999 files? Total waste of space.

What would be the most convenient way to give names to these blobs? A "naive" solution, like taking the whole path of the file in the project, removing special characters and appending an integer that is incremented by 1 in each commit, does not work that well. For instance, what if I just rename a file, or move it to a subfolder, between two commits? Should I duplicate this file?

Instead, Git relies on [hashing](https://en.wikipedia.org/wiki/Hash_function) to compute a nice fixed-size sequence of hexadecimal digits from the contents of the file, which will yield the name of the file. These blobs are in the `.git/objects` folder, separated in subfolders named after the first 2 digits of the hash (for reasons I will not detail here -- efficiency, basically). Here is one of those files in my working copy:

![](../assets/img/08-internals/git-internals-2.jpg){: .mx-auto.d-block :}

The contents of one of my files were hashed and its hash begins with `26d927bb21` (I decided against pasting the remaining 30 characters -- you can thank me later).

## Trees

Traditional file systems look like trees. Close your eyes, relax, breathe in, and picture a tree. Any tree. (I love weeping willows, but to each their own.) If you strip it down to the most basic structure you can think of, it is a root, from which "children" branches emerge, and these branches can have both branches and/or leaves as their own children, and so on.

Now, replace the root with *literally* the root folder `/` of your Unix-based OS (hi, Linux and Mac OS users!), branches with folders, and leaves with files, and this is your file system. Plain and simple.

On Windows, you have basically the same thing, except that each partition of each disk is its own tree: `C:\`, `D:\`, etc. are all roots of their own file systems. Instead of a tree, you have a forest. (Probably a very small one, but still a forest.)

A snapshot of the repo at a given point is just the state, at this point in time, of a folder and all its contents, i.e., a basic tree of folders and files. Git can easily describe this snapshot. Like, very easily: each tree (including the root) can be described by a simple text file that lists the files and subfolders in it, giving their type (file or subfolder, i.e., a *blob* or another *tree*), name, and where to find their contents.

(Actually, the mode, i.e., the read/write/execute permissions of the file or folder, is also provided, but hey. Also, the text files describing the trees are actually zipped, but that's about it: they are just text files. [*Keep It Simple, Stupid.*]({{'/05-good-practices#practices' | relative_url }}))

For consistency (among even better reasons), trees are also hashed, so that blobs **and** trees are described by their hashes.

## Commits

So far, I basically described Git (or, at least, the part of Git that does not handle client-server sync) as an arguably clever file system. However, a repo also has a history. In particular, a snapshot is not a commit if you do not know where it stands in the history of the project: you at least have to know which commits came before.

More precisely, a commit is a snapshot of the repo, or working copy, with metadata attached: author, message... but also, at least a parent. *At least*, because a merge commit has two direct parents (sometimes even more, but we are definitely not going there).

How do you describe this in a Gitty way?

...you probably guessed it: write the data in a text file, zip it, hash it. And, of course, the parents of the commit are described, in the text file, by their own hashes.

# History {#history}

We already had a look at several graphs of Git projects, including ones we got after merging branches. What they all have in common is that they are DAGs, or Directed Acyclic Graphs: each commit has at least one parent (except the very first one), and there is no loop anywhere in the graph (a commit cannot be one of its own ancestors, because causality exists).

The "direct parent" relationship between commits is already described in the commit objects themselves, so that every problem would already be solved if branches did not exist. However, branches are among the main features of Git, so... how does everything we have seen so far make it possible to handle branches?

This is where the `.git/refs` folder shines... but in an incredibly lame way.

![](../assets/img/08-internals/git-internals-3.jpg){: .mx-auto.d-block :}

Three subfolders: `heads` for the branches you have in your working copy, `remotes` for the ones on the repo, `tags` for the tags. Why is the first one called `heads`? Because, in order to switch to a given branch, Git only needs to know the hash of the head of this branch, i.e., the last commit. From the contents of this commit, Git knows the file structure, the contents of the files, and the whole history of the branch.

Hence, a single file that bears the name of the branch, and only contains a single commit hash, is enough to define a branch and its whole history at this point. The same is true for tags, which are also just pointers to a commit. The whole history of the repo is a handful of 40-bytes long text files.

# Deltas {#deltas}

There is still a huge aspect of Git that was not addressed yet. Imagine that you make one tiny change in a 1 MB file. From what we have seen so far, we would imagine that Git would take the new contents of the file, compare them to the blobs it already has in store (using their hashes, for efficiency), notice that these are indeed new contents, and... create a new blob weighing 1 MB to store the new contents? Come on, Git is more clever than this, isn't it?

Indeed it is. In such cases, Git will compute a delta, that is, the difference between the previous and current contents (pretty much like `diff` does with any pair of files in your standard Linux terminal), and store this delta instead of the whole file contents.

As a result, checking out a given commit is a pretty complicated operation, as it involves applying or undoing a lot of deltas, but Git knows how to handle this in a very efficient way, which is why it will not take you hours to switch from one branch to another, for instance.

It actually goes further: when you have a pretty large commit, Git will basically search for the strategy that minimizes the total size of the deltas. Imagine that you rename file `a` to `c` and file `b` to `d`, change a few things in each file, then commit the result without using commands like `git mv` in the process. Git will compare pairs (`a`,`b`) and (`c`, `d`) on the one hand, (`a`,`b`) and (`d`, `c`) on the other hand, see that the former will yield *waaaay* smaller deltas than the latter, and run with it. In cases where more files were moved, it uses very powerful heuristics to get to very smart solutions.

(There is also a packing strategy that I will not be covering here. Just remember that Git is stupidly smart.)

# Consequences {#consequences}

There are two main consequences of everything we just said. One of them is why Git is so preposterously efficient; the other one is, at long last, why PDF files (among a lot of others) should be stored sparingly in Git projects.

## Git is unreasonably efficient

To put it simply, I never stumbled upon a single occurrence of Git duplicating a file, and this is not even the most impressive thing about Git.

Story time. The most atrocious commit I ever created on a project was when I reorganized all source files in a 4-year-old software project: I renamed a few files, sorted everything in fresh subfolders, used this new file structure to plug in a new build system, added/deleted several files, and changed a few things here and there in some of the source files themselves so that the build could be successful. It was (and still is) a mess of a commit. Close to 400 files were moved, some of them had parts of their contents changed, new files were created... I was even more worried as a few changes had been committed in the meantime, which meant that (because of the project settings) I would also have to [rebase]({{'/09-advanced#rebase' | relative_url }}). I had pretty vivid nightmares the night before. How did it go?

...well, the whole thing took me 10 minutes. I had not used `git mv` once in the whole process, but Git was able to know which files were moved where, including the ones whose contents had been changed. A single conflict occurred in the whole rebasing stage, and it was a very simple one that I could solve in a handful of seconds. **This is how unreasonably good Git is.**

And this all makes sense from how it handles all this stuff. Git has no difficulty detecting files that were merely moved or renamed (because of hashes), and it can then use sophisticated heuristics to minimize the total size of the deltas for the remaining files (typically, those that were moved *and* changed). It turns out that, when changes in files are not too extensive, Git identifies in the blink of an eye which file was moved where, *even when you also created and deleted files*. Nothing short of amazing, if you ask me.

But...

## Git does not like binaries

How do you deal with non-text files, i.e., binary files? Well, it depends.

The whole delta optimization mechanics that Git uses for text files has no reason not to be applicable to binary files, but this does not mean that it will be efficient, or convenient. Git indeed uses deltas on binary files *if it is reasonable*: if a lot of changes were made into a binary file between two commits (which you often cannot control, as binary files are regenerated by some tools, not modified by your hand), then the whole thing will be stored alongside its previous version. This is how you bloat a repository.

Note that this is fundamentally not an issue with storage, [as this very good blog post explains](https://robinwinslow.uk/dont-ever-commit-binary-files-to-git). Storage is cheap. It's fine. The issue is that it will then become long, *too* long, to clone the repo or switch between branches, which can slowly murder a project.

{: .box-warning}
As a rule of thumb, **do not store binary files on a Git repo, unless they are reasonably small and not prone to changing often**. A 5-page PDF document that is only regenerated once per release every few months? If you need it, why not. The compiled version of your code, automatically generated with each commit? Please don't. Videos? *Absolutely freaking not.*

Also, you can always go to your project's page on GitLab, pick a commit, and see every change that this commit includes. Very informative on text files ("yeah, this `#include` did not serve any purpose anyway"), less so on binary files ("`6fc9a78c6` was replaced with `ae062bb4` and I have no idea whether I should care").

Last but not least, remember that, for all intents and purposes, there is no undo button for the history of a repo. Everything that exists in a pushed commit exists *forever*. In other words, remember that 100MB file you committed and pushed by accident, then removed with a `git revert` a few minutes later? Yep, your repo became 100MB (and change) heavier, and there is nothing you can do about it without taking huge risks of data loss and screwing with every single collaborator on the project.

----

But wait, there's more! [**Here is more extra stuff that one could be interested in.**]({{'/09-advanced' | relative_url }}) Advanced branch management, hints for your CI pipelines, a bit of troubleshooting, you name it.

You may also go back to the [detailed table of contents]({{'/index#toc' | relative_url }}).
---
layout: home
title: Git and GitLab
subtitle: The tutorial I wish I had a few years ago
---

<div align="center"><iframe src="https://giphy.com/embed/kH6CqYiquZawmU1HI6" width="480" height="220" frameBorder="2" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/devrock-code-edr-escueladevrock-kH6CqYiquZawmU1HI6"></a></p></div>

Hello, thank you for being here!

**This tutorial is intended as an introduction to the most useful features of Git and GitLab.** It is basically what I wish I had been taught, no less, no more, when I started working on collaborative software projects. It is meant to teach you everything you have to know for efficient software project management and safe collaborative development without scaring you!

{: .box-note}
I firmly believe that working on software projects, even for small-team researchware, without Git and/or GitLab is basically a waste of time and resources, and leads to a terribly error-prone development process.

On the other hand, I remember all these hours that I spent exploring Git and GitLab, making workflow choices that would prove inefficient and/or plain dumb, using Git from a terminal to carry out actions that can be done from GitLab in a few clicks, and feeling overall pretty confused about how this whole mess should be used in the first place.

![Git by XKCD](https://imgs.xkcd.com/comics/git_2x.png){: .mx-auto.d-block :}

This is the step-by-step guide that I wish I had been provided when I discovered Git and GitLab. It starts from the very beginning (that is, installing Git and configuring your GitLab account), and holds your hand until you know all the tools you need to efficiently maintain your development projects.

# Table of contents {#toc}

You can go through this tutorial in order, in which case I advise you to practice at the same time. [**Everything starts here.**]({{'/00-what-why' | relative_url }}) (At some point, you should also make sure that you are aware of [**some basic development practices**]({{'/05-good-practices#practices' | relative_url }}).)

If you are already somewhat accustomed to Git and GitLab, you may also directly skip to the parts that you wish to read. Here is a nice table of contents for you:

* [**What and why?**]({{'/00-what-why' | relative_url }})
  * [Git]({{'/00-what-why#git' | relative_url }})
  * [GitLab]({{'/00-what-why#gitlab' | relative_url }})

* [**Setup**]({{'/01-setup' | relative_url }})
  * [Installing Git]({{'/01-setup#git' | relative_url }})
  * [Connecting to GitLab]({{'/01-setup#gitlab' | relative_url }})
  * [Optional: Configuring VSCode]({{'/01-setup#vscode' | relative_url }})

* [**Creating projects on GitLab**]({{'/02-creating-projects' | relative_url }})
  * [Groups]({{'/02-creating-projects#groups' | relative_url }})
  * [Subgroups]({{'/02-creating-projects#subgroups' | relative_url }})
  * [Roles]({{'/02-creating-projects#roles' | relative_url }})
  * [Creating a project from scratch]({{'/02-creating-projects#create-project' | relative_url }})
  * [Pushing existing code to GitLab]({{'/02-creating-projects#push-code' | relative_url }})

* [**Working on a linear Git project**]({{'/03-linear-git-project' | relative_url }})
  * [Preamble]({{'/03-linear-git-project#preamble' | relative_url }})
  * [Cloning a project]({{'/03-linear-git-project#clone' | relative_url }})
  * [Repository, staging area, working copy]({{'/03-linear-git-project#repo' | relative_url }})
  * [Fetching vs. pulling]({{'/03-linear-git-project#fetch-pull' | relative_url }})
  * [Conflict handling]({{'/03-linear-git-project#conflict' | relative_url }})

* [**Branches and issues**]({{'/04-branches-issues' | relative_url }})
  * [What you should not do]({{'/04-branches-issues#not-do' | relative_url }})
  * [Using issues]({{'/04-branches-issues#issues' | relative_url }})
  * [Working on a feature branch]({{'/04-branches-issues#feature' | relative_url }})
  * [Switching branches]({{'/04-branches-issues#switch' | relative_url }})
  * [Merge requests]({{'/04-branches-issues#mr' | relative_url }})
  * [Commit graph]({{'/04-branches-issues#graph' | relative_url }})
  * [Extra: Cleaning up]({{'/04-branches-issues#clean' | relative_url }})

* [**Good practices**]({{'/05-good-practices' | relative_url }})
  * [***GitLab project management***]({{'/05-good-practices#project-management' | relative_url }})
    * [Issue templates and labels]({{'/05-good-practices#issues' | relative_url }})
    * [Preventing files from being committed]({{'/05-good-practices#gitignore' | relative_url }})
    * [MR templates and parameters]({{'/05-good-practices#mr' | relative_url }})
    * [Protecting branches]({{'/05-good-practices#protect' | relative_url }})
    * [Tags]({{'/05-good-practices#tags' | relative_url }})
    * [CI/CD pipelines]({{'/05-good-practices#cicd' | relative_url }})
  * [***Everyday use***]({{'/05-good-practices#everyday' | relative_url }})
    * [Writing informative commit messages]({{'/05-good-practices#messages' | relative_url }})
    * [Keeping a clean history]({{'/05-good-practices#history' | relative_url }})
    * [Changing your last commit]({{'/05-good-practices#amend' | relative_url }})
    * [Going further with rebasing]({{'/05-good-practices#further' | relative_url }})
    * [Software development practices]({{'/05-good-practices#practices' | relative_url }})

* [**Forks and workflows**]({{'/06-forks' | relative_url }})
  * [***Forks and merge requests***]({{'/06-forks#fork' | relative_url }})
    * [Forking]({{'/06-forks#forking' | relative_url }})
    * [Merge request]({{'/06-forks#fork-mr' | relative_url }})
    * [Closing remarks]({{'/06-forks#fork-closing' | relative_url }})
  * [***Git workflows***]({{'/06-forks#workflows' | relative_url }})
    * [Trunk-based workflow]({{'/06-forks#trunk-wf' | relative_url }})
    * [Gitflow]({{'/06-forks#gitflow' | relative_url }})
    * [Forkflow]({{'/06-forks#fork-wf' | relative_url }})
    * [Personal takes]({{'/06-forks#personal-wf' | relative_url }})

* [**Using Git with VSCode**]({{'/07-vscode' | relative_url }})
    * [Clone a repo]({{'/07-vscode#clone' | relative_url }})
    * [Stage and commit]({{'/07-vscode#stage' | relative_url }})
    * [Collaborative development]({{'/07-vscode#collab' | relative_url }})
    * [Branching and MRs]({{'/07-vscode#branch' | relative_url }})

* [**Extra: basic Git internals**]({{'/08-internals' | relative_url }})
  * [What's on my computer?]({{'/08-internals#computer' | relative_url }})
  * [Git objects]({{'/08-internals#objects' | relative_url }})
  * [History]({{'/08-internals#history' | relative_url }})
  * [Deltas]({{'/08-internals#deltas' | relative_url }})
  * [Consequences]({{'/08-internals#consequences' | relative_url }})

* [**Extra: Advanced Git commands and troubleshooting hints**]({{'/09-advanced' | relative_url }})
  * [Branch management: `git fetch` and `git fetch --prune`]({{'/09-advanced#git-fetch' | relative_url }})
  * [Cleaning up your local copy]({{'/09-advanced#cleanup' | relative_url }})
  * [Juggling with branches]({{'/09-advanced#juggling' | relative_url }})
    * [Rebasing (how to keep up with a source branch)]({{'/09-advanced#rebase' | relative_url }})
    * [Interactive rebasing (how to clean up your mess before pushing)]({{'/09-advanced#irebase' | relative_url }})
    * [Cherrypicking (how to move commits around)]({{'/09-advanced#cherrypick' | relative_url }})
  * [Searching for bugs with `git-bisect`]({{'/09-advanced#git-bisect' | relative_url }})
    * [Forking]({{'/09-advanced#forking' | relative_url }})
    * [Merge request]({{'/09-advanced#fork-mr' | relative_url }})
    * [Closing remarks]({{'/09-advanced#fork-closing' | relative_url }})
  * [About the `.git` folder]({{'/09-advanced#hooks-excludes' | relative_url }})
    * [Git hooks]({{'/09-advanced#git-hooks' | relative_url }})
    * [Local exclusion rules]({{'/09-advanced#local-excludes' | relative_url }})
  * [CI pipelines: `.gitlab-ci.yml` examples]({{'/09-advanced#gitlab-ci' | relative_url }})
  * [How to fix a commit to the wrong branch?]({{'/09-advanced#wrong-branch' | relative_url }})
      * [Moving local commits around]({{'/09-advanced#wrong-branch-local' | relative_url }})
      * [Moving pushed commits around]({{'/09-advanced#wrong-branch-pushed' | relative_url }})

In any case, [please do not hesitate to contact me](mailto:mathias.malandain@inria.fr) for any (constructive) criticism, requests or suggestions you may have. Enjoy!

----

# Changelog

## September 3rd, 2024

A lot of changes! Thanks to the careful proofreading and insightful advice of Sébastien Gilles, this site got way better.

* New section ["Forks and workflows"]({{'/06-forks' | relative_url }}) gathers information about the three most common Git workflows and how they may or may not fit your projects.
* A lot of small changes and fixes here and there. If you are driven by FOMO, I guess you should reread the whole thing. Sorry about that.

## January 16th, 2024

The first complete version is online!

----

# Future improvements

For any suggestion you may have, please do not hesitate to [send me an e-mail](mailto:mathias.malandain@inria.fr?subject=About%20your%20Git(Lab)%20tutorial).
---
title: "Creating projects on GitLab"
subtitle: "Where our will materializes."
tags: 
---

There are basically two ways you can create a project on GitLab:

* just create an empty project, or
* push existing code onto GitLab.

We will be going through both options, and see various (essentially equivalent) ways of dealing with both situations. On the way, we will have to talk about how GitLab organizes stuff. (You know what folders and subfolders are, right? Great, you're already halfway there.)

# Projects {#projects}

{: .box-success}
A **project** is pretty much what you think it is. All relevant files for a given software development project (as a first approximation, the contents of the main folder in which you have all the relevant files) are handled together in a project.

As a result, the different versions that will be "remembered" by Git are all snapshots of the entire project. (The way Git actually handles them is [way smarter]({{'/08-internals#deltas' | relative_url }}) than just storing complete copies of the whole project, so do not worry about storage space.)

Creating a project on GitLab is as simple as clicking a button:

![](../assets/img/02-creating-projects/create-project.jpg){: .mx-auto.d-block :}

Have you noticed it? With its blue color, it begs for your attention. (Do not pay attention to the rest of this screen cap. Thank you.) Let us see where this leads us:

![](../assets/img/02-creating-projects/create-project-2.jpg){: .mx-auto.d-block :}

The first option, "Create blank project", is the one we will be using here, but the "Create from template" option deserves to be known if you are into Ruby on Rails, Spring, iOS Swift apps, Jekyll/Pelican/Hugo web pages, and a few more. Okay, so "Create blank project":

![](../assets/img/02-creating-projects/create-project-3.jpg){: .mx-auto.d-block :}

There are a few things to unpack there:

* **Project name:** This one is pretty obvious, and it will, by default, be turned into the **project slug** on the line just below (after making it "URL-compliant", basically). You can still change the slug manually if you wish... I guess.
* **Project URL:** This will start with `https://gitlab.inria.fr/` and end with the slug. In the middle, you have to pick a... *group or namespace*? No worry, we will talk about this in a minute or so. If you are new to GitLab, the only choice you will have there is your default namespace, named after your username.
* **Visibility level:** For now, the rules are pretty clear. Private = invite-only. Internal = anyone from Inria. Public = anyone with the URL. Just remember that this is only visibility: not all people who can see your project can edit it! We will set up permissions (or "roles", as they call it) later on.

{: .box-note}
**Spoiler alert:** The vast majority of projects are created as private projects. When in doubt, make your project private.

* **Initialize repository with a README:** When creating a fresh empty project, I would advise never unchecking this box. All it does is create a `README.md` file at the root of the project, and this file will be displayed in all its rendered glory on the corresponding web page. As soon as the project will be accessed by anyone other than yourself, you will want to write some stuff in this file: basic description first, but then, maybe, setup instructions, a short tutorial/example, links to documentation, copyright information, etc. Common practices include adding a few more Markdown files alongside this one:

    * `LICENSE.md` where the complete text of the software license is pasted,
    * `CHANGELOG.md` where, huh, the change log is provided, and
    * `CONTRIBUTING.md` containing guidelines for contributors (especially for open-source projects, but even for small, closed, collaborative projects, having one is a nice touch and can make your work simpler later on).

GitLab will actually suggest that you create these files as soon as the project is born:

![](../assets/img/02-creating-projects/create-project-4.jpg){: .mx-auto.d-block :}

But wait! Before we reach this page, we need to know **where** to create the project! What is this "group" thing? Why should I care?

# Groups {#groups}

GitLab provides you with a *personal namespace*, based on your username. It is the most obvious solution when you want to create a project, but the fact that it is *personal* probably means that it is not the best idea to put everything in here. Also, well... it would not be a great idea either to put every project in the same place anyways, would it? There are also a lot of limitations on what you can do there, including a strict limit on the number of projects you can create inside it.

So... What are our alternatives?

Back when paper existed (aaaah, good ol' times), we organized our actual, physical files in folders, themselves sorted in different drawers and file cabinets. This basic organization was projected into our computers, with, once again, files in folders, but this time, we could have any (limited, but already potentially huge) tree-like hierarchy of folders and subfolders. Would you have any trust in a tool designed to manage your software projects that just throws all these projects on the same pile?

Yeah, exactly.

{: .box-success}
**Groups** are exactly what it says on the label. If you have (or *will* have in the foreseeable future) several related projects, you should create a group for them. You can think of a group as a folder for projects... with a sophisticated lock.

You can then invite new members to a group, and they will automatically get access to every project in this group: this is what [the GitLab documentation](https://docs.gitlab.com/ee/user/project/members/) calls "inherited membership". With inherited membership, you cannot choose to remove access to a single project: you have to remove the member from the whole group.

However, you can also add members to individual projects without adding them to the whole group! This is a pretty convenient solution in several contexts, actually:

* For dealing with all pieces of software that are developed in the context of a **large collaborative project**, create a group with a few members (the coordinator(s) + one or two admins sworn to secrecy), then grant access to individual projects as required and carefully set roles for every project (more on that later).
* For gathering all **building bricks of a software suite** that is developed and maintained as a whole by the same people, just create each "brick" as a project, all inside the same group with the same members: for each project, each member inherits the role they have in the group itself, so that you only have to deal with who does what *once and for all*.

Time for a short summary in a nice green box, don't you think?

{: .box-success}
**Group members** will be given the same role (or permissions, if you will) in all projects in the group. The administrator can then add **project members**: they will be added to projects independently, possibly with different roles in different projects, and they will not be able to see the remaining private projects in the group.

"Talking about seeing stuff", you say, "what about project visibility? Is group visibility inherited by all projects?" That's a solid question, and the answer is no, thank Gosh! What if you are collaborating with other people for writing both open-source *and* proprietary code? In such cases, the benefits of creating groups would just collapse.

However, you are not 100% free to do whatever you want with group and project visibility. Here is the rule:

{: .box-success}
A project **cannot** have a higher visibility than the group to which it belongs, but it can have a strictly lower visibility.

Remember that there are three visibility levels: *private*, *internal* and *public*, in that order. So: if the group is private, then every project inside it will be private. If the group is internal, then projects inside it can be either internal or private, but never public.

As a corollary, if at least one of the projects has to be public, you have to create a public group, but other projects can be set to internal, or even private. (People with no access to your private projects will not even be able to know they exist in the first place.)

To sum up, **as you go down the chain (from groups to projects), objects can only have their visibility reduced, and members can only have their permissions raised**. If you only remember one thing about visibility and permissions, there it is.

And it will be our guideline for the next step, because... Well, what if I work on a huge project, with 100+ pieces of software developed by various members? A single group containing a bazillion projects will not be convenient for anyone.

![](../assets/img/02-creating-projects/we-need-to-go-deeper.jpeg){: .mx-auto.d-block :}

# Subgroups {#subgroups}

Remember the analogy with folders and subfolders? Yup, here we go again:

{: .box-success}
**Subgroups** are groups within groups. They can be nested up to 20 levels.

In other words, you can create huge tree-shaped architectures of projects. I do not know of any limit for the number of possible subgroups in a group; if you happen to reach it, please (i) tell me, and (ii) sort your life out, this is way too many subgroups, *what are you doing*--

(FYI, there is no creating subgroups in your personal namespace: if you need any sort of hierarchical structure for your projects, even if they are personal, your personal namespace is not fit, and you should create a private group instead.)

As for access, roles and visibility settings, you probably already guessed where we are going. Access can be granted to groups, subgroups or individual projects, and the rules for inheritance are as stated above. Let us highlight this once again:

{: .box-success}
As you go down the hierarchical tree (from group to subgroups to projects), **objects can only have their visibility reduced, and members can only have their role raised**.

By the way, if you want to raise the role of a group member in a subgroup or a project, just add them to the subgroup or project and set their role there. The role you give them in this "direct membership" will override their inherited role. If you want to revert this, just remove them from the subgroup/project: their group membership will not be revoked, and their role in the subgroup/project will, once again, be inherited from their role in the group.

Sounds pretty abstract, right? Well, let us talk about roles.

# Roles {#roles}

This is the part where I lie to your face: **There are 5 different member roles on GitLab.** `</lie>`, I promise.

This is a lie, because a 6th role was added, called "Minimal Access". However, this role cannot be granted at any level and, at the time of writing, [has been buggy for more than three years](https://gitlab.com/gitlab-org/gitlab/-/issues/267996) (the official documentation even provides workarounds now!). So, for all intents and purposes, there are 5 different member roles on GitLab. I will try my best to tell you, in less than 500 words, what each of those means. Just scroll down to the next green box if this is too much information for you at this stage, no offence taken.

* **Guest:** On instances other than `gitlab.com`, a guest on a private project cannot see or download the code. They can basically leave comments, [open issues]({{'/04-branches-issues#issues' | relative_url }}) (which is the intended way to report a bug/incident *or request a feature*, so, yeah, kind of a misnomer), and view some very basic metrics. For all intents and purposes, guests are users that can offer feedback. (The guest role does not exist for public projects, because everyone with the right URL has these rights on a public project.)
* **Reporter:** A reporter has all guest permissions, access to the codebase and the project metrics that are provided by GitLab, plus a few permissions related to issues. This list of permissions, along with the name itself, makes me think that reporters are typically non-developers whose main job is to extract basic information about project activity and turn it into colorful graphs to be shown to suit-and-tie executives during bimonthly project follow-up meetings.
* **Developer:** The label here is pretty clear, right? Project developers are the people who write and review code. The "blood, sweat and energy drinks" tier. The larger the project, the more members with this role. (Group developers can also create fresh projects in the group.) However, developers have to play by the rules that are set by the maintainers. To make it clearer, let us see what maintainers can do that developers cannot.
* **Maintainer:** Not only do maintainers have all the permissions that developers have, but they also choose who the developers are, and they draw the line between what any developer can do and what only *they* can do. Adding or removing project members? Their job. (Group members? Nope, only the owner of a group can manage its members.) Specific conditions for code changes to be propagated into some branches? They decide, they enforce. Setting up a robust pipeline for automatic code testing? Their duty. Has the time come for a new release? I don't know, ask *them*. But, you know the saying about great powers and responsibilities. (Was that Jesus Christ or Ulysses S. Grant?) Maintainers are the ones responsible for releasing software that works, for managing issues and incidents, and sometimes for applying ten independent patches to the same codebase in a single day without opening a black hole somewhere in Nevada.
* **Owner:** Only the owner can change the visibility of their project or group, transfer it, or delete it; only they can manage members of their group(s). These are the main differences between owner and maintainers. The simplest way to gain immeasurable powers is to be the one who clicks the "Create" button. I think there's a lesson here for all of us.

Oh wow, I did it: less than 500 words. It still looks way too long and bloated. Time for a green rectangle:

<div class="box-success" markdown="1">
* The **owner** has all permissions on their group/project.
* **Maintainers** have barely less: there should be a few of them, they should be absolutely trustworthy, and at least one of them should be a somewhat experienced Git and GitLab user, if possible.
* It does not really make sense to have **developers** in projects with only two or three close collaborators, but in a larger dev team with clearly identified project leaders, all other code writers should get this role.
* **Reporters** are basically a managerial role; for pure researchware projects, this role may be forgotten entirely. For large groups created for whole teams or organizations, team leaders and managers may wear this hat.
* **Guests** are people who can tell you whenever anything goes wrong, but will not be contributing to the code itself. Dedicated users who want to provide you with feedback are the perfect guests.
</div>

# Creating a project from scratch {#create-project}

If you just read everything above this line, you are probably pretty hungry right now. Terribly sorry: we will be baking bread 🥖

Why did I pick an example that stupid?

...that's a valid question. Let's move on.

First, we will create a group. We will be working with other people, and our recipe for multigrain baguettes is only the first step towards a full-blown bakery with hundreds of different products. Be afraid, Paul, we are coming for you.

Hierarchy first: let us create our bakery as a group. Just go to GitLab, and check the menu (get it?) on the left. Click on "Groups".

![](../assets/img/02-creating-projects/create-group-1.jpg){: .mx-auto.d-block :}

Once again, there is a blue button waiting for you. Click it. You know you want to.

![](../assets/img/02-creating-projects/create-group-2.jpg){: .mx-auto.d-block :}

No import for the moment, so let us just create a new group.

![](../assets/img/02-creating-projects/create-group-3.jpg){: .mx-auto.d-block :}

This thing about subgroups is step 2, and we are currently on step 1, so we will just name our group, keep the URL that GitLab gives us, and pick a visibility level. Let us assume, for the sake of this (dumb) tutorial, that we want to make this group internal (maybe other people at Inria will want to join our food venture, and they are welcome to do so). Click "Create group". Done. What does this group look like?

![](../assets/img/02-creating-projects/create-group-4.jpg){: .mx-auto.d-block :}

Well, what did we expect? There's nothing there yet, just an empty shell. We could invite colleagues, but maybe we want to bring some furniture and hang a few posters before throwing the housewarming party. We will keep things organized, so why not create a subgroup for our bread? Just click the "Create new subgroup" button, pick a subgroup name, and check that the right parent group is selected in the dropdown. And...

![](../assets/img/02-creating-projects/create-subgroup.jpg){: .mx-auto.d-block :}

This subgroup cannot be made public, as expected: as you go down the hierarchical tree, objects can only have their visibility reduced. We will keep it internal, and create our subgroup.

![](../assets/img/02-creating-projects/create-subgroup-2.jpg){: .mx-auto.d-block :}

Notice the upper-left corner: `bakery > bread`. Time for our first actual (blank) project. I can create it from the "Create new project" button right here, or from my Projects page:

![](../assets/img/02-creating-projects/create-project.jpg){: .mx-auto.d-block :}

In both cases, I will be landing on a page that contains this:

![](../assets/img/02-creating-projects/create-project-2.jpg){: .mx-auto.d-block :}

except that, above these nice clickable boxes, I will be given different paths (`bread / New Project` in the first case, `Your work / Projects / New Project` in the second case). No worry at this stage. Once we choose "Create blank project", however, this will play a role. Let's play "Spot the difference":

![](../assets/img/02-creating-projects/create-project-from-group.jpg){: .mx-auto.d-block :}

![](../assets/img/02-creating-projects/create-project-from-main-menu.jpg){: .mx-auto.d-block :}

In the first case, you have already told GitLab where your project will land. In the second case, you have to go through the dropdown menu to pick a group (in this case, `bakery/bread`), then GitLab will check visibility parameters for the group and disable visibility levels that are inconsistent with the parent group:

![](../assets/img/02-creating-projects/create-project-from-main-menu-2.jpg){: .mx-auto.d-block :}

I am gonna keep my multigrain baguette recipe private for the moment (visibility parameters can be changed by group/project owners whenever they want, although this is not something you want to do everyday). Finally, at long last, lo and behold:

![](../assets/img/02-creating-projects/fresh-project.jpg){: .mx-auto.d-block :}

# Pushing existing code on GitLab {#push-code}

There is an alternative to creating an empty project, and that is taking existing code that you have on your computer and turning it into a GitLab project. There are many cases that lead to turning local folders into shared projects, a vast majority of which are actually cases of "we should have done it before". Once again, even if you are working by yourself on a piece of code, making it a GitLab project is a good idea for several reasons (backups, complete change history, archiving...).

Assume that, somewhere (**anywhere**) on your computer, you have some folder, let's say a folder called `cornbread` (why not?), only containing a `README.md` for now, that you want to turn into a GitLab project.

We would indeed like to have this project alongside `multigrain-baguette` on GitLab, that is, in the same subgroup `bread`. Counter-intuitively, we will first have to create an empty project `cornbread`:

![](../assets/img/02-creating-projects/project-from-local-2.jpg){: .mx-auto.d-block :}

Blue button again, and pick the settings you want... but, for convenience, **uncheck** the "Initialize repository with a README" option this time:

![](../assets/img/02-creating-projects/project-from-local-3.jpg){: .mx-auto.d-block :}

Once your project is created, you are welcomed with instructions on how to actually add content: buttons for creating or uploading files, but also a lot of command line instructions. Scroll down to this one:

![](../assets/img/02-creating-projects/project-from-local-4.jpg){: .mx-auto.d-block :}

This is exactly what you want! *If you want to push **all** the contents of the local folder you `cd` into*, just follow these instructions, and the whole folder will be pushed into your fresh GitLab project.

{: .box-warning}
**Think about cleaning up your project before pushing it:** you should not push temporary files, local configuration files, and so on. If there are specific files or subfolders that you do not want to push (the `build` or `_build` folders are typical examples), you can choose what you actually want to push. Just replace command `git add .` with several commands of the form `git add path/to/subfolder` or `git add path/to/file`. While the former tells Git to push the whole directory (this is what `.` means), the latter specifies which subfolders and files should be tracked. We will see later on how to [prevent specific files from being added]({{'/05-good-practices#gitignore' | relative_url }}).

{: .box-warning}
If the `-initial-branch` option is not recognized, you have a version of Git that is too old (maybe you installed it a few years ago and forgot to update it). Just update your Git installation (the same steps that are used for [a fresh install]({{'/01-setup#git' | relative_url }}) should work) and retry.

Understanding these lines requires knowledge about how Git works, including some dirty details about what Git does behind the curtains. No delving into it right now, but if you're curious:

<div class="box-note" markdown="1">
In short, the Git commands given here basically amount to the following:
* setup the current directory so that it is "Git-ready", and ensure that the name of the default branch is the right one (older projects might have `master` as the name of their default branch, but this was changed in 2020);
* tell Git that this local directory should be linked to the one at the given address (it is exactly the one that is provided to you by *Clone → Clone with SSH* on the project page; **do not use the `https` address instead**: this would require you to use HTTPS authentication on this project, which is way more cumbersome);
* take all the current contents of your local folder (or parts of the contents if you are using one or several `git add some/path` commands instead) and push them to the GitLab project (plus, the `-u` option tells Git that the remote project to which you just pushed will indeed be the default remote).
</div>

Done! Refresh the webpage for your project, and this is what you will see:

![](../assets/img/02-creating-projects/project-from-local-5.jpg){: .mx-auto.d-block :}

----

There are already a few things you can do online by clicking buttons, and GitLab provides a pretty nice online IDE (Integrated Development Environment), but this is **not** what you want to do. GitLab is based on Git, and Git makes it possible to work, in a collaborative manner, on a local copy of a project.

Brace yourselves: [**It is time to learn how Git works.**]({{'/03-linear-git-project' | relative_url }})

You may also go back to the [detailed table of contents]({{'/index#toc' | relative_url }}).
---
title: "Working on a linear Git project"
subtitle: "Where we learn to go with the flow."
tags: 
---

I am not one of these old buggers who think that, in order to really understand something, you have to learn it the hard way. I will not be advocating against all these stupid *so-called* "productivity tools" that pre-chew your food, because I learned to write assembly code on vi when I was 12 and do not understand what the fuss is about these "high-level languages" and "IDEs" and stuff.

However, I know for a fact that, without a proper understanding on the basics, you are doomed to make mistakes. Been there, done that.

What follows is my best attempt, to date, at explaining everything you need to know to use "basic" Git without fear, and nothing more. We will be working on our project "the old-fashioned way", by editing our files with whichever tool we want to use, and directly typing our Git commands from a terminal.

Later on, once we know how Git works, we shall see [how the "version control" panel in VSCode can help us]({{'/07-vscode' | relative_url }}). (Other tools exist that can help you with Git; I chose to only show you VSCode because (i) it is an IDE that knows Git, meaning that you can edit your local copy *and* handle version control with a single software, and (ii) it is the most widespread development environment by far, arguably for a lot of good reasons.)

# Preamble {#preamble}

The nice thing about Git is that **you will spend 95% of your time working on a Git project as if it were just a folder on your machine**. In a way, it is *your* code, and you can edit it like you would any document or code that you are keeping to yourself. You will be working on a local copy, with all files and folders of the project on your own computer. This page is mainly about how to exchange data with the project on the server (pushing your own changes to the project, pulling changes in your working copy, handling conflicts between the codebase on your computer and the one on the server, and so on).

{: .box-success}
In the Git terminology, the project (on the server) is called the **repository** (or **repo** for short), and the local copy is called the **working copy**.

# Cloning a project {#clone}

The first thing I have to do, before working on the code, is to **clone** the repo (unless, of course, I created the project by [uploading local files]({{'/02-creating-projects#push-code' | relative_url }})). In a nutshell:

{: .box-success}
**Cloning a repo** is the act of creating a working copy of this repo on your machine. The corresponding command is `git clone`.

Of course, just typing `git clone` in your terminal will give you an error: "What am I supposed to clone?" To answer that question, go back to the page of the project on GitLab, and notice the unmissable "Clone" button. Click on it.

![](../assets/img/03-linear-git-project/A-cloning/project-clone.jpg){: .mx-auto.d-block :}

We went through the (reasonable) hassle of [configuring SSH authentication for GitLab]({{'/01-setup#gitlab' | relative_url }}), why wouldn't we reap the benefits? The contents of the text box below "Clone with SSH" (it will always start with `git@`) is our missing argument for `git clone`. Open a terminal, `cd` into the folder you want to clone into, and put the pieces together; in this case, the full command would be

```shell
git clone git@gitlab.inria.fr:bakery/bread/multigrain-baguette.git
```

Type your SSH key passphrase when asked, then wait for the cloning operation to end. A new folder called `multigrain-baguette` just appeared. This is what it looks on my computer:

![](../assets/img/03-linear-git-project/A-cloning/local-1.jpg){: .mx-auto.d-block :}

The `.git` folder is hidden by default (if you do not see it in your working copy, do not worry, you still have it). [This is where the magic actually happens]({{'/08-internals' | relative_url }}), but we are not delving into this before getting a grip on how to use Git. For the moment, you may assume (for convenience) that *there is no reason for you to edit anything in the `.git` folder*. For all intents and purposes, the whole project is a `README.md` file and nothing more.

{: .box-note}
This is kind of a white lie: there are a few very good, but a bit more advanced, reasons to change the contents of this folder. You may learn more about this in [this additional content]({{'/09-advanced#hooks-excludes' | relative_url }}).

From now on, Git will remember where my folder came from. As long as I am working in the folder (or a subfolder) of the working copy, Git knows to which repository it is linked. I can now, whenever I want, fetch the changes that were submitted by my collaborators, change the code, and decide whether I want to push all or some of my changes back to the repo (thus adding my own brick to the repo's history). Okay, simple enough.

However, this basic description is already lacking something. Something that you may not be told from day 1, and that will prevent you from understanding anything from the online documentation when you will (inevitably) have to check it. Let's jump on stage.

# Repository, staging area, working copy {#repo}

Assume that you are working on a nice and simple project, that evolves incrementally. People kind of take turns making changes to the code, and the history of the project, as Git sees it, is just a nice straight line from one version to the next. (Enjoy living in this ideal world: [this will not last]({{'/03-linear-git-project#fetch-pull' | relative_url }}).

Today, it is your turn to make changes to the code. This is what you do:

* You **fetch** (or **pull**) the most recent version of the codebase on your computer, from the server.
* You work on the code, 100% locally: you change the contents of some files, create one or two fresh files for new features (or just because [some of your modules became bloated]({{'/05-good-practices#practices' | relative_url }}) and you wanted to refactor a bit), maybe remove a few files here and there, all of this on your working copy. You use your favorite editor or IDE to get work done, save your changes regularly (with Ctrl-S or `:w` or whatever floats your boat), without a care in the world.
* Finally, you check that everything you did actually works, clean up a bit, and **push** your work to the server.

Pretty simple, right? Actually, the last step is a bit more involved. See, there is the possibility that you worked on two different things in the same code today, and maybe you want to make it clear in the public history of the repository. Hence, you want to pick part of your changes and bundle them together, then pick the remaining changes and bundle *them* together, and, finally, push these two nice boxes, wrapped in beautiful wrapping paper, to the repo, where everyone can see (and take) them.

It is time to run through this analogy and use less Christmassy, more Gitty words:

* When you pick the changes that will be "bundled", you **stage** them. Staging changes in an already existing file, or deciding that a fresh file should be part of the project, is performed with the `git add` command. Deciding that a file, that was previously part of the project, should be removed from it, is performed with the `git rm` command. All these changes (additions to existing files, new files, file deletions) are tracked in the **staging area**, a kind of local buffer that stores the changes that will be part of the next bundle.
* The bundle of changes is called a **commit**, and its wrapping paper is its **message**. Committing is performed with the command `git commit -m "My commit message"`: it moves everything that is currently in the staging area to an object (hidden, like most files Git uses to handle pretty much everything it does) called, well, a commit.
* Sending one or several commits to the repo is called **pushing**. Guess the command? Yep, `git push`. No other argument: all the commits that are ready to ship are sent to the repo.

To put it in good use, I will be working on two separate things in my project: edit the `README` file, and start working on my recipe. The order in which I make my changes and/or create my files is actually irrelevant. I can keep my old working habits and save whenever I want to (that is, as often as possible).

After my work session, I will ask for information about the **status** of the project. Guess the command? That's `git status` indeed. If I run it from the root directory of my working copy, this is what I get:

![](../assets/img/03-linear-git-project/A-cloning/local-2.jpg){: .mx-auto.d-block :}

* For now, there is only one branch, called `main`: it is the one created by default (it used to be called `master`, and a lot of online tutorials were not updated, so be cautious). We shall see later how to deal with [more than one branch]({{'/04-branches-issues' | relative_url }}).
* Also, my local branch seems to be up to date with the `main` branch on `origin`, that is, the version of the server. I have no commit that the server is unaware of, and no commit was pushed to the repo while I was looking away... presumably ([we will get back to this]({{'/03-linear-git-project#fetch-pull' | relative_url }})).
* I have changed the contents of the `README.md` file (I could use `git diff README.md` to see what my changes are), and added a new file `recipe-v1.txt`).
* Finally, I am not preparing any commit.

The changes I made were very different and independent: it makes sense to put them in two separate commits. Let us start with the `README`: I can just type `git add README.md`, which means *"stage all changes I made in `README.md`"*. Now, a new `git status` gives me this:

![](../assets/img/03-linear-git-project/A-cloning/local-3.jpg){: .mx-auto.d-block :}

My first actual commit will not contain anything more, so that I can just run something like

```
git commit -m "Add relevant info to the README file"
```

Did I do everything right so far? My new best friend `git status` will tell me:

![](../assets/img/03-linear-git-project/A-cloning/local-4.jpg){: .mx-auto.d-block :}

Alright, I have one commit ready to be pushed, a file that is still untracked (it does not exist at all on the repo, only in my working copy), and my staging area is empty again. I can now add my new file and put that one in a new commit:

```
git add recipe-v1.txt
git commit -m "Add a first version of the recipe"
```

(Of course, I could have added other files/changes to the same commit, just by using the commands `git add` and `git rm` several times before committing, and/or by listing several files in my `git add`, something like `git add file.txt myfolder/otherfile.csv`.)

The output of `git status` at this stage (no pun intended) should be pretty obvious: no more changes to commit, empty staging area (the exact words should be *"Nothing to commit, working tree clean"*), and I am 2 commits ahead of `origin/main`. Time for a `git push`, during which I will be asked my SSH passphrase. I did not have to type it earlier, because **this is the first time since cloning that I actually interact with the server**. In our ideal setup, this is not an issue, but [we'll get back to this]({{'/03-linear-git-project#fetch-pull' | relative_url }}).

Going back to the project's page on GitLab, I can see that some changes occurred recently:

![](../assets/img/03-linear-git-project/A-cloning/local-5.jpg){: .mx-auto.d-block :}

The "Last commit" field displays the message associated with the last commit that actually changed the file, and the "Last update" field tells you when *that* commit was created. I can also check the graph of my repo:

![](../assets/img/03-linear-git-project/A-cloning/gitlab-graph-1.jpg){: .mx-auto.d-block :}

For the moment, the graph of this project is very straightforward, as expected.

![](../assets/img/03-linear-git-project/A-cloning/gitlab-graph-2.jpg){: .mx-auto.d-block :}

Time to wrap up this part!

<div class="box-success" markdown="1">
There are three "levels" to a Git project:
* **Repository:** Called `origin`, it is the place on the remote server where every part of the project that was pushed, is actually stored. Your computer will be sending requests to the server in order to fetch data from the repository and/or push data to it.
* **Staging area:** A "hidden" part of your working copy, the staging area is where the changes that you want to be part of the next commit are stored.
* **Working copy:** This is where the project, in its "current state", is stored on your computer, for you to play with.
</div>

Regarding the way these levels interact, there are a few important things that you may want to keep in mind. If you only read one, please let it be the one in this passive-aggressive yellow box.

{: .box-warning}
**Only fetching commands, like `git pull`, and pushing commands, like `git push`, are actual communications between the repository (on the server) and the working copy (on your computer).** The rest of the commands are 100% local to your computer. This has a few implications, good and bad.

As an illustration, this is a basic flowchart of every operation we performed so far that implies any data flow between two different levels. Notice how only the first and last commands involved the server.

![](../assets/img/03-linear-git-project/A-cloning/sequence-00.jpg){: .mx-auto.d-block :}

The fact that nearly all Git commands are local has important implications. Let us start with the bad news:

{: .box-warning}
**`git status` will lie to you**. Okay, it will not actually lie to your face, but its mission is to compare the current state of your working copy with *the last known state of the repo*. If you haven't used any fetching command in the last year, `git status` will not know anything about the repo's activity in the meantime, and this may lead to *conflicts*, that we will learn how to solve and how to alleviate [in a few screens]({{'/03-linear-git-project#conflict' | relative_url }}).

Okay, now, the good news:

{: .box-note}
**As long as you do not push, all your changes and commits are invisible to everyone but you, and you can undo anything**. We will not be going (yet) through the bazillion commands that can help you undo changes, [reorganize your commits](({{'/09-advanced#irebase' | relative_url }})), erase all evidence of your mistakes, but know that they exist.<br/>This implies that **pushing after every commit is not always the best move**. It can be more convenient for "real-time collaboration", especially as inexperienced Git users, but as you grow familiar with it, you will realize this and think to yourself "Wait, he told me, why didn't I listen?" (Been there, done that. It is part of the learning process.)

# Fetching vs. pulling {#fetch-pull}

Remember how I told you that we were working in an ideal environment, where there are never two people or more working at the same time on the same project? In general, this is *not* what will happen in actual projects. Git is acutely aware of that: it was designed so that you have every tool you need to handle situations in which your colleagues and yourself are making changes at the same time.

In other words, do not even think of sending e-mails to your colleagues to tell them that you will be working on the project from 10AM to 5PM and that they should not be editing anything on their side! (This is a temptation SVN users will have. I have seen it.)

What you want to do instead is to check, at the right times, whether your changes might be conflicting with changes your colleagues already pushed. If this looks like a daunting task, be reassured: it is not, as long as you know what you're doing and take the right habits.

If you want Git to be aware of changes that were made on the origin, you will have to fetch or pull at some point. We have been using these terms interchangeably so far: it is time to know what they actually mean.

{: .box-success}
**Fetching** amounts to synchronizing the state of your local copy with the current state of the actual repo. The command `git fetch` is how you ask Git to update its knowledge of the state of the project on the origin (i.e., the remote server).

In order to illustrate this, I asked my doppelganger to change a few things in my recipe, from the online IDE on GitLab. A new commit was created and pushed. I will now run `git status` from my computer:

![](../assets/img/03-linear-git-project/B-fetching-pulling/git-fetch-1.jpg){: .mx-auto.d-block :}

"What do you mean, up to date?", you think. "I happen to know this is not real. Stop lying to me."

Well, as I already said, `git status` is not lying to you: he just chose the wrong words. You see, your branch is in sync with *the last version of `origin/main` it knows*. However, if you want Git to update its knowledge, you have to run `git fetch` (and, of course, you will be asked your passphrase):

![](../assets/img/03-linear-git-project/B-fetching-pulling/git-fetch-2.jpg){: .mx-auto.d-block :}

Something happened, right? Let's now run `git status` again:

![](../assets/img/03-linear-git-project/B-fetching-pulling/git-fetch-3.jpg){: .mx-auto.d-block :}

See, `git status` was not lying! Now that it got the latest news about the origin, you are told that you are one commit behind, and that you can "fast-forward", [whatever that means]({{'/09-advanced#rebase' | relative_url }})! We indeed have to talk about a few other things before coming back to fast-forwarding. The only thing you have to know is that the situation is still pretty simple to handle: you did not change anything to your working copy, you just happen to be one commit behind. Git suggests that you update your working copy with `git pull`: why not?

![](../assets/img/03-linear-git-project/B-fetching-pulling/git-fetch-4.jpg){: .mx-auto.d-block :}

Great! Now, my doppelganger's changes appear in my working copy. A few things do not seem to make sense, though. First, why did I have to type my passphrase once again? Second, what the heck does "rewinding head" mean? Third, what is "f5269..."?

I will not answer question 2 in this tutorial, because (i) we are only here for the "basics", and (ii) because of the default configuration of Git, you may not even see this specific message when pulling.

The answer to question 3 will come later, when we take a look at [how Git handles all this data internally]({{'/08-internals#objects' | relative_url }}).

The one question that is important to us right now is the first one: I just fetched, so why must I type my passphrase again for pulling? The answer is twofold. First, [you did not use `ssh-add`]({{'/01-setup#gitlab' | relative_url }}), otherwise you would only be asked your passphrase once per session. Second, and this is more relevant right now:

{: .box-success}
**Pulling** is a two-stage process: first, changes are *fetched* from the origin; second, the changes that were done on the origin are *merged* with the changes that were already made in the working copy.

This means, in particular, that even if you just fetched, `git pull` will call `git fetch` anyway, so that your computer will indeed communicate with the origin once again. (There is a way to merge fetched changes with your local changes without having to call `git fetch` again, but it is way more convoluted, so that Git would rather advise you to just pull.)

{: .box-note}
There are interesting opinions out there about [why you should not always use `git pull`](http://longair.net/blog/2009/04/16/git-fetch-and-merge/), but for our purposes, using `git pull` should be fine. You should still, at some point, read the article linked above to know more about the differences between "pulling" and "fetching then merging": the more complex your Git project is, the more important it may become, and using more advanced commands like [`git rebase`]({{'/09-advanced#rebase' | relative_url }}) might essentially require you to fetch instead of pulling. Also, calling `git fetch` with optional arguments will prove pretty useful later on, so even at this stage, it is nice to know that `git pull` is all but an atomic command.

For now, we have to adapt our workflow so that changes on the remote are handled. Let's say that I just have teeny tiny changes to make, and I pulled pretty recently. After I made my changes, but before I push them, I should be pulling again! The part of the workflow that has to be changed is the following:

![](../assets/img/03-linear-git-project/B-fetching-pulling/sequence-01.jpg){: .mx-auto.d-block :}

The question that remains is: Should I pull just before or just after the commit? Both answers are technically valid, because Git is a very nice tool. However, there is a caveat:

{: .box-warning}
**Pulling before a commit is risky**: if anything bad happens during the merging phase, your work has not been safely stored in a commit!

This results in the following updated and safer workflow:

![](../assets/img/03-linear-git-project/B-fetching-pulling/sequence-02.jpg){: .mx-auto.d-block :}

> I chose to describe `git pull` as a command acting on the staging area, which is a white lie: it actually acts on the working copy. However, I think it helps visualizing the fact that your commit and the possible commits that are pulled will be interacting in some way.

To be perfectly honest, you should probably pull as often as possible. If you are afraid that the merging stage of a pull might go terribly wrong and force you to redo your work, [`git stash` might be what you need](https://stackoverflow.com/questions/18529206/when-do-i-need-to-do-git-pull-before-or-after-git-add-git-commit). (This is a powerful command, but I do not want to give you too much information at once. [We will talk about it in the next chapter.]({{'/04-branches-issues#switch' | relative_url }}))

"Excuse me, sir", you gently interrupt. "You already implied twice that merging might go wrong. How so?"

Glad you asked.

# Conflict handling {#conflict}

Conflicts are pretty common with Git. The more people work on a project and the more active it is, the more conflicts you will have to handle. The first one you face will be scary; the tenth one will be a slight annoyance (except if you haven't pulled for a long time, in which case your debt has to be paid).

Here, my doppelganger and I have been working on the project at the same time. We both edited a lot of stuff, but it so happens that most of my changes were on parts of the project he did not alter in the slightest, and vice-versa. Git will be clever enough to notice this and handle the situation like a pro.

However, there is one line that we both edited in different ways, and as he pushed first, I will be the one who deals with the conflict. (I hate that guy.)

I actually forgot to pull before pushing. This makes the situation even worse, right? Will my `git push` create a black hole? Nope:

![](../assets/img/03-linear-git-project/C-conflict/conflict-1.jpg){: .mx-auto.d-block :}

Git checked, and I happen to be late to the party. It tells me that I should pull before pushing again. Thank you, Git. Let me run `git pull`, then.

![](../assets/img/03-linear-git-project/C-conflict/conflict-2.jpg){: .mx-auto.d-block :}

The first time I saw something like this, this was my reaction:

![](../assets/img/03-linear-git-project/C-conflict/panic.jpg){: .mx-auto.d-block :}

It's actually not that bad. The only file with a conflict is `recipe-v1.txt`, and the rest of the text tells me what I have to do to fix the problem.

{: .box-error}
**Always read the text and follow the actual instructions you get.** Because of different Git settings, you may have to type different commands to resolve the conflict.

Opening the conflicting file, I notice something unusual:

![](../assets/img/03-linear-git-project/C-conflict/conflict-3.jpg){: .mx-auto.d-block :}

This, right there, is Git showing me where the differences are. The block with `HEAD` is the incoming change, the one that was made by people who pushed before I pulled. The block below contains my changes, and it even gives me the name of the commit in which I made this change. Now I can just pick and choose, or maybe create a third version: as long as I remove the conflict markers `<<<<<<<`, `=======` and `>>>>>>>`, Git will trust that I solved the conflict.

Once this is done, I will just do as Git tells me: `git add recipe-v1.txt` (this is my way of saying "I resolved all conflicts in this file", but Git will still check that all conflict markers have disappeared), then `git rebase --continue`. Once again, **this last command may be different for you: read the warning message you get when the conflict happens, and follow the instructions you are given**.

And... that was it. This was my first conflict, and it was solved in a minute.

Remember this: you will face conflicts at some point, and *99% of them are innocuous*. At worst, they are kind of a hassle. The remaining percent only occurs when you use Git in intricate ways, at which point you have already become way better at Git and are probably ready to face the challenge.

Time to sum up this section:

<div class="box-success" markdown="1">
**Pull conflicts** occur when changes made to the working copy are incompatible with changes that are pulled from the repo. Frequent pulls are the only way to encounter less conflicts and to make them easier to resolve.

Git performs advanced file comparisons in order to identify the smallest conflicting parts that have to be merged. In order to solve the conflicts:

* List the conflicting files: every line beginning with `CONFLICT` identifies one such file.
* In each conflicting file, search for conflict markers such as `>>>>>>>`, `=======` and `<<<<<<<`. Between these markers are two different versions of the same block or code/text: one that comes from the repo, one that comes from your working copy. Replace the whole think (from the line starting with `>>>>>>>`, to the line starting with `<<<<<<<`) with the code/text you want to keep. The latter can be one of the two conflicting versions of the block, or an entirely different text/code. Once all conflicts in the file are handled, use `git add <path_to_the_file>` to mark the conflicts in this file as resolved.
* Once all conflicts were handled and all corresponding files added, ***use the command provided by the log from the `git pull` command*** to continue the pull process (often `git commit`, sometimes `git rebase --continue`, depending on the settings of Git on your computer).
* Check the logs to see if more actions are required on your part.
</div>

If several commits are involved in the conflict, you might have to repeat this process several times before all conflicts have resolved; basically, if you were far behind the repo, you may have to catch up one commit at a time. The larger the number of commits, the harder it is to solve all conflicts in a way that ensures that the end result is working code. Hence, you should use `git pull` pretty often (at the very least, once just before starting work on the project, and once at the end of the day and/or before pushing).

----

Now that the stage (no pun intended) is set, get ready to dive in. [**Time to introduce the best features of all: branches and issues.**]({{'/04-branches-issues' | relative_url }})

You may also go back to the [detailed table of contents]({{'/index#toc' | relative_url }}).
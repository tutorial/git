---
title: "Using Git with VSCode"
subtitle: "Where terminals fade away."
tags: VSCode "Web IDE" GitLab
---

Using Git from the terminal? Perfectly fine. I still do it myself, because (i) it makes me feel like I am 100% in control of everything I commit and push, and (ii) it fills me with a weird sense of satisfaction. However, I get that not everyone is like me, and this is pretty good news, to be honest.

Would you rather be clicking on stuff in a dedicated software, instead of having to type every single command (and getting Git to laugh at you every time your fingers slip)? You have a lot of options there: [TortoiseGit](https://tortoisegit.org/) (Windows), [GitUp](https://gitup.co/) (Mac OS), [Git Cola](http://git-cola.github.io/) (multi-platform) are among the best free tools, and hundreds of proprietary tools with various pricing options exist.

There's also a third option, that a lot of people have already chosen. What about an Integrated Development Environment that would just speak fluent Git, while also being able to help you with basically any other programming language?

Yup, it's VSCode time again. I talked about it very briefly [a few centuries ago]({{'/01-setup#vscode' | relative_url }}), and briefly explained that it is, well, pretty freaking good. You may enjoy using it for most of your code development.

We will also be talking about GitLab's very own web IDE, because yes, if you need to, you can directly work on your project via a nice web interface! Just one button to click:

![](../assets/img/07-vscode/A-open-and-clone/vscode-01.jpg){: .mx-auto.d-block :}

(There are similar buttons on the pages you reach when exploring the project.) And this is what you get:

![](../assets/img/07-vscode/A-open-and-clone/vscode-02.jpg){: .mx-auto.d-block :}

...oh, well. It's VSCode. **It's always VSCode.** Let's do this.

# Clone a repo {#clone}

Everything that we already learned about [cloning a repo]({{'/03-linear-git-project#clone' | relative_url }}) still works. You may very well open a terminal in your VSCode window and proceed as before.

Or you may get familiar with the Git menu in VSCode. That's the third icon on the left:

![](../assets/img/07-vscode/A-open-and-clone/vscode-02bis.jpg){: .mx-auto.d-block :}

If you just click on it right now, unless you are already in a folder containing a Git project, you should get this:

![](../assets/img/07-vscode/A-open-and-clone/vscode-03.jpg){: .mx-auto.d-block :}

To clone a Git repo, you have to get and copy its URL from its GitLab page, still in the same *Clone* menu as before:

![](../assets/img/07-vscode/A-open-and-clone/vscode-04.jpg){: .mx-auto.d-block :}
> Once again, you should get the SSH link, for future convenience.

Back to VSCode, when you click on the "Clone Repository" button, there is some movement in the top part of the window, near the search bar. This is where VSCode wants you to paste your URL.

![](../assets/img/07-vscode/A-open-and-clone/vscode-05.jpg){: .mx-auto.d-block :}

Press Enter, and VSCode will ask you where to clone the project.

![](../assets/img/07-vscode/A-open-and-clone/vscode-06.jpg){: .mx-auto.d-block :}

Once again, you should **not** create a folder with the name of the project, as the project will be a fresh folder anyways. If I select a folder `cornbread` in which the project of the same name should be cloned, I will have to navigate to `cornbread/cornbread` to reach the project files.

Last step, of course:

![](../assets/img/07-vscode/A-open-and-clone/vscode-07.jpg){: .mx-auto.d-block :}

Once the project is cloned, VSCode asks you if you want to open it right away. Why not?

![](../assets/img/07-vscode/A-open-and-clone/vscode-08.jpg){: .mx-auto.d-block :}

And here it is:

![](../assets/img/07-vscode/A-open-and-clone/vscode-09.jpg){: .mx-auto.d-block :}

Not a lot of content right now (we only worked on the other one before). Time to change this!

# Stage and commit {#stage}

For now, I am going to make changes directly on the `main` branch. You can still [go back here]({{'/04-branches-issues#not-do' | relative_url }}) to read why this is a pretty bad idea in collaborative projects, so... let us assume I am the only contributor right now, I am only going to introduce non-breaking changes, and the `main` branch is not [protected]({{'/05-good-practices#protect' | relative_url }}).

So, changes in the `README.md` file (the only one I have right now), plus a first recipe for our (soon-to-be) delicious cornbread. Time to create a file for that, which I can do with the click of a button.

![](../assets/img/07-vscode/B-stage-commit/vscode-stage-01.jpg){: .mx-auto.d-block :}

I am asked the name of the file, and bam, I can work on it. A few changes later, let us have a closer look at our sidebar:

![](../assets/img/07-vscode/B-stage-commit/vscode-stage-02.jpg){: .mx-auto.d-block :}

These colors and letters are not here for nothing:

* **M** means *modified*: the contents of this file have changed since the last time the sources were fetched.
* **U** means *untracked*: this is a file that the repo does not know of. (We just created it in our working copy, so... of course.)

We basically just have a visual counterpart to `git status`, which is exactly what we need. Also, the small Git logo in the left bar has this small notification bubble with a 2 in it: this is the number of files that differ from the state of the repo, for a reason or another (in our case, one file was changed and one was just created). Let us click this button:

![](../assets/img/07-vscode/B-stage-commit/vscode-stage-03.jpg){: .mx-auto.d-block :}

Only changed files will appear here, and each of them comes with three buttons, in this order: open the file in the editor, revert changes, stage. Note that the "open file" option is a specific button; if you just click on a filename from here, you will get this view instead:

![](../assets/img/07-vscode/B-stage-commit/vscode-stage-04.jpg){: .mx-auto.d-block :}

That is a nice diff, indeed. This makes sense: this Git panel is not where you should make changes to your code, but where you should review the changes before committing. I am okay with the changes in `README.md` and ready to stage them: time to click on that `+` button.

![](../assets/img/07-vscode/B-stage-commit/vscode-stage-05.jpg){: .mx-auto.d-block :}

This will open a new tab "Staged changes". I want a commit that only contains my changes to this file, so that I can just give a message to my commit...

![](../assets/img/07-vscode/B-stage-commit/vscode-stage-06.jpg){: .mx-auto.d-block :}

...and click "Commit". The view then becomes this:

![](../assets/img/07-vscode/B-stage-commit/vscode-stage-07.jpg){: .mx-auto.d-block :}

Everything went fine, and now, only `recipe-v1.txt` is left in my uncommitted changes. I will stage it in the exact same way and provide a message for this new commit, but then, I can click on the small arrow on the right of the "Commit" button, just to see.

![](../assets/img/07-vscode/B-stage-commit/vscode-stage-08.jpg){: .mx-auto.d-block :}

Yay, we have options! The first one is the default, the second one makes it possible to [amend the commit I just created]({{'/05-good-practices#amend' | relative_url }}) (which is okay, as I have not pushed it yet), "Commit & Push" does exactly what it says on the label, and "Commit & Sync" will actually commit, then pull, then push. For now, I would just be content with committing, then pushing all my new commits. Guess what comes next?

![](../assets/img/07-vscode/B-stage-commit/vscode-stage-09.jpg){: .mx-auto.d-block :}

Yep, I am reaching the repo, so that I have to provide my SSH passphrase. Once this is done, no changes and no upstaged changes remain. Also, if this is my first time doing this, VSCode will ask me if I want it to do more:

![](../assets/img/07-vscode/B-stage-commit/vscode-stage-10.jpg){: .mx-auto.d-block :}

Remember that `git fetch` will not modify your working copy: it will just update your computer's knowledge of the state of the repo. So, why not? Just remember that `git fetch` and its variants exist if, for a reason or another, you switch back to the editor-and-terminal workflow.

# Collaborative development {#collab}

We are still working on a single branch right now, but with changes being made from several places, will VSCode still help us?

...you bet it will.

## Using the Web IDE

It's saturday morning, and someone just tested my recipe and told me that there were a few very important and urgent tweaks to apply to the recipe. No huge changes, but the end result is a total mess without these. My work computer is still in my office, and I really want to commit and push a few changes right now. It might be time for the Web IDE to shine.

I can just log in to GitLab and open the Web IDE to edit our recipe:

![](../assets/img/07-vscode/C-web-ide/vscode-sync-01.jpg){: .mx-auto.d-block :}

I already told you: **it's always VSCode**. These are pretty familiar grounds, with our good old notification in the sidebar and everything. This time, however, when I switch to the Git panel, there are considerably less options:

![](../assets/img/07-vscode/C-web-ide/vscode-sync-02.jpg){: .mx-auto.d-block :}

In particular, no choosing whether a file should be included to the next commit or not. This makes perfect sense:

{: .box-warning}
Any complex change in the codebase should be made from a working copy, then committed and pushed. The Web IDE is fine for small urgent changes (*hotfixes*, if you will), not for actual development.

Also, there is no committing without pushing. I mean, where would the commit be stored? We are on the repo itself, not on a working copy, so there's no "committing now, pushing on the repo later". One click later:

![](../assets/img/07-vscode/C-web-ide/vscode-sync-03.jpg){: .mx-auto.d-block :}

...yup, one can also create a new branch in a very convenient way from the Web IDE. For now, I just want to push to the main branch. A small pop-up window appears on the bottom-left corner as soon as I click the button:

![](../assets/img/07-vscode/C-web-ide/vscode-sync-04.jpg){: .mx-auto.d-block :}

And it's over. My changes are on the repo.

{: .box-warning}
There are cases in which you do **not** want to use the web IDE. Typically, if you have a bunch of tests that you should run before pushing code to the repo (or, even better, have plugged a [pre-commit hook]({{'/09-advanced#git-hooks' | relative_url }}) that will prevent you from pushing code that fails tests), then you should always work from your local working copy.

## Fetching and pulling

Alright, fast-forward to monday morning. I am back in my office, and I know changes were made on the repo. Maybe I asked VSCode to fetch on a regular basis, so that it will `git fetch` as soon as I open my working copy. Or maybe I did not, in which case I can just ask nicely by clicking on the three dots:

![](../assets/img/07-vscode/D-fetch-pull/vscode-sync-05.jpg){: .mx-auto.d-block :}

I can then click "Fetch", aaaaaaaand...

![](../assets/img/07-vscode/D-fetch-pull/vscode-sync-06.jpg){: .mx-auto.d-block :}

...as usual.

In both cases, my Git panel now has something to tell me:

![](../assets/img/07-vscode/D-fetch-pull/vscode-sync-07.jpg){: .mx-auto.d-block :}

Basically, there are changes on the repo, hence I should sync with it. The downwards arrow indicates that I should "download" changes, from the repo to my working copy. Alright then. "*click*"

![](../assets/img/07-vscode/D-fetch-pull/vscode-sync-08.jpg){: .mx-auto.d-block :}

Actually, in our situation, right now, we will only be pulling, not pushing, but fine, go ahead. (If I am really confident in my knowledge of Git, I may pick the "Don't show again" option. Just remember that "Overconfidence precedes carelessness." Just sayin'.)

## Handling conflicts

A few days later, I am back to work on the project. If I remember correctly, automatic fetching is on, but if I am not sure, I may pull just in case:

![](../assets/img/07-vscode/E-conflicts/vscode-conflict-01.jpg){: .mx-auto.d-block :}

I then change a few things here and there, and back to the Git panel to prepare my commit.

![](../assets/img/07-vscode/E-conflicts/vscode-conflict-02.jpg){: .mx-auto.d-block :}

I just want to stage my changes on my file (by just clicking the `+` button), write a nice commit message, then commit **and** push at the same time, because why not?

![](../assets/img/07-vscode/E-conflicts/vscode-conflict-03.jpg){: .mx-auto.d-block :}

I pulled, like, 10 minutes ago. There is no way any--

![](../assets/img/07-vscode/E-conflicts/vscode-conflict-04.jpg){: .mx-auto.d-block :}

Hmm, well, let us click the nice blue button as usual, I guess.

![](../assets/img/07-vscode/E-conflicts/vscode-conflict-05.jpg){: .mx-auto.d-block :}

Okay, someone else was working at the same time on the project. Something was pushed while I was looking away. No problem, I can handle it. However, there is something intriguing in my Git panel:

![](../assets/img/07-vscode/E-conflicts/vscode-conflict-06.jpg){: .mx-auto.d-block :}

This time, the arrow is pointing up. Am I supposed to push my changes to the repo? Let's see what happens.

![](../assets/img/07-vscode/E-conflicts/vscode-conflict-07.jpg){: .mx-auto.d-block :}

...

![](../assets/img/07-vscode/E-conflicts/oh-no.jpg){: .mx-auto.d-block :}

These red exclamation marks are no good news, right? Is it conflict time again?

Well, yes, but VSCode is really friendly with conflicts. See, this is what is displayed now in the editor, as my `recipe-v1.txt` file:

![](../assets/img/07-vscode/E-conflicts/vscode-conflict-08.jpg){: .mx-auto.d-block :}

Here, the green block is the "current change", i.e., the one that comes from my working copy, while the blue one is the "incoming change", i.e., the one made on the repo while I was looking away. I can choose whether to keep only one of the changes, or both at the same time.

If none of these options are fine with me, I can also just edit the file by hand, as I did before: this whole part (starting with `<<<<<<<<` and ending with `>>>>>>>>`) has been formatted by VSCode, but it is just good old editable code. Or I can do the same thing, but better, with the "Resolve in Merge Editor" option:

![](../assets/img/07-vscode/E-conflicts/vscode-conflict-09.jpg){: .mx-auto.d-block :}

On this view, I have my version on the left, the incoming version on the right, and the result I want to get on the bottom. As long as I do not type anything in the orange rectangle in the bottom editor, this rectangle will stay here, telling me that there are "no changes accepted". As soon as I edit its contents, whatever the edit actually is, the rectangle will disappear:

![](../assets/img/07-vscode/E-conflicts/vscode-conflict-10.jpg){: .mx-auto.d-block :}

It is up to me to check that what I type there is exactly what I intended to type. (If this is actual code, it might be a good idea to compile and test my code before I commit and push it.)

{: .box-success}
When solving conflicts on VSCode, one may start with the ones that only require picking one or both versions of the code, then turn to the Merge Editor for the remaining, more involved, conflicting blocks.

Once every conflict was handled, I can "Complete Merge" (VSCode will refuse to cooperate if some of the conflicts are not solved yet), and the status of the conflicting file(s) will change:

![](../assets/img/07-vscode/E-conflicts/vscode-conflict-11.jpg){: .mx-auto.d-block :}

Depending on your Git settings, it may be impossible for you to edit the title of the commit message:

![](../assets/img/07-vscode/E-conflicts/vscode-conflict-12.jpg){: .mx-auto.d-block :}

You should still try and give a fresh, clear commit message, and if this warning appears, just Ctrl-Z to get back to the original message.

One commit later:

![](../assets/img/07-vscode/E-conflicts/vscode-conflict-13.jpg){: .mx-auto.d-block :}

I am now one commit ahead of the repo, and can safely push by clicking on "Sync changes".

# Branching and MRs {#branch}

Nothing too terribly bad so far, right? I think we are ready to switch to safe collaborative development practices. Let us leave `main` alone and (at long last) work on branches again!

## Working on a fresh branch

How to create a branch in VSCode? Well, not that hard, really:

![](../assets/img/07-vscode/F-branch/vscode-branch-01.jpg){: .mx-auto.d-block :}

You will get a prompt asking you for the name of the new branch:

![](../assets/img/07-vscode/F-branch/vscode-branch-02.jpg){: .mx-auto.d-block :}

A `git switch -c <branch_name>` will be done automatically. If you'd rather be careful, just run `git status` in a terminal (...why not the one that is integrated to VSCode?), *et voilà*:

![](../assets/img/07-vscode/F-branch/vscode-branch-03.jpg){: .mx-auto.d-block :}

On this branch `change-recipe-format`, I am going to just turn my `.txt` file into a proper Markdown file, formatting and all. Once this is done, the Git panel tells me what was changed:

![](../assets/img/07-vscode/F-branch/vscode-branch-04.jpg){: .mx-auto.d-block :}

File `recipe-v1.md` is untracked, and `recipe-v1.txt` does not exist anymore. In both cases, though, I can just click on the `+` button. Do not worry, VSCode knows what it is doing: for the `.md` file, a `git add` will be run; for the `.txt` file, a `git rm`. This is made clear once both stages are changed:

![](../assets/img/07-vscode/F-branch/vscode-branch-05.jpg){: .mx-auto.d-block :}

Indeed, the `.md` file is added and the `.txt` file is deleted. Great. I can now commit my changes (with a proper commit message) or, if I like doing two things at once:

![](../assets/img/07-vscode/F-branch/vscode-branch-06.jpg){: .mx-auto.d-block :}

"Hey, this should not work, right?" Indeed: every change so far is local. In particular, no remote branch called `change-recipe-format` exists yet. Should you go back to the terminal for the push and use the `--set-upstream` option? Nope:

![](../assets/img/07-vscode/F-branch/vscode-branch-07.jpg){: .mx-auto.d-block :}

Just click OK and this will be done under the hood.

## Creating a MR

Hey, there are merge options in our menu!

![](../assets/img/07-vscode/F-branch/vscode-branch-08.jpg){: .mx-auto.d-block :}

Let us click on this one, then pick a source branch...

![](../assets/img/07-vscode/F-branch/vscode-branch-09.jpg){: .mx-auto.d-block :}

...well, hold on.

This is not right, is it?

There is something at play here: merging branches is something that Git can do by itself, but every nice option that is provided by GitLab (picking reviewers, discussing changes, running tests on a branch before it can be merged...) is forgotten here. This is just plain old Git merging, in a pure unsafe and selfish way. You do **not** want to do this.

Let us make this absolutely clear:

{: .box-warning}
Merging should **never** be done outside of GitLab, because the safeguards offered by GitLab are here for a reason.

{: .box-note}
Actually, every single user of the project should at least be prevented from pushing and merging onto the `main` branch, so that all changes to this branch have to be validated through a MR. [We went through this before.]({{'/05-good-practices#protect' | relative_url }})

So, back to the project page on GitLab. You should get a nice banner here:

![](../assets/img/07-vscode/F-branch/vscode-branch-10.jpg){: .mx-auto.d-block :}

If this is not the case, you can still create your MR from the menu on the left-hand side.

## Switching branches

Once again, pretty easy, except that it is not called "switching" but "checking out" here:

![](../assets/img/07-vscode/F-branch/vscode-branch-11.jpg){: .mx-auto.d-block :}

It is basically the same thing. Well, in this specific case, it is *exactly* the same thing. [We went through part of this before]({{'/04-branches-issues#feature' | relative_url }}), but here are more details:

{: .box-note}
The `git switch` command was added for disambiguation purposes. The historical `checkout` command can be used in [several different ways](https://stackoverflow.com/a/57266005/21698549) (change branches, create a new branch, inspect a specific commit, remove fresh changes to specific files, ~~bring coffee and toasts, trigger the end of the universe as we know it~~ and, above all, make inexperienced Git users utterly confused). Now, there is `switch` for changing branches, `restore` to remove untracked changes from one or several files, and `checkout` for old-school masochists and IDEs. However, `git switch <branch>` and `git checkout <branch>` are equivalent, and `checkout` should not be deprecated in the foreseeable future.

Okay, so, checking out/switching in VSCode. A single click is enough to open a nice list of branches to choose from:

![](../assets/img/07-vscode/F-branch/vscode-branch-12.jpg){: .mx-auto.d-block :}

Back to `main`, my editor tab for `recipe-v1.md` is still open, but clearly indicates that, where I currently am, this file does not exist: VSCode just shows me a buffered file that will disappear as soon as I close the tab.

![](../assets/img/07-vscode/F-branch/vscode-branch-13.jpg){: .mx-auto.d-block :}

Also note that, in the process, no SSH passphrase was asked. Indeed, no request was sent to the repo: I just switched to my local copy of `main` (this is how `git switch` works). Who knows what happened on the repo while I was working on my branch? If I really want to make sure that I did not miss out on anything:

![](../assets/img/07-vscode/F-branch/vscode-branch-14.jpg){: .mx-auto.d-block :}

This illustrates why one should, at least in my opinion, learn to use Git "the hard way" *before* using it through VSCode: the IDE makes your life easier, but only if you know what you are doing and what the buttons mean. Use the Git panel in VSCode without understanding the basics of Git, and you are in for terrible headaches.

----

The "core" of this tutorial is now officially over... but there are still nice things to learn. How about knowing more about [**how Git works under the hood?**]({{'/08-internals' | relative_url }}) ("Wow, this site is a gift that keeps on giving!" I know. Please hold your applause until the end.)

You may also go back to the [detailed table of contents]({{'/index#toc' | relative_url }}).
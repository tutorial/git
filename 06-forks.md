---
title: "Forks and workflows"
subtitle: "Where we have options."
tags: 
---

There is a sort of assumption that was taken as granted so far, namely: all collaborators to a project will work on branches that should be merged to the `main` branch at some point. End of story.

This workflow is pretty simple, and will probably be the one you will "instinctively" want to use in your first projects, but ~~people who are way more clever than me~~ the community has come with more refined workflows that leverage even further the capabilities of Git and Git-based tools.

There is even a way of collaborating on a project in which none of the developers will be granted access to the original project!

"Wait," some of you just said, "what the heck?" (Yes, I heard that.) Time to talk about forks.

# Forks and merge requests {#fork}

I was recently told by a friend of mine that, in the company he used to work for, MRs for branches basically do not exist; instead, they rely on MRs for *forks*. For experienced users, there are indeed a lot of advantages to this workflow. To explain this in further detail, we first have to know what a fork is.

## Forking {#forking}

{: .box-success}
A ***fork*** is a fresh repository that contains an exact copy of an already existing repository. This new repository is now yours, in the sense that you have all permissions on it, but GitLab still knows where it comes from.

In other words, forking a repo is a way of getting your own copy of an existing project. You are then free to do whatever you want with it... in a way.

### Legal disclaimer

We should be a bit more precise about this stuff. Just a tiny bit.

{: .box-warning}
You are free to modify and distribute a forked project ***within the limits of the license of the original project.***

Be very cautious with this! Open source software, in particular, are subject to pretty permissive licenses, either in the "do-whatever-you-want" category (MIT, BSD...) or in the "you-have-to-keep-the-same-licensing" category (aka *copyleft*: GPL, MPL, and others), but a public codebase is **not** open source by default!

Ask your doctor is forking is right for you.

### Forking a GitLab project

This one is tricky.

That is, GitLab-tricky: there is no big blue button on the page on the project you want to fork.

Instead, the button is white:

![](../assets/img/06-forks/fork-01.jpg){: .mx-auto.d-block :}

Yup. Just click on the "Forks" button, and you will have the option to fork the project. You will be asked the name you want to give your fork and where you want to put it (either your namespace, which is [still a bad option in most cases]({{'/02-creating-projects#groups' | relative_url }}), or a group/subgroup):

![](../assets/img/06-forks/fork-02.jpg){: .mx-auto.d-block :}

This absolutely looks like your standard project creation page, with a bit less options (it would make no sense to give you an "Initialize project with a README" option, after all).

Click.

![](../assets/img/06-forks/fork-03.jpg){: .mx-auto.d-block :}

A few seconds later, you have a project. It is yours (kind of). You can work on it.

## Merge request {#fork-mr}

Remember when I told you that a MR is about bringing the contents of a branch into another branch? Well, here is a nice little detail for you: *these branches do not need to be in the same project*. You can suggest merging contents of a fork into the original project.

This is how it happens for me. I made some changes on my fork, and I really think these changes would be beneficial for other people who use the original project, without breaking any already existing feature.

Well, guess who will help you?

![](../assets/img/06-forks/fork-04.jpg){: .mx-auto.d-block :}

This appeared on the page of my fork as soon as I committed some stuff on it. I can totally open an MR into the source project:

![](../assets/img/06-forks/fork-05.jpg){: .mx-auto.d-block :}

I can also, at any point, open an MR from the sidebar, on the webpage of my fork: branches of the original project will appear among the possible target branches.

![](../assets/img/06-forks/fork-06.jpg){: .mx-auto.d-block :}

The form you have to fill to actually create the MR is basically [the same as with "local" MRs]({{'/04-branches-issues#mr' | relative_url }}), except for this new gizmo at the bottom of the page:

![](../assets/img/06-forks/fork-07.jpg){: .mx-auto.d-block :}

...that's two gizmos. Well. So:

1. If the original project is not a private project, that basically means that you are offering some code to a "public" codebase. It can make sense for the community that developed this codebase in the first place to have their word, and what better way of letting them contribute than directly allowing them to commit?
2. This "Contribution guidelines" stuff does not come from nowhere. Well... Turns out there is a reason why GitLab suggests (and [so did I]({{'/05-good-practices#cicd' | relative_url }})) to create a file named `CONTRIBUTING.md` at the root of any project to which other people might contribute. This link is a pointer to the `CONTRIBUTING.md` file of the project you forked. The closer you follow these guidelines, the higher the chances of your MR being accepted.

{: .box-note}
On the other hand, if you want to work from a given snapshot of a project without contributing to it, you may even remove the "fork relationship", by delving into *Settings > Advanced* -- at your own risk, though, because you will also be missing out on new features, bug fixes, etc. (Once again, you should still make sure that you are complying with the licence(s) of the original project.)

## Closing remarks {#fork-closing}

**As a forker**, remember that the project you forked will probably evolve while you are working on your own personal copy. In other words, you will sometimes fall behind the original project, which will make things pretty complicated if you want to submit an MR at some point. (Also, maybe some of the commits that were pushed on the original project solve bugs you would have encountered at some point... so, it is also in *your* best interest to stay up to date.)

GitLab will tell you when you are behind the forked project:

![](../assets/img/06-forks/fork-update.jpg){: .mx-auto.d-block :}

...but you have to deal with the consequences (even when you have less commits to pull than here). As a result, you will have to think about your workflow when you work on a fork (more on that below).

**As a forked** (...forkee?), remember that the forks of a project depend on the project itself. For instance, this is the reminder I will get if I want to delete a project that was forked:

![](../assets/img/06-forks/fork-08.jpg){: .mx-auto.d-block :}

This is a nice summary of why your project maybe should not be deleted: the higher the numbers, the more you should think about it. Plus, it could be considered rude.

Regarding forks, there is a simple rule to be remembered:

{: .box-warning}
***If you delete a private project, its forks will be deleted too.*** If you delete a *public* project, one of its forks will be automatically picked as the new "original project" from which all other forks are forked off.

# Git workflows {#workflows}

It is now time to get back to our first concern: which workflow should we use for a project? Indeed, there exist several different workflows for Git projects, and which one you actually implement depends on the needs you have. Let us have a look at the three "main" workflows that emerged, each with its benefits and drawbacks.

## Trunk-based workflow {#trunk-wf}

{: .box-success}
The **trunk-based workflow** consists in merging frequent, small updates to the `main` branch (the "trunk" of the repo). This is probably the workflow you will be tempted to use when you start using Git, as it is a very simple and intuitive one.

Up to this point, we have been using this workflow without giving it a name. The fact that it is centralized means that, with non-experienced users (very much including people who were accustomed to heavily centralized tools like SVN), it will be very easy to implement.

A trunk-based workflow can be very efficient for [DevOps](https://www.atlassian.com/devops/what-is-devops), a software engineering practice in which continuous feedback is used for improving the codebase very often (pretty much on a daily basis). It is also a good fit for prototype projects with a small dedicated team of developers. However, it can hinder long-term research and development efforts; its agile characteristics do not always fit the needs of researchware. It also does not scale well: as your development team grows larger, the constant merging and conflict resolution will become more of a hindrance.

A small change to this workflow consists in updating a `develop` branch on a regular basis, while the `main` branch will only get updated once in a while, typically when everything is working ("no test should fail on the `main` branch"). This approach, sometimes referred to as **feature branching**, is arguably "cleaner" but suffers from the same drawbacks. However, if you push it a bit further, you can get to...

## Gitflow {#gitflow}

{: .box-success}
**Gitflow** is a strict branching model centered on releases. It typically relies on two "main" branches, namely a `develop` branch and a `main` branch that only receives commits from `develop`, and adds release branches on which only release-ready versions of the codebase will be pushed, and several feature and hotfix branches with clearly-defined roles.

Let us delve a bit further into this branching model:

* The `develop` branch is the one on which new features will be implemented, to be merged at some point into the `main` branch. As was the case with feature branching, this is a way to ensure that the `HEAD` of the `main` branch always points to production-ready code.
* Feature branches can only be branched off from `develop` and merge back to it. This is where features are developed, issues are handled, and so on. Nothing too fancy for the moment.
* Release branches can only be branched off from `develop`, and they have to be merged back into both `develop` and `master`.
    * The basic idea is this: when the code on `develop` is "nearly ready" for a production release, let's say version `0.1` for the sake of illustration, create a branch called `release-0.1`. The code on this branch will only get updates like minor bug fixes and metadata updates. (Meanwhile, the `develop` branch is living its best life, probably incorporating some new features that will be part of version `0.2`!)
    * When the release is ready, merge it into the `main` branch, where it will be given a tag (`v0.1` for example). Also merge it back to the `develop` branch, so that pre-release bugfixes get into the current working version.
* Hotfix branches can only be merged off from `main`, and must be merged back into both `main` and `develop`.
    * Imagine that some bug shows its ugly head just after version 0.1 is released. You can create a branch `hotfix-0.1` (or any other name starting with `hotfix-` indeed) from `main` and get one or several dedicated members of your team working on this branch to work on a fix.
    * Once the problem is fixed, a new minor version of the software (that would be `0.1.1` in our case) is ready to be released: merge it back into `main` with a nice `v0.1.1` tag, and merge it into `develop` so that the fix is now part of the "main" codebase.
    * Of course, hotfix branches can be created for any previous release if needed.

There is no "magic trick" in Gitflow, no new commands to master, no new tools to use: once again, this is just a branching model... but it is a very good branching model for incremental projects with official releases. Conceived in 2010 by software engineer Vincent Driessen, who explained it in detail in [this immensely popular blog post](https://nvie.com/posts/a-successful-git-branching-model/), it can benefit, among others, large collaborative projects with industrial stakes.

However, merging a feature branch in `develop` requires more effort and caution than in a trunk-based workflow, as there are more commits and larger changes in feature branches (with the risk of having to handle more conflicts along the way). The `hotfix-*` branches also increase complexity and require even more careful planning. If your software is not explicitly versioned, with several versions (typically, the last X versions) that need to get maintained/patched if needed, Gitflow is likely too heavy for your needs.

## Forkflow {#fork-wf}

{: .box-success}
The **forkflow**, more commonly named **forking workflow**, consists in letting any developer or developing team fork a repository and open MRs from the fork to the original project.

Typically, completed feature branches from a fork will be the object of MRs into the original project. It is then up to the maintainers of this project to integrate these new features and deal with releases. As such, the forkflow tends to rely on a branching model similar to Gitflow, except that features are developed in forks instead of new branches in the same project.

Note that, as a forker, you will have to pull the changes from the original project on a regular basis while working on a feature: you do not want to rewrite some piece of code that was recently added by other contributors, and you want to be able to open a MR that is *merge-ready*, i.e., not too far behind the destination branch. You may [read GitLab's page about forkflows](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) for further info.

The forkflow is an ideal workflow for open-source projects, where possible contributors are basically "trusted third parties" that should be able to propose new features or fixes without being given write access to the project. However, advantages are not limited to the world of FOSS.

In an active project with a lot of contributors, the history of the project will soon get overloaded with development branches. Even when enforcing naming conventions (none of which is a perfect solution) for branches, this can quickly become a mess. This will not happen with forks, so that the important branches of the original project will have a clearer history. As for the CI, it comes packaged with the rest of the project when you fork it! Just set a runner on your fork and you're good to go.



# Personal takes {#personal-wf}

In practice, as a project gets larger, I believe that you may very well "reinvent the wheel" one step at a time, by reorganizing your workflow once in a while, so as to address the development management problems when they are encountered... until you reach a workflow that actually looks incredibly similar to Gitflow.

Or you may converge to a workflow that takes advantage of features from different "conventional" workflows (this is typically the case of a project I am working on, and our workflow became more or less stable after more than three years).

{: .box-note}
On the other hand, if, after reading tens of different articles on this topic and/or because of your own experience, you reach the conclusion that *one* specific workflow suits your needs for a given project, then you should not only implement this workflow as early as possible, but also **describe this workflow in simple terms in the `CONTRIBUTING.md` file at the root of your project**.

To me, for most non-industrial projects, pretty much anything is fine as long as:
* collaborators agree on, and are fine with, the workflow they use;
* you still keep in mind that a workflow can and *should* be improved or changed whenever efficiency problems arise.

----

We have come quite far in all things related to software development, Git and GitLab. I hope you did not try and read it in a single sitting (if you did, you should really go take a nap). The next part will be way simpler, now that we went through all the tough stuff: it is about [**using Git directly from VSCode**]({{'/07-vscode' | relative_url }}).

You may also go back to the [detailed table of contents]({{'/index#toc' | relative_url }}).
# Mermaid graphs

## Codes for the graphs in Section 3

```mermaid
sequenceDiagram
participant Working
participant Staging
participant Repo
Repo->>Working: git clone git@...
Working->>Staging: git add ...
Working->>Staging: git commit ...
Working->>Staging: git add ...
Working->>Staging: git commit ...
Staging->>Repo: git push
```

```mermaid
sequenceDiagram
participant Working
participant Staging
participant Repo
Working->>Staging: git add ...
Working->>Staging: git commit ...
Staging->>Repo: git push
```

```mermaid
sequenceDiagram
participant Working
participant Staging
participant Repo
Working->>Staging: git add ...
Working->>Staging: git commit ...
Repo->>Staging: git pull
Staging->>Repo: git push
```

## Conversion

I used [this online Mermaid editor](https://workflow.jace.pro) with configuration:

```json
{
    "theme": "neutral"
}
```

"Link to image" then directly gives the generated JPEG image.
# Git/GitLab tutorial

A friendly tutorial about Git and Gitlab. Also, jokes. (I apologize.)

## Why another one?

I definitely know that the Internet is filled to the brim with tutorials on Git and GitLab. I have read, at least partially, tens of them, some incredibly insightful, other ones... not so much. So, why write another one?

The answer is pretty simple, actually: the first sentence here was a white lie. There are not a lot of tutorials on Git and GitLab. Instead, there are a lot of tutorials on Git, *and* a lot of tutorials on GitLab.

I spent way too much time learning Git "the hard way", from the terminal only, then learning GitLab by delving through its official documentation, then experimenting (and making terrible mistakes) until I finally got hold of the whole package.

As I started collaborating with people who did not have the time and/or patience to take on the same journey, the idea of writing a Git **and** GitLab tutorial became self-evident. This is the current state of the result. Hope you enjoy!

## How can I contribute?

Please do not hesitate to contact me via [e-mail](mailto:mathias.malandain@inria.fr) if you have questions and/or constructive criticism. You may also open an issue on the project, and I will be happy to have a chat with you!

For convenience, I will not be accepting MRs that are not related to any open issue. Other MRs are welcomed, with the simplest solution being submitting them from a fork (please grant me writing permissions).

To validate your changes before opening an MR, please follow the instructions provided in the [Local Deployment readme file](docs/LocalServing.md).

## Acknowledgements

My huge, huge, ***HUGE*** thanks to Sébastien Gilles for his careful proofreading and insightful advice.

Powered by [Beautiful Jekyll](https://beautifuljekyll.com) by [Dean Attali](https://deanattali.com), under the MIT license.

## Legal

This whole tutorial is licensed under [CC BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/).

(c) [Mathias Malandain](mathias.malandain@inria.fr), Inria center at Rennes University
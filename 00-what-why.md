---
title: "What and why?"
subtitle: "Where we know what we are doing here."
tags: 
---

# Git {#git}

{: .box-success}
Git is a **free distributed version control system** created by Linus Torvalds for the small, pretty confidential, operating system kernel he created (maybe you heard of it).

Among its advantages, you should remember that Git:

* is made for ***collaborative development***, even with very large teams of contributors with different roles;
* provides ***versioning utilities*** to manage releases, bug tracking, and so on;
* handles ***conflicts*** between different versions of a code in a pretty advanced way;
* introduces a ***branching system*** for, among others, enabling the parallel development of features.

This might look like complete overkill for the development of researchware, but it is very useful in this context for several reasons:

* It makes it possible to ***control what is merged in the main version of the codebase*** (and ensure that this main version always works, as we shall see).
* It provides a very convenient ***tagging system***, so that the exact version of the codebase that was used for providing numerical results in scientific articles can be easily retrieved (and you may even go further by providing stable execution environments...).
* The use of ***Git tools*** such as GitLab makes it possible to ensure long-term backups of codes developed by permanent and non-permanent members alike.

# GitLab {#gitlab}

{: .box-success}
GitLab is a **software development platform** that relies on Git for providing high-level tools for handling your codebase.

A lot of features provided by GitLab are incredibly useful on a day-to-day basis, among others:

* A very convenient ***issue and bug tracking*** system for clear and efficient communication and collaboration;
* A ***fine handling of permissions and code review***: you can control who can write to each branch of the project, and under which conditions a feature can be merged into the main version of the codebase (which tests should be run, how many people should review and approve the changes, and so on);
* Easy-to-use ***CI/CD pipelines***: Continuous Integration makes it possible to thoroughly test your code before merging it in the main version, and Continuous Delivery provides you with the possibility to automatically deploy your software for your collaborators to use.

A lot of other Git-based development platforms exist (GitHub, Gitea, Bitbucket, to name a few), but it so happens that Inria hosts two instances of GitLab, at `gitlab.inria.fr` and `gitlab-int.inria.fr`. Everything on these instances is hosted on Inria's own servers, which guarantees that your code is safe.

{: .box-warning}
In most cases, you should **not** use `gitlab.com` or `github.com` for your software projects. If you are only working with specific collaborators, including a few who do not have an Inria account, no need for going on GitHub: you can very simply [invite external collaborators to `gitlab.inria.fr`](https://external-account.inria.fr). The use of external platforms like `gitlab.com` and `github.com` can be envisioned for open-source software, for which external contributions can be expected (...and in that case, you should also probably discuss the legal aspects with the right people at Inria before making any move), but as long as there is no stringent need, you should keep your codebase safe.

{: .box-note}
Quickly, about `gitlab.inria.fr` and `gitlab-int.inria.fr`: both are hosted on Inria's own servers, but the latter is way more protected, as *one cannot invite external collaborators on it* (only people with Inria accounts and e-mail addresses) and it can only be accessed from the intranet (i.e., from your office network or via VPN). **Confidential projects *must* be hosted on `gitlab-int.inria.fr`.** For non-confidential projects, `gitlab.inria.fr` is the default choice, and it will be used throughout the rest of this tutorial (the other instance works pretty much the same way). More information about Inria's GitLab instances [on this page](https://doc-si.inria.fr/display/SU/Gitlab) (French only at the time of writing).

It is impossible to overstate how the use of GitLab makes collaborating on a software project easier and faster, and you can learn how to use it **one step at a time**.

----

Of course, before that, [**we have to go through a bit of setup**]({{'/01-setup' | relative_url }}).

You may also go back to the [detailed table of contents]({{'/index#toc' | relative_url }}).
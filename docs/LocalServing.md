# Serve the site locally

During development, you might want to test your changes and see them on the website.
To do so, you can run Jekyll locally and serve the website on `localhost:4000`.

## Install Ruby and RubyGems

Easy peasy:

```shell
sudo apt install ruby-dev gems
```

## Install Jekyll and the bundler

Using `gem`:

```shell
sudo gem install jekyll bundler
```

## Install the website dependencies

Just `cd` into the root directory of the project (where `Gemfile` is located), then:

```shell
bundle install
```

## Start the local server

```
bundle exec jekyll serve --config "_config_dev.yml"
```

This last command makes sure that `_config_dev.yml` is used, so as to override production environment with development environment.

---

Now the website should be available at `localhost:4000` and will automatically be updated every time the code is changed.

(Huge thanks to Gwendal Jouneaux for this.)
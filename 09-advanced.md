---
title: "Advanced commands and troubleshooting"
subtitle: "Where we learn from our mistakes."
tags: 
---

This part is not 101 stuff, but rather various "tricks" that you may or may not have to (or want to) use. If you arrived at this point for the first time, I strongly advise you to skip this part entirely, so that you do not suffer from information overload. However, if you happen to have questions on a specific use of Git and it is actually answered below, please enjoy! (And if you have a question that is unanswered, please [let me know](mailto:mathias.malandain@inria.fr) and I might write an answer down here for everyone to enjoy.)

# Branch management: `git fetch` and `git fetch --prune` {#git-fetch}

As we have seen numerous times before, there are not a lot of Git commands that actually have your computer interact with the repo. The one you will be using a lot will be `git pull`, which is actually `git fetch` followed by `git merge`: the former actually fetches the changes from the repo, and the latter merges these changes with your current working copy.

Note, however, that `git fetch`, used by itself, will do more than just fetch the contents of the branch you are working on:

{: .box-success}
Command `git fetch` is used to fetch the contents of the repo (or *remote*), including branches and tags previously unknown by your computer.

This is an example of an output of `git fetch`:

```
remote: Enumerating objects: 6, done.
remote: Counting objects: 100% (6/6), done.
remote: Compressing objects: 100% (4/4), done.
remote: Total 4 (delta 2), reused 0 (delta 0), pack-reused 0
Unpacking objects: 100% (4/4), 484 bytes | 242.00 KiB/s, done.
From gitlab.inria.fr:bakery/bread/multigrain-baguette
 * [new branch]      readme-structure-change -> origin/readme-structure-change
 * [new tag]         checkpoint              -> checkpoint
```

You do not have to understand exactly what the first 5 lines mean: essentially, there was content on the repo that was not on my computer, so that Git downloaded it for me. (This content was packed beforehand, so as to reduce the volume of data to be transferred, and unpacked on my machine after the transfer itself.)

The last three lines are more interesting:

* I am told that new content was fetched from the Inria GitLab repo.
* In particular, a branch that I did not know of, called `readme-structure-change`, now exists. This branch now exists in my working copy: a `git switch` to this branch will be lightning fast, because my computer holds all necessary data to switch to this branch.
* In a similar fashion, my computer did not know about tag `checkpoint`; now it does.

{: .box-note}
"New content fetched from the repo? Well," you may ask, "where else could it come from? Duh!" First, please do not duh me, thank you, and second, a Git project may very well have several remotes associated to it. Our current use (with GitLab, Github and others) relies on a central repo, but nothing technically prevents you from collaborating on a Git project with several people in a decentralized network... apart from the fact that it is pretty difficult, messy, and no one does that anymore.

Note, however, that `git fetch` may bring you new content, but that it will not delete content that does not exist anymore on the repo.

That is, unless you ask. Remember [`git fetch --prune`]({{'/04-branches-issues#clean' | relative_url }})? This command will delete references that do not exist anymore on the repo before it performs the actual fetching: this includes branches that were deleted from the repo -- hopefully after they were merged to active branches -- but not tags: option `--prune-tags` also has to be provided for deleting tags. (Alternatively, one may edit the `.git/config` file so that one line reads `fetch.pruneTags=true`: this tells Git to also prune tags when `git fetch --prune` is invoked.)

Once again, do not get too anxious about pruning: local branches that you never pushed to the repo will **not** be deleted. Pinky promise.

# Cleaning up your local copy {#cleanup}

Your Git repo was probably created for one of several software pieces, which you will compile and run on your computer at some point. By doing this, you will create a lot of files and folders that should not be committed: subfolders like -- hey, [am I just repeating myself now?]({{'/05-good-practices#gitignore' | relative_url }})

This is the thing: one might want to remove all untracked files (i.e., all files in their *local* working copy that are unknown by Git), or, more probably, to only remove the files that are ignored by Git (typically, those pesky compilation/runtime junk files -- and you know that one of those caches preprocessor variables and messes with your CMake, but you do not know which one it is and it has already been an hour, Gosh).

All hail `git clean`.

* Use `git clean -f` to remove all untracked files from the current folder.
* Add the `-d` option to do this recursively: all subfolders of the current folders will be cleaned up as well.
* Add the `-X` option (that's an uppercase `X`) to only remove files ignored by Git.
* Lowercase `x` has a different meaning: `-x` will remove every untracked file, ignored or not. Say goodbye to any local file that you were holding dear.
* Add the `-n` option for a dry run, so that you just know what would be removed without it. If you are OK with these changes, you may now run the same command without this option.

{: .box-warning}
The `-f`, or `--force`, option is here because one of the Git configuration variables might very well prevent you from cleaning anything without forcing. This is one of the rare instances when it is kind of OK to use `-f` in a Git command, because if misused, it will only have an impact on *yourself*. **When using pretty much any Git command other than `git clean`, do not ever write `-f`, unless you are 110% sure that you know what you are doing.**

(**Pro Tip:** If you want to play the `-f` card at some point, have your local software engineer do it instead, so that *they* will be held accountable if anything goes wrong.)

These are the most used options for `git clean`; the rest is [here](https://git-scm.com/docs/git-clean) if you want to know more. Several options can be combined using a single dash before all of them, which leads us to the main command you should remember (plus, the advice you should always keep in mind):

<div class="box-success" markdown="1">
* Use `git clean -dfX` (uppercase `X`) to **remove ignored files from the current directory and all subdirectories**.
* In most (in not all) cases, **you should do a dry run *before* the actual cleanup by adding the `-n` option**.
</div>

# Juggling between branches {#juggling}

The title of this section is admittedly vague, so let us have a brief look at what is covered below:

* *rebasing*, i.e., keeping up with the branch from which our current working branch was created;
* *interactive rebasing*, i.e., reorganizing commits on a local branch (while also rebasing, if we are feeling daring);
* *cherrypicking*, i.e., applying one or several commits on a different branch.

## Rebasing (how to keep up with a source branch) {#rebase}

There are already a lot of very good resources about `git rebase`, probably because it is a command that seems pretty frightening at first, but is actually a very powerful and beautiful command. This is why I will only get to the "why" and the 101 here, before providing you with a few links. Also, this is kind of a "pure Git" command, which is not exactly the scope of this tutorial.

{: .box-success}
**Rebasing** consists in taking a sequence of commits and moving them somewhere else in the graph of the project.

...okay, well, that's... pretty vague. Could I provide an example? Yeah, sure. Here is the most common use of rebasing:

* You created a local `feature` branch from a source branch (let's say `main`), and pushed a few commits on it.
* Meanwhile, other commits were pushed to the `main` branch. Some of these commits probably change files that you also modified on the `feature` branch.
* You know that you will go on working on `feature`, and that other people will go on pushing stuff on `main`. The longer it goes, the harder merging your branch in `main` will be.
* What if you moved all your fresh commits to the latest commit on `main`? This is what rebasing your `feature` branch onto `main` will do.

<div class="box-success" markdown="1">
By rebasing, you can have your local branch "look like" it originates from the latest commit on the source branch, and during this rebasing process, you will be able to fix conflicts directly on your local branch. This means two things:

* First, if you make a mistake when fixing conflicts, the outcome will be in your working copy, not on the shared source branch. In other words, if you break something, you are not breaking it *for everyone*.
* Second, if you rebase on a regular basis, you will have fewer and simpler conflicts to handle each time, and the chances that you actually break your code will be way slimmer!
</div>

This is the most common "why". The "how" is not that difficult:

1. Check that you committed all your changes on the `feature` branch.
2. Update the source branch; if it is `main`, you may run `git switch main` followed by `git pull`, for example. (The use of `git pull` here makes step 1 basically mandatory; otherwise, uncommitted changes are moved to the `main` branch when you switch to it, and `git pull` will force you to merge these changes with the contents of `main`.)
3. Switch back to `feature` and run `git rebase main`. (Step 2 was used to make `main` point to the latest commit. If you skip step 2, you may rebase on an older commit... which was not the goal, was it?)

If conflicts arise at some point during the rebasing process, Git will tell you which files contain conflicts. In these files, you will be able to see the usual conflict markers (`<<<<<<<<`, `========`, `>>>>>>>>`), and you can solve these conflicts as you see fit. Once it is done, you can `git add` the files to tell Git that you solved all conflicts in them, and run `git rebase --continue`.

Finally, be reassured: if anything goes wrong in the process, you can just run `git rebase --abort` and everything will be back to the initial state.

Oh, also, before we forget: rebasing is great, but for local branches only.

{: .box-error}
**Do not rebase a branch that was pushed to the repo:** if you do so, you will then have to force push, that is, to rewrite the public history of the repo, and this is a very bad practice. Not only is it generally deemed disrespectful and unsafe, but it will also cause you a lot of problems when anyone then tries to interact with the branch from their working copy.

A notable exception to the above rule is for projects in which a "fast-forward merge only" strategy is enforced, meaning that one just cannot merge a branch if it does not start from the head of the branch onto which it has to be merged. In such cases, though, one will typically work on a branch that was already marked as "ready for merging" (i.e., no more commits to push to this branch) and that will probably be deleted as soon as it is merged, so... No impact whatsoever on your collaborators.

While we're at it, we should at least mention a huge advantage of a workflow in which everything has to be rebased before merging: this will prevent commits from different branches to end up mingled in the main development branch (which may very well happen without it, especially in large and active projects). Commits that were coming from a given branch will always end up being contiguous, which is a blessing both in terms of clarity and for bug fixing. The latter, in particular, is why squashing commits when merging is not a good strategy: in most cases, you should go with rebasing instead.

If you are into reading stuff (which you probably are, as you are currently reading Section 642279 of this thing), a pretty good reference about rebasing is [the Git SCM page about it](https://git-scm.com/book/en/v2/Git-Branching-Rebasing). It features just the right amount of examples, every time along with the basic commands needed in the corresponding situations. It just suffers from two minor issues:

* At the time of writing, it still uses the kind of deprecated `git checkout` command instead of `git switch` (this is a pretty common thing [we already discussed before]({{'/04-branches-issues#feature' | relative_url }})).
* It assumes that both branches are already up-to-date on your computer, which might not be the case: before running the rebase itself, remember to `pull` the changes on the branch you want to rebase into.

If you want to understand the difference, in a nutshell, between rebasing and merging, there's [a nice page on the Atlassian website](https://www.atlassian.com/git/tutorials/merging-vs-rebasing) about this, as well as countless questions and answers on Stack Overflow, some of which are [very short and to the point](https://stackoverflow.com/questions/804115/when-do-you-use-git-rebase-instead-of-git-merge), others [way more detailed](https://stackoverflow.com/questions/457927/git-workflow-and-rebase-vs-merge-questions).

On the Atlassian page, there is also something about... *interactive* rebasing? What the heck is this? Well, glad you asked.

## Interactive rebasing (how to clean up your mess before pushing) {#irebase}

As we just saw, one of the most common uses (if not *the* typical use-case) of rebasing goes something like this: you are working locally on a branch (say, `feature`) that you created from a source branch (say, `main`), and you want to keep up with what's happening on `main`, so that you will not be drowning in conflicts once you open a MR to `main`. Thus, you rebase on a regular basis (pun not intended, sorry about this). How nice.

But what about filling two needs with one deed? (Not the most common idiom, but as long as I hold the reins, there will be no bird stoning on this website.)

**Assume** that the history of local branch `feature` is kind of a mess. This happens all the time: you need to develop 3 different methods that work together for implementing a given feature, but only see a mistake in method A once it is used by method B, resulting in a bug. You fix the mistake, commit the result, realize that your fix is actually not enough, change method A again, commit again... oh, and between those two commits, you took a few minutes to write the docstring for method C. (Also, if you are anything like me, you were probably a bit salty when writing some of the commit messages, so... why not also change those before you have to push this branch?)

{: .box-success}
**Interactive rebasing** consists in manipulating a series of commits so as to reorder them, edit their messages, delete or squash commits, split a commit into several ones, and so on. It is effectively a way of cleaning up the history of (all or part of) a branch before pushing it.

Speaking of which, of course:

{: .box-error}
Just like good ol' rebasing, interactive rebasing rewrites history. Hence, **do NOT use interactive rebasing on a branch that was already pushed to the origin**.

The basic course of action is as follows:
* See where you want to start rebasing: you can get, with [commands such as `git log`]({{'/04-branches-issues#graph' | relative_url }}), the short hash of the commit *just before* the sequence of commits you want to change (e.g., `4f29ed1`), or count the number of commits you want to take into account, in which case the `HEAD~<number_of_commits>` shortcut will come in handy (e.g., `HEAD~5` if you want to change the last 5 commits), or even manipulate the whole branch by using the name of the branch it was branched off (e.g., `main`).
  * In the latter case, **make sure that you just rebased!** Otherwise, you will be trying to do two things at the same time (integrating fresh changes from `main` into your branch *and* manipulating your local history), and I do not recommend trying this. Safety first.
* Launch the interactive rebase session (`git rebase -i 4f29ed1` or `git rebase -i HEAD~5` or `git rebase -i main` in our examples).
* A text file appears, listing the commits and prefixing them with a short description of the operation to be performed on each commit. Every commit is initially prefixed with keyword `pick` (basically, keep the commit exactly as it is). Below this list is a long comment telling you what keywords can be used. Change the file as you want (you can also move lines up and down to reorder commits).
* Save and exit (hopefully).

Here is an illustration of what the text file might look like when you start rebasing:

```
pick 1fc6c95 Patch A
pick 6b2481b Patch B
pick c619268 A fix for Patch B
pick fa39187 something to adddd to patch A
pick 7b36971 something to move before patch B

# Rebase 4f29ed1..7b36971 onto 4f29ed1
#
# Commands:
#  p, pick = use commit
#  r, reword = use commit, but edit the commit message
#  e, edit = use commit, but stop for amending
#  s, squash = use commit, but meld into previous commit
#  f, fixup = like "squash", but discard this commit's log message
#  x, exec = run command (the rest of the line) using shell
#
# If you remove a line here THAT COMMIT WILL BE LOST.
# However, if you remove everything, the rebase will be aborted.
#
```

Note that `git rebase -i` lists the commits in the opposite order of `git log`, i.e., in the standard chronological order, with the most recent being at the bottom. This makes things clearer to our poor monkey brains.

This is what the same file might look like after you work on it (of course, you may or may not remove the commented lines):

```
pick 1fc6c95 Patch A
fixup fa39187 something to adddd to patch A
reword 7b36971 Prepare for Batch B
pick 6b2481b Patch B
fixup c619268 A fix for Patch B
```

I found [this very nice article by Blake DeBoer](https://dev.to/blakedeboer/beginners-guide-to-interactive-rebasing-1ob) about interactive rebasing, based on a not-that-simple example, and 100% performed from the terminal. It is, in my opinion, a way better introduction than the thousands of pages showing you the basics ("I want to rename a commit") and leaving you with that. I hope you enjoy.

Also, you should definitely play with `git rebase -i` on a dummy project. You might very well realize that it is actually pretty simple to use, while also proving incredibly powerful in a lot of cases. This is how you gain confidence in your Git skills, and this is how, in a few weeks or months, your collaborators may thank you for providing them with very clear and organized branches in your MRs.

## Cherrypicking (how to move commits around) {#cherrypick}

Cherrypicking is basically how one can take a commit on a given branch and apply the same changes on another branch. This absolutely does not look like something anyone would like to use in an actual project, right? If I want to move some code from one branch to another, I would like to either merge, or rebase!

Well, yes and no. Here is a good use-case of cherrypicking: a hotfix was applied in the `release` branch of a project, and I want to move it back to the main development branch, but I do not want to merge `release` into `main` (maybe because the other commits on the `release` branch are just a bunch of patches in a user interface that is currently being redesigned from scratch, or because I work with people who regard merging `release` into `main` as malpractice).

Here is another one: someone just fixed a bug on the development branch and I want to use the corresponding commit to patch the latest release of the project. However, a bunch of other stuff has been changed on `main` since then, so that I just cannot merge `main` into the release branch.

Finally, to make things clear, here is a case in which cherrypicking is actually a bad idea: a feature that you are currently developing in a fresh branch (that was, say, branched off `main`) was already fully implemented by someone else, in several commits, on another branch `feature`. Even if you cherrypick all these commits at once, this will very likely yield merge conflicts, because cherrypicking is actually merging! What you probably want to do instead is go back on `main`, create a fresh branch from there, and merge `feature` in there.

Now, for more practical information:

{: .box-success}
**Cherrypicking** consists in picking the changes that a commit applied to the codebase on one branch, and applying these changes on another branch.

This is a more specific definition than the arguably simpler "applying a commit on another branch". The reason is that, in the Git terminology, a commit is an object that not only contains a description of the changes (the *diff*) to be applied, but also information about the author, time of creation, etc. as well as a hash that serves as an identifier of this commit for future purposes. As such, copy-pasting a commit would be weird: its metadata would be wrong, and its hash would be shared with the original commit, which defeats the purposes of a hash.

Hence, cherrypicking applies, to the current branch, the same changes that were applied by another commit on another branch, and "packages" them into a fresh commit with its own metadata and its own hash.

Cherrypicking is pretty simple:
* Find the hash of the commit that you want to "copy" on top of another branch.
* Switch to the branch in question.
* Type `git cherry-pick <commit_hash>` (or `git cherry-pick -x <commit_hash>`, see below).
* Profit.

Adding option `-x` to the `git cherry-pick` command will automatically append `(cherry picked from commit <commit_hash>)` to the message of the fresh commit, which makes it easier to track where the changes come from. Oh, wait, did I write "easier"? I meant "possible in the first place": if this option is not given, the new commit will only appear as a standard commit, created *ex nihilo*, that just happens to have the same message as a commit from another branch. If you want even the slightest bit of traceability, you should definitely use this option.

"Wait, that was it?" Yup, pretty much.

# Searching for bugs with `git bisect` {#git-bisect}

Okay, let us delve into some pretty depressing scenarios.

* Imagine that you just implemented a new test for your software, and it fails miserably. The problem is that this test focuses on a feature that was introduced a long time ago.
* Or maybe you already had this test but forgot to run it for a pretty long time (...maybe you should think about implementing a [CI pipeline]({{'/05-good-practices#cicd' | relative_url }})). Like, you think you remember it passed last time you ran the whole test suite, in March of last year maybe?
* Or this test only runs on the versions that were tagged for release, so that you know that this test passed on the latest release, but fails on the one you are currently preparing.

In any case, the current version of the codebase is "bad", the last known "good" version was a bazillion commits ago, and these commits introduced changes in thousands of lines of code across tens of files.

You try and use your favorite debugger for fixing the issue, but you just end up trying to understand the logics of tens or hundreds of methods that you either did not write, or wrote eons ago. The situation looks dire.

Well, what if I told you that you may be able to restrict the scope of your investigation to a few tens of lines?

{: .box-success}
**`git bisect`** is a simple implementation of binary search (in Baguette: "recherche par dichotomie") that helps you pinpoint the commit that introduced a bug.

It is actually pretty simple to use. In our case, we first want to save our test outside of our working copy. Once this is done, find a commit hash, a tag, anything that can identify a commit that you know did not "feature" the bug (maybe the one just before the first implementation of the method that your failing test focuses on, or maybe an old version in which the test actually passes if you are dealing with regression): let us denote it by `<good_commit>` (it could be something like `v2.1`, or `HEAD~100`, for instance).

The magical command is

```sh
git bisect start HEAD <good_commit>
```

This command states that you want to start searching for the first "bad" commit, and you know that `HEAD` (the current state of your codebase) is "bad" and `<good_commit>` is "good". The search session is initiated with Git pulling a specific commit, namely, the one that is "right in the middle" of the range of commits in which you are searching for the bug. If `<good_commit>` is `HEAD~100`, for instance, Git will pull the commit referenced by `HEAD~50`.

You can test this version of the codebase and see whether it works or not. If it does, type `git bisect good`; if it doesn't, `git bisect bad`. You just cut the search space roughly in half: Git tells you how many commits are left in the range of commits to assess, and how many more steps of this game are left before you catch the culprit. It also jumps to another commit, right in the middle of the remaining range, so that you can now test this one.

Lather, rinse, repeat, until Git provides you with the description of the first bad commit. Now you may just inspect this specific commit (just click on it in the "Repository graph" on the webpage of your GitLab project and you will get a nice coloured diff that shows you all changes it introduced, and only those changes). This might make debugging way simpler!

{: .box-warning}
Do not forget to type `git bisect reset` once this is over, or if you get tired of this game before it ends; Git will clean everything up and return your copy to its state just before you started the binary search. Lovely.

For more information and options, [the Git SCM website is there for you](https://git-scm.com/docs/git-bisect).

# About the `.git` folder {#hooks-excludes}

[Earlier in this tutorial]({{'/03-linear-git-project#clone' | relative_url }}), I told you that you might just pretend that there is no reason to touch anything in the `.git` folder, you know, the one that is [automatically created]({{'/08-internals#computer' | relative_url }}) when you initialize or clone a project.

*Well, actually*, there are a few neat tricks that you can use there. Let us have a look at two of them: Git hooks, and local excludes.

## Git hooks {#git-hooks}

{: .box-success}
A ***hook*** is a script that gets executed when certain actions occur.

Hooks can be *client-side* (triggered by actions such as committing and merging), or *server-side* (triggered by actions such as receiving pushed commits); we will just have a look at client-side hooks in this section. Here are a few examples of such hooks ([there are way more](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks)):

* `pre-commit` may be used for running test suites or inspecting code (linting, formatting, etc.) before it gets committed. These are used quite a lot in practice: they help the project owner ensure that every pushed code complies with the standards of the project, they tell contributors what they should do to make their code comply with them, and they save time for everyone during MRs.
* `prepare-commit-msg` provides the text editor with a template, or default, commit message.
* `commit-msg` checks the validity of a commit message (generally in terms of conformance to a pattern) before creating the commit.
* `pre-rebase` may be used to ensure that a rebase will not go wrong before actually running it.

In all the cases above, the current operation (committing or rebasing) will be aborted if the hook returns a non-zero value.

Hooks may be written in any scripting language. Of course, the most common examples are shell scripts, like this very basic `pre-rebase` hook that would just prevent you from rebasing *at all*:

```bash
#!/bin/sh

echo "pre-rebase: Rebasing is dangerous. Don't do it."
exit 1
```

or Python scripts, like this `prepare-commit-msg` hook that fetches the issue number from the name of the branch and includes it to the commit message:

```python
#!/usr/bin/env python

import sys, os, re
from subprocess import check_output

# Collect the parameters
commit_msg_filepath = sys.argv[1]
if len(sys.argv) > 2:
    commit_type = sys.argv[2]
else:
    commit_type = ''
if len(sys.argv) > 3:
    commit_hash = sys.argv[3]
else:
    commit_hash = ''

print "prepare-commit-msg: File: %s\nType: %s\nHash: %s" % (commit_msg_filepath, commit_type, commit_hash)

# Figure out which branch we're on
branch = check_output(['git', 'symbolic-ref', '--short', 'HEAD']).strip()
print "prepare-commit-msg: On branch '%s'" % branch

# Populate the commit message with the issue #, if there is one
if branch.startswith('issue-'):
    print "prepare-commit-msg: Oh hey, it's an issue branch."
    result = re.match('issue-(.*)', branch)
    issue_number = result.group(1)

    with open(commit_msg_filepath, 'r+') as f:
        content = f.read()
        f.seek(0, 0)
        f.write("ISSUE-%s %s" % (issue_number, content))
```

(Both of these examples were blatantly stolen from [this more complete tutorial on Git hooks](https://www.atlassian.com/git/tutorials/git-hooks). If you are still not entertained, [here is the full reference](https://git-scm.com/docs/githooks).)

{: .box-note}
When a repository is initialized (with `git init`), sample hooks are created in the `.git/hooks` folder, with extensions `.sample`. The "dummy" codes inside them provides you with information about the inputs of the scripts.

In order for these hooks to be run, they must be renamed to the name of the hook (without any extension) and copied to `.git/hooks`.

<div class="box-warning" markdown="1">
***The `.git` folder is not under version control***: the hooks must be installed "manually" by each collaborator who wants to use them, and as a project maintainer, you probably **want** these hooks to be used by every single one of them.

* One solution is to provide hooks in another folder of the project and to give clear installation instructions to the developers, but the process might be made difficult for them by dependencies.
* To alleviate these problems, you should have a look at the [pre-commit framework](https://pre-commit.com/), which will only ask your collaborators to install it (via `pip install pre-commit` for example) and run it (with `pre-commit install`). Despite its name, this framework not only supports the `pre-commit` hook but also 9 other client-side hooks (at the time of writing).

In both cases, write down some clear information about how to setup and run these hooks in your `CONTRIBUTING.md` file.
</div>

## Local exclusion rules {#local-excludes}

We already talked about this one [a few million words ago]({{'/05-good-practices#gitignore' | relative_url }}), but let us state the basics again:

{: .box-success}
File **`.git/info/exclude`** can be used for defining local exclusion rules. It will provide additional instructions to Git on which files/folders from *your* working copy should be ignored, without setting fixed rules for all developers on your project.

There can be a lot of reasons why you would have some files in your working copy that you do not want to push to the origin. (And no, I am not talking about this `passwords.txt` file again, though... Come on.) Whatever those reasons are, there is no way you change the contents of the `.gitignore` file, a file that is pushed to the origin, hence shared with every collaborator on the project, for a couple files that no one else has in their working copy. Just edit the `.git/info/exclude` file instead, using the exact same syntax as in the `.gitignore` file.

# CI pipelines: `.gitlab-ci.yml` examples {#gitlab-ci}

Because of how CI pipelines can be configured, **no Git workflow is incompatible with CI**. A few examples are given here, in order to illustrate various use cases.

Remember that stage names are not fixed: one may define any stage name, and list all their stage names as a list under the tag `stages:` at the beginning of the document.

* **Run a job (here, a simple build job) for every commit and MR, excluding tags**

```yaml
build-job:
  stage: build
  script:
    - cd src
    - make install
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - when: always
```

As tags are separated from commits, this avoids running the same task twice on the same code (once for the commit itself, once when the tag is pushed).

* **Run a job (here, building a PDF documentation) for tags only**

```yaml
makedoc:
  stage: documentation
  script:
    - mkdir generated_doc
    - for file in $(ls *.md); do basename=$(echo "$file" | cut -f 1 -d '.'); pandoc "$file" -o generated_doc/"$basename".pdf; done
  rules:
    - if: $CI_COMMIT_TAG
  artifacts:
    paths: ["generated_doc/*.pdf"]
```

The `rules` section here is equivalent to

```yaml
  rules:
    - if: $CI_COMMIT_TAG
      when: always
    - when: never
```

In a workflow where tags are only used (and pushed on the repo) for releases, it makes sense to only generate documentation for tagged commits. This documentation is made available as a pipeline *artifact*.

* **Define rules that exclude specific branches/tags/MRs**

```yaml
# Only run the job if the pipeline was triggered by a MR
.merge_request_only_rules:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
```

```yaml
# Do NOT run the job if the pipeline is triggered by a commit
# on a branch whose name contains "misc", by a MR with target branch
# "misc_branch" (full name), or by a tag whose name contains "misc".
.exclude_misc_rules:
  rules:
    - if: $CI_COMMIT_BRANCH =~ /misc/
      when: never
    - if: $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "misc_branch"
      when: never
    - if: $CI_COMMIT_TAG =~ /misc/
      when: never
```

If you have several jobs that should be triggered under the exact same conditions, you should absolutely define such rules... and then:

* **Use a previously defined rule**

```yaml
# Run specific tests for MRs only (only after the build job was successful)
ut:
  needs:
    - build-job
  stage: mr_tests
  script:
    - cd tests/mr_tests
    - touch unittests.log
    - make build
    - for file in $(ls *.byte); do ./$file 2>&1 | tee unittests.log; done
    - make clean
  rules:
    - !reference [.merge_request_only_rules, rules]
  artifacts:
    paths: ["tests/mr_tests/unittests.log"]
```

By the way, a `rules` section may also contain references to several previously defined rules, possibly alternating with other custom rules:

```yaml
  rules:
    - if: $CI_COMMIT_TAG
      when: always
    - !reference [.exclude_misc_rules, rules]
    - !reference [.merge_request_only_rules, rules]
```

The first rule that applies to the current pipeline will take precedence. In the above example, a tag containing `misc` will not prevent the job using these rules from running, as the very first rule is the one that will apply.

# How to fix a commit to the wrong branch? {#wrong-branch}

This is something that a lot of people working with Git have done more often than they care to admit. Your project lives on several branches, and juggling between these branches is a process that will eventually have you commit something on the wrong branch. This can happen when you are working on several features, or when you are alternating between developing a new feature and patching a release: at some point, you *will* forget to run `git status` before committing, and your commit will end up on the wrong branch.

Not all occurrences require the exact same solution, in particular because of this Number One Rule that I already wrote, like, 600 times in this tutorial: **Public history must not be rewritten.** (Yes, I am still emphasizing it like it is the first time.) Hence, depending on whether your commit was already pushed on the origin or not, you should not respond in the same way.

## Moving local commits around {#wrong-branch-local}

This is the nice use case: you made your commit to the wrong branch, but it only lives on your computer. Nothing has been pushed yet.

{: .box-note}
This is one of the many reasons why `git commit` and `git push` should not always be used together: you can create several commits without pushing any of them, so that you can safely review your work and local history before pushing anything. Other reasons include the possibility to [rebase on a regular basis]({{'/09-advanced#rebase' | relative_url }}) so as to make the development and merging process more streamlined, and/or [reorganize your branch before pushing it]({{'/09-advanced#irebase' | relative_url }}).

Here is our use case (just change branch names depending on the mix-up you currently want to fix): the last commit I created is on branch `main`, but it was actually intended for branch `feature`. (Oh, BTW, if `main` is protected, [which it should definitely be]({{'/05-good-practices#protect' | relative_url }}), Git will prevent you from pushing this commit, which will help you realize that you made a mistake and avoid making the situation worse.)

First, copy the hash of your commit (a simple `git log` will do). Then, the process is basically in two steps: [cherrypick]({{'/09-advanced#cherrypick' | relative_url }}) the commit to the `feature` branch, and delete it from the `main` branch.

* Step 1: `git switch feature` (switch to the branch on which the commit should go), `git cherry-pick <commit_hash>` (create a new commit that applies the same changes on `feature` as your misplaced commit did on `main`);
* Step 2: `git switch main` (switch back to the branch where you wrongly committed in the first place), `git reset --hard HEAD~1` (reset the branch to its state one commit ago, which actually deletes the last commit).

{: .box-warning}
Remember that `git reset --hard HEAD~1` will undo and delete the changes from the last commit, while `git reset --soft HEAD~1` would just "uncommit" them but keep them in your working copy as untracked changes. We can safely use option `--hard` here, because the changes in question were just applied to another branch *and* have absolutely nothing to do on the current branch: this is one of the rare instances where using `git reset --hard` is "safe".

## Moving pushed commits around {#wrong-branch-pushed}

Now, assume that the situation is pretty much the same, last commit with hash `<commit_hash>` was created on `main` but should have been on `feature`, blah-bidy-blah, but this time, **you pushed this commit on `main`**. This is more serious now.

Step 1 remains basically unchanged: cherrypick the commit on the branch to which it belongs. However, if you were to apply step 2 as is, this would not change the situation on the remote. Instead, this would just result in the `main` branch of your working copy being one commit behind `origin/main`, and your next  `git status` from this branch would just tell you that you should pull and it would be a simple fast-forward. Not good.

Some nice people on Stack Overflow would then advise you to force-push: `git push --force` would overwrite `origin/main` with the contents of your local `main` branch, thus effectively erasing the last commit from public history... You may already know why this is terrible advice in most cases, right? If one or several of your collaborators already pulled the branch as it was seconds ago, with your "wrong" commit on it, then you just made the situation worse for everyone. Once again (for the last time, pinky promise): **Do not rewrite public history unless absolutely necessary.**

{: .box-note}
By "absolutely necessary", I basically mean "one of the public commits contains sensitive, private information", and even then, you will have to carefully agree with all members of the project on a specific date at which public history will be rewritten, explicitly tell them what commands they will have to use, make sure that absolutely everyone understands and agrees... Yeah, basically, try and avoid such a terrible situation.

"Okay, so, I cannot just delete my commit, because it is already public. What do I do?" Well, pretty simple: you *revert* it.

{: .box-success}
**Reverting a commit** is the act of inverting/undoing the changes introduced by this commit. It does not remove the commit, but instead creates a fresh commit that applies the inverse changes.

This will work, because you do not have to rewrite public history: instead, you will only be adding a fresh commit on top of it, which is absolutely fine. And all you have to do it to type `git revert <commit_hash>`. Even better: if you want to "undo" the very last commit, then `git revert HEAD` will also work, because `HEAD` is actually a pointer to the last commit on the branch. How nice.

Of course, this means that the history of the branch will now contain two commits that basically cancel each other, the second one bearing the message `Revert "<message of the original commit>"`. There is now evidence that you screwed up at some point. Your collaborators *know*. Your credibility is lost. Nothing will ever be the same again.

...just kidding. Maybe one or two of them will notice, and they will be like "eh, happens to the best of us", or "wait, how do you revert a commit? I didn't know this existed", depending on whom you work with.

So, to sum up:

* Step 1: `git switch feature` (switch to the branch on which the commit should go), `git cherry-pick <commit_hash>` (create a new commit that applies the same changes on `feature` as your misplaced commit did on `main`);
* Step 2: `git switch main` (switch back to the branch where you wrongly committed in the first place), `git revert <commit_hash>` (create a fresh commit that undoes the changes introduced by your misplaced commit), `git push` ("undo" your mistake on the origin).

----

Aaaand you reached the end of this whole mess of a tutorial 🎉 If you feel like something is missing (in this page in particular), please do not hesitate to contact me using the clicky black circles below.

Just in case, [here is the table of contents]({{'/index#toc' | relative_url }}).